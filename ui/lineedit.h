#ifndef LINEEDIT_H
#define LINEEDIT_H

#include <QLineEdit>
#include <QFocusEvent>
#include <QResizeEvent>
#include <QLabel>

class LineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit LineEdit(QWidget *parent = 0);
private:

protected:
    virtual void focusInEvent(QFocusEvent *e);
    virtual void resizeEvent(QResizeEvent *event);

signals:
    void focused();

public slots:

};

#endif // LINEEDIT_H
