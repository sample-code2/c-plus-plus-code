#include "startloadingdialog.h"
#include <QMovie>
#include <QFile>
#include <QTextCodec>
#include "settings.h"
#include "mainview.h"

StartLoadingDialog::StartLoadingDialog(QWidget *parent) :QWidget(parent){
    resize(600, 380);

    setStyleSheet("background:transparent");
    setAttribute(Qt::WA_TranslucentBackground);
    setWindowFlags(Qt::FramelessWindowHint);

    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    setFont(QFont(":/fonts/BHoma.ttf"));

    QLabel *backLable = new QLabel(this);
    backLable->resize(size());
    backLable->setPixmap(QPixmap(":/startLogo"));
    backLable->setScaledContents(true);

    QMovie *movie = new QMovie(":/loading");
    QLabel *processLabel = new QLabel(this);
    processLabel->resize(100,100);
    processLabel->move(430, 250);
    processLabel->setMovie(movie);
    processLabel->setStyleSheet(" background:transparent;");
    movie->start();

    QByteArray version = Settings::Version();
    QLabel *versionLable = new QLabel(this);
    versionLable->resize(200, 25);
    versionLable->setText(QString("%0%1%2").arg((quint8)version.at(0), 2, 10, QChar('0')).arg((quint8)version[1], 2, 10, QChar('0')).arg((quint8)version[2], 2, 10, QChar('0')));
    versionLable->move(100, height()-versionLable->height());
    versionLable->setStyleSheet("color: white; text-align: left; font: 20px;");

    messageLable = new QLabel(this);
    messageLable->resize(width(), 30);
    messageLable->move(0, height()/2+100);
    messageLable->setStyleSheet("color: white; font: 30px;font-family:'B Homa';");
    messageLable->setAlignment(Qt::AlignCenter);
}

void StartLoadingDialog::showMessage(QString str){
     messageLable->setText(str);
}
