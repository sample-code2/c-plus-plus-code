#ifndef ANTENAWIDGET_H
#define ANTENAWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QTimer>

class AntenaWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AntenaWidget(QString text = QString(), QWidget *parent = 0);

private:
    QLabel *pix;
    QTimer *flashTimer;
signals:
private slots:
    void showHide();

public slots:
    void SetValue(quint8 value);
    void setFixShow(bool active);
    
};

#endif // ANTENAWIDGET_H
