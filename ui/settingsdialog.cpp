#include "settingsdialog.h"
#include <QSettings>
#include <QProcess>
#include <QMessageBox>
#include <QDebug>
#include "mylibs/settings.h"
#include "messagebox.h"
#include "mainview.h"

SettingsDialog::SettingsDialog(QWidget *parent) :QWidget(parent){
    keyboard = MainView::instance()->_keyboard;

    resize(MainView::instance()->size());
    setLayoutDirection(Qt::RightToLeft);

    Tabs = new QTabWidget(this);
    Tabs->resize(size());

    Create_SystemPage();
    Create_ServerPage();
    Create_ReaderPage();
    Create_DatabasePage();

    Tabs->addTab(SystemSettings, "سیستم");
    Tabs->addTab(ServerSettings, "سرور");
    Tabs->addTab(ReaderSettings, "کارتخوان ها");
    Tabs->addTab(DatabaseSettings, "بانک داده");
}

void SettingsDialog::Create_SystemPage(){
    SystemSettings = new QWidget(this);
    SystemSettings->resize(size());
    SystemSettings->setLayoutDirection(Qt::LeftToRight);

    timeEdit = new QTimeEdit(SystemSettings);
    timeEdit->resize(200, 64);
    timeEdit->move(SystemSettings->width()-timeEdit->width()-10, 25);
    timeEdit->setTime(QTime::currentTime());

    QLabel *dateTimeSettingLable = new QLabel(SystemSettings);
    dateTimeSettingLable->setText(":تنظیم ساعت و تاریخ");
    dateTimeSettingLable->setStyleSheet("font: 18px;color:green");
    dateTimeSettingLable->resize(200, 18);
    dateTimeSettingLable->setAlignment(Qt::AlignBottom | Qt::AlignRight);
    dateTimeSettingLable->move(timeEdit->x()+timeEdit->width()-dateTimeSettingLable->width(), timeEdit->y()-dateTimeSettingLable->height());

    dateEdit = new jalaliDateEdit(SystemSettings);
    dateEdit->resize(280, 64);
    dateEdit->move(timeEdit->x()-dateEdit->width()-5, timeEdit->y());
    dateEdit->setStyleSheet("font: 24px;");
    dateEdit->setDate(QJalaliDate::currentDate());

    applyDateTimeButton_Custom = new QPushButton(SystemSettings);
    applyDateTimeButton_Custom->resize(64, 64);
    applyDateTimeButton_Custom->setText("تنظیم\nدستی");
    applyDateTimeButton_Custom->setIconSize(applyDateTimeButton_Custom->size());
    applyDateTimeButton_Custom->move(dateEdit->x()-applyDateTimeButton_Custom->width()-5, timeEdit->y());
    applyDateTimeButton_Custom->setFlat(true);
    connect(applyDateTimeButton_Custom, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    applyDateTimeButton_Auto = new QPushButton(SystemSettings);
    applyDateTimeButton_Auto->resize(64, 64);
    applyDateTimeButton_Auto->setText("تنظیم از\nماهواره");
    applyDateTimeButton_Auto->setIconSize(applyDateTimeButton_Auto->size());
    applyDateTimeButton_Auto->move(applyDateTimeButton_Custom->x()-applyDateTimeButton_Auto->width()-5, timeEdit->y());
    applyDateTimeButton_Auto->setFlat(true);
    connect(applyDateTimeButton_Auto, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    Shutdown_Button = new QPushButton(QIcon(":/Graphics/system-shutdown.png"), "", SystemSettings);
    Shutdown_Button->resize(64, 64);
    Shutdown_Button->setIconSize(Shutdown_Button->size());
    Shutdown_Button->move(10, height()-Shutdown_Button->height()-50);
    Shutdown_Button->setFlat(true);
    connect(Shutdown_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    Reboot_Button = new QPushButton(QIcon(":/Graphics/system-reboot.png"), "", SystemSettings);
    Reboot_Button->resize(64, 64);
    Reboot_Button->setIconSize(Reboot_Button->size());
    Reboot_Button->move(Shutdown_Button->x()+Reboot_Button->width()+10, Shutdown_Button->y());
    Reboot_Button->setFlat(true);
    connect(Reboot_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    Close_Button = new QPushButton(QIcon(":/Graphics/close.png"), "", SystemSettings);
    Close_Button->resize(64, 64);
    Close_Button->setIconSize(Reboot_Button->size());
    Close_Button->move(Reboot_Button->x()+Close_Button->width()+10, Shutdown_Button->y());
    Close_Button->setFlat(true);
    connect(Close_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    //    CheckUpdate_Button = new QPushButton(SystemSettings);
    //    CheckUpdate_Button->resize(64, 64);
    //    CheckUpdate_Button->setIconSize(Shutdown_Button->size());
    //    CheckUpdate_Button->move(Close_Button->x()+CheckUpdate_Button->width()+10, Shutdown_Button->y());
    //    CheckUpdate_Button->setText("به روز\nرسانی");
    //    CheckUpdate_Button->setFlat(true);
    //    connect(CheckUpdate_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));
}

void SettingsDialog::Create_ServerPage(){
    ServerSettings = new QWidget(this);
    ServerSettings->setLayoutDirection(Qt::LeftToRight);
    ServerSettings->resize(size());

    ServerAddresLineedit = new LineEdit(ServerSettings);
    ServerAddresLineedit->resize(180, 32);
    ServerAddresLineedit->move(width()-ServerAddresLineedit->width()-10, 10);
    ServerAddresLineedit->setAlignment(Qt::AlignCenter);
    ServerAddresLineedit->setFont(QFont("B homa"));
    ServerAddresLineedit->setStyleSheet("color: darkblue; font: 18px;");
    ServerAddresLineedit->setPlaceholderText("آدرس سرور");
    connect(ServerAddresLineedit, SIGNAL(focused()), SLOT(setKeyboard()));

    ServerPortLineedit = new LineEdit(ServerSettings);
    ServerPortLineedit->resize(120, 32);
    ServerPortLineedit->move(ServerAddresLineedit->x()-ServerPortLineedit->width(), ServerAddresLineedit->y());
    ServerPortLineedit->setAlignment(Qt::AlignCenter);
    ServerPortLineedit->setFont(QFont("B homa"));
    ServerPortLineedit->setStyleSheet("color: darkblue; font: 18px;");
    ServerPortLineedit->setPlaceholderText("پورت");
    connect(ServerPortLineedit, SIGNAL(focused()), SLOT(setKeyboard()));

    ServerUrlLineedit = new LineEdit(ServerSettings);
    ServerUrlLineedit->resize(300, 32);
    ServerUrlLineedit->move(ServerPortLineedit->x(), ServerPortLineedit->y()+ServerUrlLineedit->height()+5);
    ServerUrlLineedit->setAlignment(Qt::AlignCenter);
    ServerUrlLineedit->setFont(QFont("B homa"));
    ServerUrlLineedit->setStyleSheet("color: darkblue; font: 18px;");
    ServerUrlLineedit->setPlaceholderText("صفحه ارسال");
    connect(ServerUrlLineedit, SIGNAL(focused()), SLOT(setKeyboard()));

    CardSendRate_Lineedit = new LineEdit(ServerSettings);
    CardSendRate_Lineedit->resize(150, 32);
    CardSendRate_Lineedit->move(ServerPortLineedit->x(), ServerUrlLineedit->y()+CardSendRate_Lineedit->height()+5);
    CardSendRate_Lineedit->setAlignment(Qt::AlignCenter);
    CardSendRate_Lineedit->setFont(QFont("B homa"));
    CardSendRate_Lineedit->setStyleSheet("color: darkblue; font: 18px;");
    CardSendRate_Lineedit->setPlaceholderText("سیکل ارسال کارت");
    connect(CardSendRate_Lineedit, SIGNAL(focused()), SLOT(setKeyboard()));

    AvlSendRate_Lineedit = new LineEdit(ServerSettings);
    AvlSendRate_Lineedit->resize(150, 32);
    AvlSendRate_Lineedit->move(CardSendRate_Lineedit->x()+AvlSendRate_Lineedit->width(), CardSendRate_Lineedit->y());
    AvlSendRate_Lineedit->setAlignment(Qt::AlignCenter);
    AvlSendRate_Lineedit->setFont(QFont("B homa"));
    AvlSendRate_Lineedit->setStyleSheet("color: darkblue; font: 18px;");
    AvlSendRate_Lineedit->setPlaceholderText("سیکل ارسال موقعیت");
    connect(AvlSendRate_Lineedit, SIGNAL(focused()), SLOT(setKeyboard()));

    ServerApply_Button = new QPushButton(QIcon(":/Graphics/system-run.png"), "", ServerSettings);
    ServerApply_Button->resize(64, 64);
    ServerApply_Button->setIconSize(ServerApply_Button->size());
    ServerApply_Button->move(10, height()-ServerApply_Button->height()-50);
    ServerApply_Button->setFlat(true);
    connect(ServerApply_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    ServerReadValues_Button = new QPushButton(QIcon(":/Graphics/system-update.png"), "", ServerSettings);
    ServerReadValues_Button->resize(64, 64);
    ServerReadValues_Button->setIconSize(ServerReadValues_Button->size());
    ServerReadValues_Button->move(ServerApply_Button->x()+ServerReadValues_Button->width()+10, ServerApply_Button->y());
    ServerReadValues_Button->setFlat(true);
    connect(ServerReadValues_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    ServerClear_Button = new QPushButton(QIcon(":/Graphics/trash-full.png"), "", ServerSettings);
    ServerClear_Button->resize(64, 64);
    ServerClear_Button->setIconSize(ServerClear_Button->size());
    ServerClear_Button->move(ServerReadValues_Button->x()+ServerClear_Button->width()+10, ServerApply_Button->y());
    ServerClear_Button->setFlat(true);
    connect(ServerClear_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

}

void SettingsDialog::Create_ReaderPage(){
    ReaderSettings = new QWidget(this);
    ReaderSettings->resize(size());
    ReaderSettings->setLayoutDirection(Qt::LeftToRight);

    price0_lineEdit = new LineEdit(ReaderSettings);
    price0_lineEdit->resize(120, 35);
    price0_lineEdit->move(width()-price0_lineEdit->width()-10, 10);
    price0_lineEdit->setAlignment(Qt::AlignCenter);
    price0_lineEdit->setFont(QFont("B homa"));
    price0_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price0_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price0_lineEdit->setPlaceholderText("مبلغ ۰");
    connect(price0_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    price1_lineEdit = new LineEdit(ReaderSettings);
    price1_lineEdit->resize(price0_lineEdit->size());
    price1_lineEdit->move(price0_lineEdit->x(), price0_lineEdit->y()+price1_lineEdit->height()+5);
    price1_lineEdit->setAlignment(Qt::AlignCenter);
    price1_lineEdit->setFont(QFont("B homa"));
    price1_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price1_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price1_lineEdit->setPlaceholderText("مبلغ ۱");
    connect(price1_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    price2_lineEdit = new LineEdit(ReaderSettings);
    price2_lineEdit->resize(price0_lineEdit->size());
    price2_lineEdit->move(price1_lineEdit->x(), price1_lineEdit->y()+price2_lineEdit->height()+5);
    price2_lineEdit->setAlignment(Qt::AlignCenter);
    price2_lineEdit->setFont(QFont("B homa"));
    price2_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price2_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price2_lineEdit->setPlaceholderText("مبلغ ۲");
    connect(price2_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    price3_lineEdit = new LineEdit(ReaderSettings);
    price3_lineEdit->resize(price0_lineEdit->size());
    price3_lineEdit->move(price2_lineEdit->x(), price2_lineEdit->y()+price3_lineEdit->height()+5);
    price3_lineEdit->setAlignment(Qt::AlignCenter);
    price3_lineEdit->setFont(QFont("B homa"));
    price3_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price3_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price3_lineEdit->setPlaceholderText("مبلغ ۳");
    connect(price3_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    price4_lineEdit = new LineEdit(ReaderSettings);
    price4_lineEdit->resize(price0_lineEdit->size());
    price4_lineEdit->move(price3_lineEdit->x(), price3_lineEdit->y()+price4_lineEdit->height()+5);
    price4_lineEdit->setAlignment(Qt::AlignCenter);
    price4_lineEdit->setFont(QFont("B homa"));
    price4_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price4_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price4_lineEdit->setPlaceholderText("مبلغ ۴");
    connect(price4_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    price5_lineEdit = new LineEdit(ReaderSettings);
    price5_lineEdit->resize(price0_lineEdit->size());
    price5_lineEdit->move(price4_lineEdit->x(), price4_lineEdit->y()+price5_lineEdit->height()+5);
    price5_lineEdit->setAlignment(Qt::AlignCenter);
    price5_lineEdit->setFont(QFont("B homa"));
    price5_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price5_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price5_lineEdit->setPlaceholderText("مبلغ ۵");
    connect(price5_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    price6_lineEdit = new LineEdit(ReaderSettings);
    price6_lineEdit->resize(price0_lineEdit->size());
    price6_lineEdit->move(price5_lineEdit->x(), price5_lineEdit->y()+price6_lineEdit->height()+5);
    price6_lineEdit->setAlignment(Qt::AlignCenter);
    price6_lineEdit->setFont(QFont("B homa"));
    price6_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price6_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price6_lineEdit->setPlaceholderText("مبلغ ۶");
    connect(price6_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    price7_lineEdit = new LineEdit(ReaderSettings);
    price7_lineEdit->resize(price0_lineEdit->size());
    price7_lineEdit->move(price6_lineEdit->x(), price6_lineEdit->y()+price7_lineEdit->height()+5);
    price7_lineEdit->setAlignment(Qt::AlignCenter);
    price7_lineEdit->setFont(QFont("B homa"));
    price7_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price7_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price7_lineEdit->setPlaceholderText("مبلغ ۷");
    connect(price7_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    price8_lineEdit = new LineEdit(ReaderSettings);
    price8_lineEdit->resize(price0_lineEdit->size());
    price8_lineEdit->move(price7_lineEdit->x(), price7_lineEdit->y()+price8_lineEdit->height()+5);
    price8_lineEdit->setAlignment(Qt::AlignCenter);
    price8_lineEdit->setFont(QFont("B homa"));
    price8_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price8_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price8_lineEdit->setPlaceholderText("مبلغ ۸");
    connect(price8_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    price9_lineEdit = new LineEdit(ReaderSettings);
    price9_lineEdit->resize(price0_lineEdit->size());
    price9_lineEdit->move(price8_lineEdit->x(), price8_lineEdit->y()+price9_lineEdit->height()+5);
    price9_lineEdit->setAlignment(Qt::AlignCenter);
    price9_lineEdit->setFont(QFont("B homa"));
    price9_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    price9_lineEdit->setValidator(new QIntValidator(MIN_PRICE, MAX_PRICE, this));
    price9_lineEdit->setPlaceholderText("مبلغ ۹");
    connect(price9_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    busSerial_lineEdit = new LineEdit(ReaderSettings);
    busSerial_lineEdit->resize(120, 32);
    busSerial_lineEdit->move(price0_lineEdit->x()-busSerial_lineEdit->width()-10, 10);
    busSerial_lineEdit->setAlignment(Qt::AlignCenter);
    busSerial_lineEdit->setFont(QFont("B homa"));
    busSerial_lineEdit->setValidator(new QIntValidator(0, 999999, this));
    busSerial_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    busSerial_lineEdit->setPlaceholderText("شماره اتوبوس");
    connect(busSerial_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    busLine_lineEdit = new LineEdit(ReaderSettings);
    busLine_lineEdit->resize(price0_lineEdit->size());
    busLine_lineEdit->move(busSerial_lineEdit->x(), busSerial_lineEdit->y()+busLine_lineEdit->height()+5);
    busLine_lineEdit->setAlignment(Qt::AlignCenter);
    busLine_lineEdit->setFont(QFont("B homa"));
    busLine_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    busLine_lineEdit->setValidator(new QIntValidator(0, 999999, this));
    busLine_lineEdit->setPlaceholderText("شماره خط");
    connect(busLine_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    setReaderCountComboBox = new QComboBox(ReaderSettings);
    setReaderCountComboBox->resize(price0_lineEdit->size());
    setReaderCountComboBox->move(busLine_lineEdit->x(), busLine_lineEdit->y()+setReaderCountComboBox->height()+5);
    setReaderCountComboBox->setStyleSheet("border-radius:15px; font: 18px;");
    setReaderCountComboBox->addItem("یک دستگاه", 1);
    setReaderCountComboBox->addItem("دو دستگاه", 2);
    setReaderCountComboBox->addItem("سه دستگاه", 3);
    setReaderCountComboBox->setCurrentIndex(Settings::ReaderCount()-1);
    connect(setReaderCountComboBox, SIGNAL(activated(int)), SLOT(ReaderCountChanged(int)));

#ifdef MY210
    setReaderIDButton1 = new QPushButton(ReaderSettings);
    setReaderIDButton1->resize(price0_lineEdit->size());
    setReaderIDButton1->setFont(QFont("B homa"));
    setReaderIDButton1->move(busLine_lineEdit->x(), setReaderCountComboBox->y()+setReaderIDButton1->height()+5);
    setReaderIDButton1->setText("تنظیم آیدی ها");
    connect(setReaderIDButton1, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));
#elif defined TINY210
    setReaderIDButton1 = new QPushButton(ReaderSettings);
    setReaderIDButton1->resize(price0_lineEdit->size());
    setReaderIDButton1->setFont(QFont("B homa"));
    setReaderIDButton1->move(busLine_lineEdit->x(), setReaderCountComboBox->y()+setReaderIDButton1->height()+5);
    setReaderIDButton1->setText("تنظیم شماره 1");
    connect(setReaderIDButton1, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    setReaderIDButton2 = new QPushButton(ReaderSettings);
    setReaderIDButton2->resize(price0_lineEdit->size());
    setReaderIDButton2->setFont(QFont("B homa"));
    setReaderIDButton2->move(busLine_lineEdit->x(), setReaderIDButton1->y()+setReaderIDButton2->height()+5);
    setReaderIDButton2->setText("تنظیم شماره 2");
    connect(setReaderIDButton2, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    setReaderIDButton3 = new QPushButton(ReaderSettings);
    setReaderIDButton3->resize(price0_lineEdit->size());
    setReaderIDButton3->setFont(QFont("B homa"));
    setReaderIDButton3->move(busLine_lineEdit->x(), setReaderIDButton2->y()+setReaderIDButton3->height()+5);
    setReaderIDButton3->setText("تنظیم شماره 3");
    connect(setReaderIDButton3, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));
#endif

    clearMemoryButton1 = new QPushButton(ReaderSettings);
    clearMemoryButton1->resize(price0_lineEdit->size());
    clearMemoryButton1->setFont(QFont("B homa"));
#ifdef MY210
    clearMemoryButton1->move(busLine_lineEdit->x(), setReaderIDButton1->y()+clearMemoryButton1->height()+5);
#elif defined TINY210
    clearMemoryButton1->move(busLine_lineEdit->x(), setReaderIDButton3->y()+clearMemoryButton1->height()+5);
#endif
    clearMemoryButton1->setText("فرمت کارتخوان 1");
    clearMemoryButton1->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(200, 0, 0, 80%));");
    connect(clearMemoryButton1, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    clearMemoryButton2 = new QPushButton(ReaderSettings);
    clearMemoryButton2->resize(price0_lineEdit->size());
    clearMemoryButton2->setFont(QFont("B homa"));
    clearMemoryButton2->move(busLine_lineEdit->x(), clearMemoryButton1->y()+clearMemoryButton2->height()+5);
    clearMemoryButton2->setText("فرمت کارتخوان 2");
    clearMemoryButton2->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(200, 0, 0, 80%));");
    connect(clearMemoryButton2, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    clearMemoryButton3 = new QPushButton(ReaderSettings);
    clearMemoryButton3->resize(price0_lineEdit->size());
    clearMemoryButton3->setFont(QFont("B homa"));
    clearMemoryButton3->move(busLine_lineEdit->x(), clearMemoryButton2->y()+clearMemoryButton3->height()+5);
    clearMemoryButton3->setText("فرمت کارتخوان 3");
    clearMemoryButton3->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(200, 0, 0, 80%));");
    connect(clearMemoryButton3, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    Apply_Button = new QPushButton(QIcon(":/Graphics/system-run.png"), "", ReaderSettings);
    Apply_Button->resize(64, 64);
    Apply_Button->setIconSize(Apply_Button->size());
    Apply_Button->move(10, height()-Apply_Button->height()-50);
    Apply_Button->setFlat(true);
    connect(Apply_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    ReadValues_Button = new QPushButton(QIcon(":/Graphics/system-update.png"), "", ReaderSettings);
    ReadValues_Button->resize(64, 64);
    ReadValues_Button->setIconSize(ReadValues_Button->size());
    ReadValues_Button->move(Apply_Button->x()+ReadValues_Button->width()+10, Apply_Button->y());
    ReadValues_Button->setFlat(true);
    connect(ReadValues_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    clear_Button = new QPushButton(QIcon(":/Graphics/trash-full.png"), "", ReaderSettings);
    clear_Button->resize(64, 64);
    clear_Button->setIconSize(clear_Button->size());
    clear_Button->move(ReadValues_Button->x()+clear_Button->width()+10, ReadValues_Button->y());
    clear_Button->setFlat(true);
    connect(clear_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    ReaderCountChanged(setReaderCountComboBox->currentIndex());
}

void SettingsDialog::Create_DatabasePage(){
    DatabaseSettings = new QWidget(this);
    DatabaseSettings->resize(size());
    DatabaseSettings->setLayoutDirection(Qt::LeftToRight);

    VACUUM_Button = new QPushButton(DatabaseSettings);
    VACUUM_Button->resize(120, 35);
    VACUUM_Button->move(width()-VACUUM_Button->width()-10, 10);
    VACUUM_Button->setText("سازماندهی");
    connect(VACUUM_Button, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    TruncetCardTable = new QPushButton(DatabaseSettings);
    TruncetCardTable->resize(120, 35);
    TruncetCardTable->move(VACUUM_Button->x(), VACUUM_Button->y()+TruncetCardTable->height()+5);
    TruncetCardTable->setText("پاکسازی کارتها");
    TruncetCardTable->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(200, 0, 0, 80%));");
    connect(TruncetCardTable, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    TruncetAvlTable = new QPushButton(DatabaseSettings);
    TruncetAvlTable->resize(120, 35);
    TruncetAvlTable->move(VACUUM_Button->x(), TruncetCardTable->y()+TruncetAvlTable->height()+5);
    TruncetAvlTable->setText("avl پاکسازی");
    TruncetAvlTable->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(200, 0, 0, 80%));");
    connect(TruncetAvlTable, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));
}

void SettingsDialog::On_Buttons_Clicked(){
    if(sender() == Apply_Button){
        MessageBox msgBox(this);
        msgBox.setText("تنظیمات جدید برای کارتخوان ها ذخیره می شود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("بله", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            QSettings settings("tcco", "console");
            settings.setValue("bus/BusLine", busLine_lineEdit->text().toUInt());
            settings.setValue("bus/BusSerial", busSerial_lineEdit->text().toUInt());
            settings.setValue("bus/Price0", price0_lineEdit->text().toUShort());
            settings.setValue("bus/Price1", price1_lineEdit->text().toUShort());
            settings.setValue("bus/Price2", price2_lineEdit->text().toUShort());
            settings.setValue("bus/Price3", price3_lineEdit->text().toUShort());
            settings.setValue("bus/Price4", price4_lineEdit->text().toUShort());
            settings.setValue("bus/Price5", price5_lineEdit->text().toUShort());
            settings.setValue("bus/Price6", price6_lineEdit->text().toUShort());
            settings.setValue("bus/Price7", price7_lineEdit->text().toUShort());
            settings.setValue("bus/Price8", price8_lineEdit->text().toUShort());
            settings.setValue("bus/Price9", price9_lineEdit->text().toUShort());
            emit newReaderSettingAvailable();
        }
    }else if (sender() == Reboot_Button) {
        MessageBox msgBox(this);
        msgBox.setText("سیستم ریست می شود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("بله", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            QProcess RebootProcess;
            RebootProcess.startDetached("reboot");
        }
    }else if (sender() == Shutdown_Button) {
        MessageBox msgBox(this);
        msgBox.setText("سیستم خاموش می شود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("بله", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            QProcess RebootProcess;
            RebootProcess.startDetached("shutdown -h");
        }
    }else if (sender() == Close_Button) {
        MessageBox msgBox(this);
        msgBox.setText("نرم افزار کنسول بسته میشود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("بله", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            MainView::instance()->close();
        }
    }else if (sender() == ReadValues_Button) {
        QSettings settings("tcco", "console");

        price0_lineEdit->setText(settings.value("bus/Price0").toString());
        price1_lineEdit->setText(settings.value("bus/Price1").toString());
        price2_lineEdit->setText(settings.value("bus/Price2").toString());
        price3_lineEdit->setText(settings.value("bus/Price3").toString());
        price4_lineEdit->setText(settings.value("bus/Price4").toString());
        price5_lineEdit->setText(settings.value("bus/Price5").toString());
        price6_lineEdit->setText(settings.value("bus/Price6").toString());
        price7_lineEdit->setText(settings.value("bus/Price7").toString());
        price8_lineEdit->setText(settings.value("bus/Price8").toString());
        price9_lineEdit->setText(settings.value("bus/Price9").toString());
        busSerial_lineEdit->setText(settings.value("bus/BusSerial").toString());
        busLine_lineEdit->setText(settings.value("bus/BusLine").toString());
    }else if (sender() == clear_Button) {
        price0_lineEdit->clear();
        price1_lineEdit->clear();
        price2_lineEdit->clear();
        price3_lineEdit->clear();
        price4_lineEdit->clear();
        price5_lineEdit->clear();
        price6_lineEdit->clear();
        price7_lineEdit->clear();
        price8_lineEdit->clear();
        price9_lineEdit->clear();
        busSerial_lineEdit->clear();
        busLine_lineEdit->clear();
    }
#ifdef MY210
    else if (sender() == setReaderIDButton1) {
        MessageBox msgBox(this);
        msgBox.setText("آیدی جدید برای کارت خوانها تنظیم میشود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("اعمال", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            emit readerIdAvailable();
        }
    }
#elif defined TINY210
    else if (sender() == setReaderIDButton1) {
        MessageBox msgBox(this);
        msgBox.setText("لطفا تمامی کارت خوانها به جزء کارتخوان 1 را خاموش سپس کلید اعمال را بفشارید");
        QPushButton *yesButton = msgBox.addButton("اعمال", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            emit readerIdAvailable(1);
        }
    }else if (sender() == setReaderIDButton2) {
        MessageBox msgBox(this);
        msgBox.setText("لطفا تمامی کارت خوانها به جزء کارتخوان 2 را خاموش سپس کلید اعمال را بفشارید");
        QPushButton *yesButton = msgBox.addButton("اعمال", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            emit readerIdAvailable(2);
        };
    }else if (sender() == setReaderIDButton3) {
        MessageBox msgBox(this);
        msgBox.setText("لطفا تمامی کارت خوانها به جزء کارتخوان 3 را خاموش سپس کلید اعمال را بفشارید");
        QPushButton *yesButton = msgBox.addButton("اعمال", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            emit readerIdAvailable(3);
        }
    }
#endif
    else if (sender() == clearMemoryButton1) {
        MessageBox msgBox(this);
        msgBox.setText("تمامی اطلاعات کارتخوان 1 پاک میشود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("اعمال", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/error.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            emit ReaderFormatAvailable(1);
        }
    }else if (sender() == clearMemoryButton2) {
        MessageBox msgBox(this);
        msgBox.setText("تمامی اطلاعات کارتخوان 2 پاک میشود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("اعمال", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/error.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            emit ReaderFormatAvailable(2);
        }
    }else if (sender() == clearMemoryButton3) {
        MessageBox msgBox(this);
        msgBox.setText("تمامی اطلاعات کارتخوان 3 پاک میشود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("اعمال", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/error.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            emit ReaderFormatAvailable(3);
        }
    }else if (sender() == applyDateTimeButton_Custom) {
        setDateTime(QDateTime(dateEdit->date(), timeEdit->time()));
        MainView::instance()->messegeBox->setText("ساعت سیستم تنظیم شد");
        MainView::instance()->messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
        MainView::instance()->messegeBox->Show(3);
    }else if (sender() == applyDateTimeButton_Auto){
        if(MainView::instance()->_gps->dataIsValid()){
            setDateTimeFromGMT(QDateTime(MainView::instance()->_gps->parameters().Date,MainView::instance()->_gps->parameters().Time));

            MainView::instance()->messegeBox->setText("ساعت با ماهواره تنظیم شد");
            MainView::instance()->messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
            MainView::instance()->messegeBox->Show(3);
        }else{
            MainView::instance()->messegeBox->setText("اطلاعات ماهواره صحیح نیست\nلطفا ساعت را دستی تنظیم نمایید");
            MainView::instance()->messegeBox->setIcon(QPixmap(":/Graphics/exit.png"));
            MainView::instance()->messegeBox->Show(3);
        }
    }else if (sender() == ServerApply_Button){
        MessageBox msgBox(this);
        msgBox.setText("تنظیمات جدید ذخیره می شود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("بله", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            Settings::setServerIp(ServerAddresLineedit->text());
            Settings::setServerPort(ServerPortLineedit->text());
            Settings::setServerUrl(ServerUrlLineedit->text());
            Settings::setCardSendRate(CardSendRate_Lineedit->text().toUInt());
            Settings::setAvlSendRate(AvlSendRate_Lineedit->text().toUInt());
        }
    }else if (sender() == ServerReadValues_Button){
        ServerAddresLineedit->setText(Settings::ServerIp());
        ServerPortLineedit->setText(Settings::ServerPort());
        ServerUrlLineedit->setText(Settings::ServerUrl());
        CardSendRate_Lineedit->setText(QString("%0").arg(Settings::CardSendRate()));
        AvlSendRate_Lineedit->setText(QString("%0").arg(Settings::AvlSendRate()));
    }else if (sender() == ServerClear_Button){
        ServerAddresLineedit->clear();
        ServerPortLineedit->clear();
        ServerUrlLineedit->clear();
        AvlSendRate_Lineedit->clear();
        CardSendRate_Lineedit->clear();
    }else if (sender() == VACUUM_Button){
        QSqlQuery query_Eticket(QSqlDatabase::database("Local_Eticket_Sqlite"));
        QSqlQuery query_avl(QSqlDatabase::database("Local_Avl_Sqlite"));
        query_Eticket.exec("VACUUM");
        query_avl.exec("VACUUM");
        MainView::instance()->messegeBox->setText("سازماندهی بانک انجام شد");
        MainView::instance()->messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
        MainView::instance()->messegeBox->Show(3);
    }else if (sender() == TruncetCardTable){
        MessageBox msgBox(this);
        msgBox.setText("تمامی اطلاعات کارتها از بانک داده پاک میشود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("اعمال", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/error.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            QSqlQuery query(QSqlDatabase::database("Local_Eticket_Sqlite"));
            query.exec("DELETE FROM 'cardinfo';");
            MainView::instance()->messegeBox->setText("پاکسازی انجام شد");
            MainView::instance()->messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
            MainView::instance()->messegeBox->Show(3);
        }

    }else if (sender() == TruncetAvlTable){
        MessageBox msgBox(this);
        msgBox.setText("تمامی اطلاعات موقعیت ها از بانک داده پاک میشود");
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("اعمال", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/error.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            QSqlQuery query(QSqlDatabase::database("Local_Avl_Sqlite"));
            query.exec("DELETE FROM 'avlinfo';");
            MainView::instance()->messegeBox->setText("پاکسازی انجام شد");
            MainView::instance()->messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
            MainView::instance()->messegeBox->Show(3);
        }
    }
}

void SettingsDialog::setKeyboard(){
    if(sender() == price0_lineEdit){
        keyboard->setTarget(price0_lineEdit);
        keyboard->setVisible(true);
    }else if (sender() == price1_lineEdit) {
        keyboard->setTarget(price1_lineEdit);
        keyboard->show();
    }
    else if (sender() == price2_lineEdit) {
        keyboard->setTarget(price2_lineEdit);
        keyboard->show();
    }
    else if (sender() == price3_lineEdit) {
        keyboard->setTarget(price3_lineEdit);
        keyboard->show();
    }
    else if (sender() == price4_lineEdit) {
        keyboard->setTarget(price4_lineEdit);
        keyboard->show();
    }
    else if (sender() == price5_lineEdit) {
        keyboard->setTarget(price5_lineEdit);
        keyboard->show();
    }
    else if (sender() == price6_lineEdit) {
        keyboard->setTarget(price6_lineEdit);
        keyboard->show();
    }
    else if (sender() == price7_lineEdit) {
        keyboard->setTarget(price7_lineEdit);
        keyboard->show();
    }
    else if (sender() == price8_lineEdit) {
        keyboard->setTarget(price8_lineEdit);
        keyboard->show();
    }
    else if (sender() == price9_lineEdit) {
        keyboard->setTarget(price9_lineEdit);
        keyboard->show();
    }
    else if (sender() == busSerial_lineEdit) {
        keyboard->setTarget(busSerial_lineEdit);
        keyboard->show();
    }
    else if (sender() == busLine_lineEdit) {
        keyboard->setTarget(busLine_lineEdit);
        keyboard->show();
    }
    else if (sender() == ServerAddresLineedit) {
        keyboard->setTarget(ServerAddresLineedit);
        keyboard->show();
    }
    else if (sender() == ServerPortLineedit) {
        keyboard->setTarget(ServerPortLineedit);
        keyboard->show();
    }else if (sender() == ServerUrlLineedit) {
        keyboard->setTarget(ServerUrlLineedit);
        keyboard->show();
    }else if (sender() == CardSendRate_Lineedit) {
        keyboard->setTarget(CardSendRate_Lineedit);
        keyboard->show();
    }else if (sender() == AvlSendRate_Lineedit) {
        keyboard->setTarget(AvlSendRate_Lineedit);
        keyboard->show();
    }
}

void SettingsDialog::ReaderCountChanged(int count){
    newReaderCountAvailable(count+1);
    if(count == 0){
#ifdef TINY210
        setReaderIDButton1->setVisible(true);
        setReaderIDButton2->setVisible(false);
        setReaderIDButton3->setVisible(false);
#endif
        clearMemoryButton1->setVisible(true);
        clearMemoryButton2->setVisible(false);
        clearMemoryButton3->setVisible(false);
    }else if(count == 1){
#ifdef TINY210
        setReaderIDButton1->setVisible(true);
        setReaderIDButton2->setVisible(true);
        setReaderIDButton3->setVisible(false);
#endif
        clearMemoryButton1->setVisible(true);
        clearMemoryButton2->setVisible(true);
        clearMemoryButton3->setVisible(false);
    }else if(count == 2){
#ifdef TINY210
        setReaderIDButton1->setVisible(true);
        setReaderIDButton2->setVisible(true);
        setReaderIDButton3->setVisible(true);
#endif
        clearMemoryButton1->setVisible(true);
        clearMemoryButton2->setVisible(true);
        clearMemoryButton3->setVisible(true);
    }
}

void SettingsDialog::setDateTime(QDateTime dateTime){
    QProcess applyDateTimeProcess;
    applyDateTimeProcess.startDetached("date --set="+dateTime.toString("\"yyyy-MM-dd hh:mm:ss\""));// "23 June 1988 10:00:00"
    applyDateTimeProcess.startDetached("hwclock -w");

    MainView::instance()->messegeBox->setText("ساعت با ماهواره تنظیم شد");
    MainView::instance()->messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
    MainView::instance()->messegeBox->Show(3);
}

void SettingsDialog::setDateTimeFromGMT(QDateTime dateTime){
    QJalaliDate Jdate = QJalaliDate(dateTime.date());
    QDateTime _dateTime;
    if(Jdate.Month() <= 6 ) _dateTime = dateTime.addSecs(16200);
    else _dateTime = _dateTime = dateTime.addSecs(12600);

    QProcess applyDateTimeProcess;
    applyDateTimeProcess.startDetached("date --set="+_dateTime.toString("\"yyyy-MM-dd hh:mm:ss\""));// "23 June 1988 10:00:00"
    applyDateTimeProcess.startDetached("hwclock -w");

    MainView::instance()->messegeBox->setText("ساعت با ماهواره تنظیم شد");
    MainView::instance()->messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
    MainView::instance()->messegeBox->Show(3);
}
