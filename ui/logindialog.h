#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QResizeEvent>
#include <QPushButton>
#include "ui/lineedit.h"
#include "vkeyboard/vkeyboard.h"

class LoginDialog : public QDialog
{
    Q_OBJECT
public:
    typedef enum{ROOT_USER, ADMIN_1_USER, ADMIN_2_USER, ADMIN_3_USER, NONE_USER} USER_TYPE;

    explicit LoginDialog(QWidget *parent = 0);
    USER_TYPE user();

private:
    VKeyboard *keyboard;
    QLabel *back;
    LineEdit *userName_lineEdit;
    LineEdit *password_lineEdit;
    USER_TYPE userType;
    QPushButton *Login_Button;
    QPushButton *Close_Button;

protected:
    virtual void resizeEvent(QResizeEvent *event);
    
signals:
    void logedIn(USER_TYPE);
    
public slots:
    void logOut();

private slots:
    void setKeyboard();
    void On_MainButtons_Clicked();
    
};

#endif // LOGINDIALOG_H
