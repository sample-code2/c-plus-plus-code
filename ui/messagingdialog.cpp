#include "messagingdialog.h"
#include <mainview.h>

MessagingDialog::MessagingDialog(QWidget *parent) : QWidget(parent){

    resize(MainView::instance()->size());
    setLayoutDirection(Qt::RightToLeft);

    Create_SendPage();
}

void MessagingDialog::Create_SendPage(){
    SendPage = new QWidget(this);
    SendPage->resize(size());
    SendPage->setLayoutDirection(Qt::LeftToRight);


}
