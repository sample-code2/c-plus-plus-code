#ifndef MESSEGEBOX_H
#define MESSEGEBOX_H

#include <QWidget>
#include <QLabel>
#include <QTimer>
#include <QPropertyAnimation>

#define MESSEGEBOX_DEFAULT_BACKGROUND	"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(0, 0, 100, 80%));"
#define MESSEGEBOX_DEFAULT_STYLE       "font: 50px; color:rgb(0,255,0); border: 5px solid #000000;border-radius:20px;"

class MessegeBox : public QWidget
{
    Q_OBJECT
public:
    explicit MessegeBox(QWidget *parent = 0);

private:
    QLabel *Back;
    QLabel *Icon;
    QPropertyAnimation *FadeAnimation;
    QWidget *Parent;

protected:
    virtual void resizeEvent(QResizeEvent *event);

signals:

public slots:
    void setText(QString str);
    void Show(quint8 time);
    void Show();
    void endShow();
    void setIcon(QPixmap icone);

};

#endif // MESSEGEBOX_H
