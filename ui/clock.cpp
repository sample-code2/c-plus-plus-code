#include "clock.h"
#include "QTimer"
#include "QTime"
#include "QDebug"

Clock::Clock(QWidget *parent):QWidget(parent){
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
    timer->start(1000);

    back = new QLabel(this);
    back->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(0, 0, 50, 80%));"
                        "border: 4px solid #000000;border-radius:25px;border-color:rgb(255,0,0);");

    digits = new QLabel(this);
    digits->setStyleSheet("font-family: 'B Homa';color:rgb(255, 255, 255); font: bold 70px;");
    digits->setAlignment(Qt::AlignBottom | Qt::AlignCenter);
    digits->move(digits->x(), digits->y()+8);
    digits->setText("00:00");

    date = new QLabel(this);
    date->setStyleSheet("font-family: 'B Homa';color:rgb(255, 255, 0); font: bold 30px;");
    date->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    date->setMargin(10);
    date->setText("00/00/00");

    DayName = new QLabel(this);
    DayName->setStyleSheet("font-family: 'B Homa';color:rgb(0, 255, 0); font: 30px;");
    DayName->setAlignment(Qt::AlignTop | Qt::AlignRight);
    DayName->setMargin(10);
    DayName->setText("---");

    showTime();
}

void Clock::showTime(){
    QJalaliDate Jdate = QJalaliDate::currentDate();

    QString text = QTime::currentTime().toString("hh:mm");
    if ((QTime::currentTime().second() % 2) == 0)
        text[2] = ' ';

    //digits->display(text);
    digits->setText(text);
    date->setText(Jdate.toString());
    DayName->setText(Jdate.dayOfWeek_str());
}

void Clock::setDate(QJalaliDate dt){
    timer->stop();
    this->date->setText(dt.toString());
    DayName->setText(dt.dayOfWeek_str());
    digits->hide();
    date->setAlignment(Qt::AlignCenter | Qt::AlignBottom);
    date->setStyleSheet("font-family: 'B Homa';color:rgb(255, 255, 0); font: bold 50px;");

}

void Clock::start(){
    date->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    date->setStyleSheet("font-family: 'B Homa';color:rgb(255, 255, 0); font: bold 30px;");
    digits->show();
    showTime();
    timer->start();
}

void Clock::resizeEvent(QResizeEvent *event){
    digits->resize(this->size());
    date->resize(size());
    DayName->resize(size());
    back->resize(size());
    QWidget::resizeEvent(event);
    event->accept();
}
