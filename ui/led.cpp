#include "led.h"

LED::LED(QString text, QWidget *parent):QWidget(parent){
    resize(50, 42);

    pix = new QLabel(this);
    pix->resize(height()-15, height()-15);
    pix->move(width()/2-pix->width()/2, 0);
    pix->setPixmap(QPixmap(":/Graphics/led_black.png"));
    pix->setScaledContents(true);
    Color = Qt::black;

    QLabel *Title = new QLabel(this);
    Title->resize(size());
    Title->move(0, 0);
    Title->setStyleSheet("color:rgb(0, 255, 0, 100%); font:bold 15px;");
    Title->setAlignment(Qt::AlignBottom | Qt::AlignCenter);
    //Title->setAlignment(Qt::AlignCenter);
    Title->setText(text);
}

void LED::setColor(Qt::GlobalColor color){
    Color = color;
    if(color == Qt::blue)           pix->setPixmap(QPixmap(":/Graphics/led_blue.png"));
    else if(color == Qt::black)     pix->setPixmap(QPixmap(":/Graphics/led_black.png"));
    else if(color == Qt::darkBlue)  pix->setPixmap(QPixmap(":/Graphics/led_dark-blue.png"));
    else if(color == Qt::red)       pix->setPixmap(QPixmap(":/Graphics/led_red.png"));
    else if(color == Qt::darkRed)   pix->setPixmap(QPixmap(":/Graphics/led_dark_red.png"));
    else if(color == Qt::yellow)    pix->setPixmap(QPixmap(":/Graphics/led_yellow.png"));
    else if(color == Qt::darkYellow)pix->setPixmap(QPixmap(":/Graphics/led_dark_yellow.png"));
    else if(color == Qt::green)     pix->setPixmap(QPixmap(":/Graphics/led_green.png"));
    else if(color == Qt::darkGreen) pix->setPixmap(QPixmap(":/Graphics/led_dark_green.png"));
    else                            pix->setPixmap(QPixmap(":/Graphics/led_black.png"));
}

Qt::GlobalColor LED::color(){
    return Color;
}

void LED::setOn(){
    if(color() == Qt::darkBlue || color() == Qt::blue)     pix->setPixmap(QPixmap(":/Graphics/led_blue.png"));
    else if(color() == Qt::darkRed || color() == Qt::red)   pix->setPixmap(QPixmap(":/Graphics/led_red.png"));
    else if(color() == Qt::darkYellow || color() == Qt::yellow)pix->setPixmap(QPixmap(":/Graphics/led_yellow.png"));
    else if(color() == Qt::darkGreen || color() == Qt::green) pix->setPixmap(QPixmap(":/Graphics/led_green.png"));
}

void LED::setOff(){
    if(color() == Qt::darkBlue || color() == Qt::blue) pix->setPixmap(QPixmap(":/Graphics/led_dark-blue.png"));
    else if(color() == Qt::darkRed || color() == Qt::red) pix->setPixmap(QPixmap(":/Graphics/led_dark_red.png"));
    else if(color() == Qt::darkYellow || color() == Qt::yellow)pix->setPixmap(QPixmap(":/Graphics/led_dark_yellow.png"));
    else if(color() == Qt::darkGreen || color() == Qt::green) pix->setPixmap(QPixmap(":/Graphics/led_dark_green.png"));
}

void LED::setOnOff(bool cmd){
    if(cmd)setOn();
    else setOff();
}

