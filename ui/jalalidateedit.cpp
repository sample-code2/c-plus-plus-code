#include "jalalidateedit.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDate>

jalaliDateEdit::jalaliDateEdit(QWidget *parent):QWidget(parent){

    back = new QLabel(this);
    back->setStyleSheet("background-color:rgb(0, 0, 100, 80%);"
                        "border: 3px solid #000000;border-radius:10px;border-color:blue;");

    QVBoxLayout	*VLayout = new QVBoxLayout(this);
    QHBoxLayout	*HLayout = new QHBoxLayout();

    YearCombo = new QComboBox(this);
    YearCombo->setMaxVisibleItems(6);
    int StartYear = 1391;
    for(int i=0; i<10; i++){
        YearCombo->addItem(QString("%0").arg(StartYear+i), QVariant(StartYear+i));
    }
    HLayout->addWidget(YearCombo);

    MoonCombo = new QComboBox(this);
    MoonCombo->setMaximumWidth(90);
    MoonCombo->setMaxVisibleItems(6);
    for(int i=1; i<=12; i++){
        MoonCombo->addItem(QString("%0").arg(i), QVariant(i));
    }
    HLayout->addWidget(MoonCombo);

    DayCombo = new QComboBox(this);
    DayCombo->setMaxVisibleItems(6);
    for(int i=1; i<=31; i++){
        DayCombo->addItem(QString("%0").arg(i), QVariant(i));
    }
    HLayout->addWidget(DayCombo);

    VLayout->insertLayout(0 , HLayout);    
}

QDate jalaliDateEdit::date(){
    return QJalaliDate(YearCombo->itemData(YearCombo->currentIndex()).toUInt()
                       , MoonCombo->itemData(MoonCombo->currentIndex()).toUInt()
                       , DayCombo->itemData(DayCombo->currentIndex()).toUInt()
                       ).ToGregorian();
}

void jalaliDateEdit::setDate(QJalaliDate dt){
    YearCombo->setCurrentIndex(YearCombo->findData(dt.Year()));
    MoonCombo->setCurrentIndex(MoonCombo->findData(dt.Month()));
    DayCombo->setCurrentIndex(DayCombo->findData(dt.Day()));
}

void jalaliDateEdit::resizeEvent(QResizeEvent *event){
    back->resize(size());
    event->accept();
}
