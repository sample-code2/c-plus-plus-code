#ifndef DRIVERPICTURE_H
#define DRIVERPICTURE_H

#include <QWidget>
#include <QDebug>
#include <QLabel>
#include <QResizeEvent>

class DriverPicture : public QWidget
{
    Q_OBJECT
public:
    explicit DriverPicture(QWidget *parent = 0);

private:
    QLabel *Back;
    
protected:
    virtual void resizeEvent(QResizeEvent *event);

signals:
    
public slots:
    void setPixmap(QPixmap pix);
};

#endif // DRIVERPICTURE_H
