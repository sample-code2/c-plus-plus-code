#include "driverpicture.h"

DriverPicture::DriverPicture(QWidget *parent):QWidget(parent){
    Back = new QLabel(this);
    Back->resize(size());
    Back->setScaledContents(true);
    Back->setStyleSheet("border: 3px solid blue;"
                        "border-radius:15px;"
                        "background-color:rgb(200,200,200,150);");
    Back->setAlignment(Qt::AlignCenter);
    Back->setMargin(0);
    Back->setPixmap(QPixmap(":/Graphics/DefaultDriver.png"));
}

void DriverPicture::resizeEvent(QResizeEvent *event){
    Back->resize(this->size());
    event->accept();

}

void DriverPicture::setPixmap(QPixmap pix){
    if(pix.isNull()) Back->setPixmap(QPixmap(":/Graphics/DefaultDriver.png"));
    else Back->setPixmap(pix);
}
