#ifndef JALALIDATEEDIT_H
#define JALALIDATEEDIT_H

#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QResizeEvent>
#include "mylibs/jalalidate.h"

class jalaliDateEdit : public QWidget
{
    Q_OBJECT
public:
    explicit jalaliDateEdit(QWidget *parent = 0);
    QDate date();
private:
    QLabel * back;
    QComboBox* YearCombo;
    QComboBox* MoonCombo;
    QComboBox* DayCombo;

protected:
    virtual void resizeEvent(QResizeEvent *event);
    
signals:
    
public slots:
    void setDate(QJalaliDate dt);
    
};

#endif // JALALIDATEEDIT_H
