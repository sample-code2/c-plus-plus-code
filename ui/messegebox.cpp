#include "messegebox.h"
#include <QResizeEvent>
#include "QDebug"

MessegeBox::MessegeBox(QWidget *parent) :QWidget(parent){
    Parent = parent;
    Back = new QLabel(this);
    Back->setStyleSheet(QString(MESSEGEBOX_DEFAULT_BACKGROUND) + MESSEGEBOX_DEFAULT_STYLE);
    Back->setAlignment(Qt::AlignCenter);

    Icon = new QLabel(this);
    Icon->resize(120, 120);
    Icon->setMargin(10);
    Icon->setScaledContents(true);

    FadeAnimation  = new QPropertyAnimation(this, "pos", this);
    FadeAnimation->setDuration(600);

    hide();
}

void MessegeBox::setText(QString str){
    Back->setText(str);
}

void MessegeBox::Show(quint8 time){
    this->show();
    FadeAnimation->setStartValue(QPoint(50, -480));
    FadeAnimation->setEndValue(QPoint(50, 50));
    FadeAnimation->setEasingCurve(QEasingCurve::OutBack);
    FadeAnimation->start();
    QTimer::singleShot(time*1000, this, SLOT(endShow()));
}

void MessegeBox::Show(){
    FadeAnimation->setStartValue(QPoint(50, -480));
    FadeAnimation->setEndValue(QPoint(50, 50));
    FadeAnimation->setEasingCurve(QEasingCurve::OutBack);
    FadeAnimation->start();
    this->show();
}

void MessegeBox::endShow(){
    FadeAnimation->setStartValue(QPoint(50, 50));
    FadeAnimation->setEndValue(QPoint(50, 480));
    FadeAnimation->setEasingCurve(QEasingCurve::InBack);
    FadeAnimation->start();
    //this->hide();
}

void MessegeBox::setIcon(QPixmap icone){
    Icon->setPixmap(icone);
}

void MessegeBox::resizeEvent(QResizeEvent *event){
    Back->resize(this->size());

    event->accept();
}
