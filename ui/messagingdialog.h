#ifndef MESSAGINGDIALOG_H
#define MESSAGINGDIALOG_H

#include <QWidget>

class MessagingDialog : public QWidget
{
    Q_OBJECT
public:
    explicit MessagingDialog(QWidget *parent = 0);

private:
    void Create_SendPage();

    QWidget *SendPage;

signals:
    
public slots:
    
};

#endif // MESSAGINGDIALOG_H
