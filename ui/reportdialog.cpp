#include "reportdialog.h"
#include <QDate>
#include <QDebug>
#include <QtSql>
#include <QStandardItemModel>
#include <QHeaderView>
#include "mainview.h"
#include "mydatetime.h"
#include "Defines.h"
#include "mylibs/settings.h"

ReportDialog::ReportDialog(QWidget *parent) :QWidget(parent){
    messegbox=MainView::instance()->messegeBox;
    reportType= NONE_REPORT_TYPE;

    resize(MainView::instance()->size());

#ifdef MY210
    printer = new Printer("/dev/ttySAC2", Printer::E17_MODEL, this);
#endif

#ifdef TINY210
    printer = new Printer("/dev/ttySAC3", Printer::E17_MODEL, this);
#endif

    startTime = new QTimeEdit(this);
    startTime->resize(250, 75);
    startTime->setStyleSheet("font:bold 45px;");
    startTime->move(width()-startTime->width()-10, 25);
    startTime->setTime(QTime(0,0,0));

    QLabel *startLable = new QLabel(this);
    startLable->setText(":تاریخ شروع");
    startLable->setStyleSheet("font: 18px;color:green");
    startLable->resize(200, 18);
    startLable->setAlignment(Qt::AlignBottom | Qt::AlignRight);
    startLable->move(startTime->x()+startTime->width()-startLable->width(), startTime->y()-startLable->height());

    Startdate = new jalaliDateEdit(this);
    Startdate->resize(350, 75);
    Startdate->move(startTime->x()-Startdate->width()-10, startTime->y());
    Startdate->setDate(QJalaliDate::currentDate());

    endTime = new QTimeEdit(this);
    endTime->resize(250, 75);
    endTime->setStyleSheet("font:bold 45px;");
    endTime->move(startTime->x(), startTime->y()+startTime->height()+20);
    endTime->setTime(QTime(23,59,59));

    QLabel *endLable = new QLabel(this);
    endLable->setText(":تاریخ پایان");
    endLable->setStyleSheet("font: 18px;color:green");
    endLable->resize(200, 18);
    endLable->setAlignment(Qt::AlignBottom | Qt::AlignRight);
    endLable->move(endTime->x()+endTime->width()-endLable->width(), endTime->y()-endLable->height());

    Enddate=new jalaliDateEdit(this);
    Enddate->resize(350, 75);
    Enddate->move(Startdate->x(), endTime->y());
    Enddate->setDate(QJalaliDate::currentDate());

    QStringList headerStrings;
    headerStrings << "مبلغ تراکنش" << "تعداد تراکنش" << "کارکرد به تومان";
    reportTable = new QTableWidget(this);
    reportTable->setColumnCount(3);
    reportTable->resize(width()-Enddate->x()-15, height()-Enddate->y()-Enddate->height()-20);
    reportTable->move(Enddate->x(), Enddate->y()+Enddate->height()+10);
    reportTable->setColumnWidth(0, reportTable->width()/3-20);
    reportTable->setColumnWidth(1, reportTable->width()/3-20);
    reportTable->setColumnWidth(2, reportTable->width()/3-20);
    reportTable->setLayoutDirection(Qt::RightToLeft);
    reportTable->setHorizontalHeaderLabels(headerStrings);
    reportTable->setSelectionMode(QAbstractItemView::NoSelection);

    GetReportButton = new QPushButton(QIcon(":/Graphics/ok.png"), "", this);
    GetReportButton->resize(70, 70);
    GetReportButton->setIconSize(GetReportButton->size());
    GetReportButton->move(10, Enddate->y());
    GetReportButton->setFlat(true);
    connect(GetReportButton, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    PrintButton = new QPushButton(QIcon(":/Graphics/printer.png"), "", this);
    PrintButton->resize(70, 70);
    PrintButton->setIconSize(PrintButton->size());
    PrintButton->move(GetReportButton->x()+PrintButton->width()+10, GetReportButton->y());
    PrintButton->setFlat(true);
    connect(PrintButton, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    GetReportAllDriverButton = new QPushButton("همه\nرانندگان", this);
    GetReportAllDriverButton->resize(70, 70);
    GetReportAllDriverButton->move(GetReportButton->x(), GetReportButton->y()+GetReportAllDriverButton->height()+10);
    GetReportAllDriverButton->setFlat(true);
    connect(GetReportAllDriverButton, SIGNAL(clicked()), SLOT(On_Buttons_Clicked()));

    Shutdown_Button = new QPushButton(QIcon(":/Graphics/system-shutdown.png"), "", this);
    Shutdown_Button->resize(70, 70);
    Shutdown_Button->setIconSize(Shutdown_Button->size());
    Shutdown_Button->move(10, height()-Shutdown_Button->height()-10);
    Shutdown_Button->setFlat(true);
    connect(Shutdown_Button, SIGNAL(clicked()), MainView::instance()->powerController, SLOT(ShutDown()));
}

void ReportDialog::On_Buttons_Clicked(){

    if(sender() == GetReportButton) {
        QList<REPORT> reportList;
        REPORT tempReport;
        quint16 cardCount=0;
        quint16 totalCardCount=0;
        quint16 testCardCount=0;
        quint16 previousPrice=0;
        quint16 curentPrice=0;
        quint32 sumPrice=0;
        quint32 totalSumPrice=0;
        bool isfirst=true;

        if(QDateTime(Startdate->date(), startTime->time()) >= QDateTime(Enddate->date(), endTime->time())){
            messegbox->setText(QString("بازه تاریخ نامعتبر است"));
            messegbox->setIcon(QPixmap(":/Graphics/warning.png"));
            messegbox->Show(3);
            reportType = NONE_REPORT_TYPE;
            return;
        }
        QString cmd = QString("SELECT * FROM cardinfo where datetime >= %0 AND datetime <=%1 AND driverserial=%2 GROUP BY datetime, rfid, remainprice order by price;")
                .arg(MyDateTime::createDateTime(QDateTime(Startdate->date(), startTime->time())))
                .arg(MyDateTime::createDateTime(QDateTime(Enddate->date(), endTime->time())))
                .arg(Settings::driverId());
        qLog << cmd;
        QSqlQuery query(cmd, QSqlDatabase::database("Local_Eticket_Sqlite"));

        reportTable->clear();
        QStringList headerStrings;
        headerStrings << "مبلغ تراکنش" << "تعداد تراکنش" << "کارکرد به تومان";
        reportTable->setHorizontalHeaderLabels(headerStrings);
        /********************************start creating report list***********************/
        reportList.clear();
        if(query.first()){
            while(query.isValid()){
                /**************check for is test*******************/
                if(query.record().value("cardtype").toUInt() == 9){
                    ++testCardCount;
                    query.next();
                    continue;
                }
                curentPrice = query.record().value("price").toUInt();
                if(curentPrice != previousPrice && !isfirst){
                    tempReport.price = previousPrice;
                    tempReport.numberOfCards = cardCount;
                    tempReport.sumPrice = sumPrice;
                    reportList.append(tempReport);
                    cardCount=0;
                    sumPrice=0;
                }
                cardCount++;
                totalCardCount++;
                sumPrice += curentPrice;
                totalSumPrice += curentPrice;

                isfirst=false;
                previousPrice = curentPrice;
                query.next();
                qLog << sumPrice << cardCount << reportList.size();
            }
            tempReport.price = previousPrice;
            tempReport.numberOfCards = cardCount;
            tempReport.sumPrice = sumPrice;
            reportList.append(tempReport);
        }
        /********************************end creating report list***********************/
        reportTable->setRowCount(reportList.length());
        for(quint8 row=0; row < reportList.length(); ++row){
            reportTable->setItem(row, 0, new QTableWidgetItem(QString("%0 تومانی").arg(reportList[row].price)));
            reportTable->setItem(row, 1, new QTableWidgetItem(QString("%0 عدد").arg(reportList[row].numberOfCards)));
            reportTable->setItem(row, 2, new QTableWidgetItem(QString("%0 تومان").arg(reportList[row].sumPrice)));
        }
        if(testCardCount>0){
            reportTable->setRowCount(reportTable->rowCount()+1);
            reportTable->setItem(reportTable->rowCount()-1, 0, new QTableWidgetItem("کارت تست"));
            reportTable->setItem(reportTable->rowCount()-1, 1, new QTableWidgetItem(QString("%0 عدد").arg(testCardCount)));
            reportTable->setItem(reportTable->rowCount()-1, 2, new QTableWidgetItem("کارکرد ندارد"));
        }

        reportTable->setRowCount(reportTable->rowCount()+1);
        reportTable->setItem(reportTable->rowCount()-1, 0, new QTableWidgetItem("جمع کل"));
        reportTable->setItem(reportTable->rowCount()-1, 1, new QTableWidgetItem(QString("%0 عدد").arg(totalCardCount)));
        reportTable->setItem(reportTable->rowCount()-1, 2, new QTableWidgetItem(QString("%0 تومان").arg(totalSumPrice)));

        reportType = THIS_DRIVER_REPORT_TYPE;

    }else if(sender() == GetReportAllDriverButton){
        if(QDateTime(Startdate->date(), startTime->time()) >= QDateTime(Enddate->date(), endTime->time())){
            messegbox->setText(QString("بازه تاریخ نامعتبر است"));
            messegbox->setIcon(QPixmap(":/Graphics/warning.png"));
            messegbox->Show(3);
            reportType = NONE_REPORT_TYPE;
            return;
        }
        if(MainView::instance()->loginDialog->user() != LoginDialog::ADMIN_3_USER && MainView::instance()->loginDialog->user() != LoginDialog::ROOT_USER){//@temp
            MainView::instance()->loginDialog->exec();
            if(MainView::instance()->loginDialog->user() != LoginDialog::ADMIN_3_USER && MainView::instance()->loginDialog->user() != LoginDialog::ROOT_USER){
                messegbox->setText(QString("لطفا نام کاربری و رمز عبور\nصحیح را وارد کنید"));
                messegbox->setIcon(QPixmap(":/Graphics/warning.png"));
                messegbox->Show(3);
                reportType = NONE_REPORT_TYPE;
                return;
            }
        }
        QList<DRIVER_REPORT> DriverReportList;
        bool isfirst=true;
        quint64 previousDriver=0;
        quint64 curentDriver=0;
        DRIVER_REPORT tempDriverReport;
        quint16 cardCount=0;
        quint16 totalCardCount=0;
        quint32 sumPrice=0;
        quint32 totalSumPrice=0;
        quint16 curentPrice=0;

        QString cmd = QString("SELECT * FROM cardinfo where datetime >= %0 AND datetime <=%1 AND cardtype!=9 GROUP BY datetime, rfid, remainprice order by driverserial;")
                .arg(MyDateTime::createDateTime(QDateTime(Startdate->date(), startTime->time())))
                .arg(MyDateTime::createDateTime(QDateTime(Enddate->date(), endTime->time())));
        qLog << cmd;
        QSqlQuery query(cmd, QSqlDatabase::database("Local_Eticket_Sqlite"));

        reportTable->clear();
        QStringList headerStrings;
        headerStrings << "راننده" << "تعداد تراکنش" << "کارکرد به تومان";
        reportTable->setHorizontalHeaderLabels(headerStrings);
        /********************************start creating report list***********************/
        DriverReportList.clear();
        if(query.first()){
            while(query.isValid()){
                curentPrice = query.record().value("price").toUInt();
                curentDriver = query.record().value("driverserial").toUInt();
                if(curentDriver != previousDriver && !isfirst){
                    tempDriverReport.driverName = Settings::driverName(previousDriver);
                    tempDriverReport.numberOfCards = cardCount;
                    tempDriverReport.sumPrice = sumPrice;
                    DriverReportList.append(tempDriverReport);
                    cardCount=0;
                    sumPrice=0;
                }
                cardCount++;
                totalCardCount++;
                sumPrice += curentPrice;
                totalSumPrice += curentPrice;

                isfirst=false;
                previousDriver = curentDriver;
                query.next();
            }
            tempDriverReport.driverName = Settings::driverName(previousDriver);
            tempDriverReport.numberOfCards = cardCount;
            tempDriverReport.sumPrice = sumPrice;
            DriverReportList.append(tempDriverReport);
        }
        /********************************end creating report list***********************/
        reportTable->setRowCount(DriverReportList.length());
        for(quint8 row=0; row < DriverReportList.length(); ++row){
            reportTable->setItem(row, 0, new QTableWidgetItem(QString(DriverReportList[row].driverName)));
            reportTable->setItem(row, 1, new QTableWidgetItem(QString("%0 عدد").arg(DriverReportList[row].numberOfCards)));
            reportTable->setItem(row, 2, new QTableWidgetItem(QString("%0 تومان").arg(DriverReportList[row].sumPrice)));
        }
        reportTable->setRowCount(reportTable->rowCount()+1);
        reportTable->setItem(reportTable->rowCount()-1, 0, new QTableWidgetItem("همه راننده ها(جمع کل)"));
        reportTable->setItem(reportTable->rowCount()-1, 1, new QTableWidgetItem(QString("%0 عدد").arg(totalCardCount)));
        reportTable->setItem(reportTable->rowCount()-1, 2, new QTableWidgetItem(QString("%0 تومان").arg(totalSumPrice)));
        reportType = ALL_DRIVERS_REPORT_TYPE;
    }else if(sender() == PrintButton) {
        if(reportType == NONE_REPORT_TYPE){
            messegbox->setText(QString("گزارشی گرفته نشده است"));
            messegbox->setIcon(QPixmap(":/Graphics/warning.png"));
            messegbox->Show(3);
            return;
        }
        QString temp_Str, temp_Str2;
        printer->initPrinter();
        printer->ReversePrint(true);
        printer->print_EN("--------------------------------");
        printer->sendEnter(1);
        temp_Str="سازمان اتوبوسرانی تبریز و حومه";
        printer->printSpace((32-temp_Str.length())/2);
        printer->print_FA(temp_Str);
        printer->sendEnter(1);
        printer->printSpace((32-24)/2);
        printer->printDateTime(QDateTime::currentDateTime());
        printer->print_EN(" : ");
        printer->print_FA("تاریخ");
        printer->sendEnter(1);
        temp_Str="**********";
        printer->printSpace((32-temp_Str.length())/2);
        printer->print_EN(temp_Str);
        printer->sendEnter(1);
        printer->printSpace(32-28);
        printer->printDateTime(QDateTime(Startdate->date(), startTime->time()));
        printer->print_EN(" : ");
        printer->print_FA("از تاریخ");
        printer->sendEnter(1);
        printer->printSpace(32-28);
        printer->printDateTime(QDateTime(Enddate->date(), endTime->time()));
        printer->print_EN(" : ");
        printer->print_FA("تا تاریخ");
        printer->sendEnter(1);
        temp_Str="شماره اتوبوس";
        temp_Str2=QString("%0").arg(Settings::BusSerial());
        printer->printSpace(32-temp_Str.length()-temp_Str2.length()-3);
        printer->print_FA(temp_Str2);
        printer->print_EN(" : ");
        printer->print_FA(temp_Str);
        printer->sendEnter(1);
        if(reportType == THIS_DRIVER_REPORT_TYPE){
            temp_Str="نام راننده";
            temp_Str2=driverName(Settings::driverId());
            printer->printSpace(32-temp_Str.length()-temp_Str2.length()-3);
            printer->print_FA(temp_Str2);
            printer->print_EN(" : ");
            printer->print_FA(temp_Str);
            printer->sendEnter(1);
            temp_Str="کارت راننده";
            temp_Str2=QString("%0").arg(Settings::driverId());
            printer->printSpace(32-temp_Str.length()-temp_Str2.length()-3);
            printer->printNumber_FA(temp_Str2);
            printer->print_EN(" : ");
            printer->print_FA(temp_Str);
            printer->sendEnter(1);
            for(quint16 i=0; i< reportTable->rowCount(); ++i){
                temp_Str="-----------------";
                printer->printSpace((32-temp_Str.length())/2);
                printer->print_EN(temp_Str);
                printer->sendEnter(1);
                temp_Str="مبلغ";
                temp_Str2=reportTable->item(i, 0)->text();
                printer->printSpace(32-temp_Str.length()-temp_Str2.length()-3);
                printer->print_FA(temp_Str2);
                printer->print_EN(" : ");
                printer->print_FA(temp_Str);
                printer->sendEnter(1);

                temp_Str=reportTable->item(i, 1)->text();
                temp_Str2=reportTable->item(i, 2)->text();
                printer->printSpace(32-temp_Str.length()-temp_Str2.length()-3);
                printer->print_FA(temp_Str2);
                printer->print_EN("<--");
                printer->print_FA(temp_Str);
                printer->sendEnter(1);
            }
            temp_Str="-----------------";
            printer->printSpace((32-temp_Str.length())/2);
            printer->print_EN(temp_Str);
            printer->sendEnter(1);
        }else if(reportType == ALL_DRIVERS_REPORT_TYPE){
            for(quint16 i=0; i< reportTable->rowCount(); ++i){
                temp_Str="-----------------";
                printer->printSpace((32-temp_Str.length())/2);
                printer->print_EN(temp_Str);
                printer->sendEnter(1);
                temp_Str="راننده";
                temp_Str2=reportTable->item(i, 0)->text();
                printer->printSpace(32-temp_Str.length()-temp_Str2.length()-3);
                printer->print_FA(temp_Str2);
                printer->print_EN(" : ");
                printer->print_FA(temp_Str);
                printer->sendEnter(1);

                temp_Str=reportTable->item(i, 1)->text();
                temp_Str2=reportTable->item(i, 2)->text();
                printer->printSpace(32-temp_Str.length()-temp_Str2.length()-3);
                printer->print_FA(temp_Str2);
                printer->print_EN("<--");
                printer->print_FA(temp_Str);
                printer->sendEnter(1);
            }
            temp_Str="-----------------";
            printer->printSpace((32-temp_Str.length())/2);
            printer->print_EN(temp_Str);
            printer->sendEnter(1);
        }
        temp_Str="**********";
        printer->printSpace((32-temp_Str.length())/2);
        printer->print_EN(temp_Str);
        printer->sendEnter(1);
        temp_Str="شرکت طراحان کنترل شرق";
        printer->printSpace((32-temp_Str.length())/2);
        printer->print_FA(temp_Str);
        printer->sendEnter(1);
        temp_Str="www.tcco.ir";
        printer->printSpace((32-temp_Str.length())/2);
        printer->print_EN(temp_Str);
        printer->sendEnter(1);
        printer->print_EN("--------------------------------");
        printer->newLine(10);
        printer->sendEnter(1);
    }
}

QString ReportDialog::driverName(quint32 serial){
    QSqlQuery q(QString("SELECT name FROM drivers WHERE RFID = %0 ;").arg(serial), QSqlDatabase::database("Local_Bus_Sqlite"));
    q.first();
    if(q.record().value("name").isNull()){
        return "ثبت نشده";
    }else{
        return q.record().value("name").toString();
    }
}

void ReportDialog::clearTable(){
    reportTable->clear();
}
