#ifndef CLOCK_H
#define CLOCK_H

#include <QWidget>
#include "QLCDNumber"
#include "QLabel"
#include "QResizeEvent"
#include "mylibs/jalalidate.h"

class Clock : public QWidget
{
    Q_OBJECT
public:
    explicit Clock(QWidget *parent = 0);
    
private:
    QLabel * back;
    QTimer *timer;
    QLabel *digits;
    QLabel *date;
    QLabel *DayName;

protected:
    virtual void resizeEvent(QResizeEvent *event);

signals:
    
public slots:
    void setDate(QJalaliDate date);
    void start();

private slots:
    void showTime();
    
};

#endif // CLOCK_H
