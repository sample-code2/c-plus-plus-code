#include "numbericlable.h"

NumbericLable::NumbericLable(QWidget *parent) :QWidget(parent){
    Back = new QLabel(this);
    Back->resize(size());
    Back->setAlignment(Qt::AlignTop | Qt::AlignCenter);
    Back->setMargin(10);
    Back->setText("-");
    Back->setStyleSheet("font:bold 50px;"
                        "color:darkblue;"
                        "border: 3px solid #000000;"
                        "border-radius:15px;"
                        "border-color:blue;"
                        "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(200, 200, 255, 30%));");

    Text = new QLabel(this);
    Text->resize(size());
    Text->setMargin(3);
    Text->setAlignment(Qt::AlignCenter | Qt::AlignBottom);
    Text->setStyleSheet("font: 20px;color:rgb(255,255,255);");
}

void NumbericLable::resizeEvent(QResizeEvent *event){
    Back->resize(this->size());
    Text->resize(this->size());

    event->accept();
}

void NumbericLable::setLable(QString text){
    Text->setText(text);
}

void NumbericLable::setText(QString num){
    Back->setText(num);
}

void NumbericLable::setInUse(){
    Back->setStyleSheet("font:bold 50px;"
                        "color:darkblue;"
                        "font-family: 'B Homa';"
                        "border: 3px solid blue;"
                        "border-radius:15px;"
                        "border-color:blue;"
                        "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(255, 0, 0, 80%));");
    Text->setStyleSheet("font: 20px;font-family: 'B Homa';color:rgb(0,255,0);");
}

void NumbericLable::setDefault(){
    Back->setStyleSheet("font:bold 50px;"
                        "color:darkblue;"
                        "font-family: 'B Homa';"
                        "border: 3px solid blue;"
                        "border-radius:15px;"
                        "border-color:blue;"
                        "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(200, 200, 255, 30%));");
    Text->setStyleSheet("font: 20px;font-family: 'B Homa';color:rgb(255,255,255);");
}
