#include "antenawidget.h"
#include <QVariant>

AntenaWidget::AntenaWidget(QString text, QWidget *parent) :QWidget(parent){
    resize(42, 42);

    pix = new QLabel(this);
    pix->resize(width()-5, height()-10);
    pix->move(width()/2-pix->width()/2, 0);
    pix->setPixmap(QPixmap(":/Graphics/0.png"));
    pix->setScaledContents(true);

    QLabel *Title = new QLabel(this);
    Title->resize(size());
    Title->move(0, 0);
    Title->setStyleSheet("color:rgb(0, 255, 0, 100%); font:bold 12px;");
    Title->setAlignment(Qt::AlignBottom | Qt::AlignCenter);
    Title->setText(text);

    flashTimer = new QTimer(this);
    connect(flashTimer, SIGNAL(timeout()), SLOT(showHide()));
    flashTimer->setInterval(500);


}

void AntenaWidget::SetValue(quint8 value){
    if(value>=90) pix->setPixmap(QPixmap(":/Graphics/100.png"));
    else if(value>=60)  pix->setPixmap(QPixmap(":/Graphics/75.png"));
    else if(value>=40)  pix->setPixmap(QPixmap(":/Graphics/50.png"));
    else if(value>=20)  pix->setPixmap(QPixmap(":/Graphics/25.png"));
    else pix->setPixmap(QPixmap(":/Graphics/0.png"));
}

void AntenaWidget::showHide(){
    pix->setVisible(!pix->isVisible());
}

void AntenaWidget::setFixShow(bool active){
    if(active){
        flashTimer->stop();
        pix->setVisible(true);
    }
    else flashTimer->start();
}
