#include "messagebox.h"
#include "QSizePolicy"

MessageBox::MessageBox(QWidget *parent):QMessageBox(parent){
    setWindowFlags(Qt::FramelessWindowHint);
    setLayoutDirection(Qt::LeftToRight);
    setStyleSheet("font: 30px; color:rgb(0,255,0);font-family:'B Homa'; background:transparent;");
    setFixedSize(size());

    Back = new QLabel(this);
    Back->setAlignment(Qt::AlignCenter);
    Back->setStyleSheet(QString(DEFAULT_BACKGROUND) + DEFAULT_STYLE);
    Back->lower();

    FadeAnimation  = new QPropertyAnimation(this, "pos", this);
    FadeAnimation->setDuration(600);
}

void MessageBox::resizeEvent(QResizeEvent *event){
    Back->resize(size());
    QMessageBox::resizeEvent(event);
    event->accept();
}
