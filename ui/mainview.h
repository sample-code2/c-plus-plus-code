#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QtSql>
#include <QSound>
#include <QProgressBar>
#include <QTextEdit>
#include "Defines.h"
//#include <QSocketNotifier>//@temp
#include "ui/settingsdialog.h"
#include "ui/messagingdialog.h"
#include "ui/reportdialog.h"
#include "vkeyboard/vkeyboard.h"
#include "MyTypes.h"
#include "messegebox.h"
#include "messagebox.h"
#include "numbericlable.h"
#include "clock.h"
#include "driverpicture.h"
#include "logindialog.h"
#include "mylibs/updater.h"
#include "mylibs/readers.h"
#include "gsm.h"
#include "gps.h"
#include "datatransfer.h"
#include "startloadingdialog.h"
#include "powercontroller.h"

class MainView : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainView(StartLoadingDialog *sld, QWidget *parent = 0);
    typedef enum {PrevoiusDay, NextDay, Today, ThisDriver}ReportType;
    static MainView *instance(){ return _instance;}
    VKeyboard *_keyboard;
    MessegeBox *messegeBox;
    MessageBox *msgBox;
    LoginDialog *loginDialog;
    QProgressBar *ProgressBar;
    PowerController *powerController;
    GSM *_gsm;
    GPS *_gps;

private:
    void insertCrashedData(quint8 Reader);

    static MainView *_instance;

    QSize LcdResolation;
    QRect ScreenRect;
    Readers *readers;
    Updater *updater;
    QWidget *homeWidgets;
    Clock *clock;
    DriverPicture *picture;
    SettingsDialog *settingsDialog;
    ReportDialog *reportDialog;
    MessagingDialog *messagingDialog;
    QLabel *BackLable;
    QPushButton *BackButton;
    QPushButton *mainButton_1;
    QPushButton *mainButton_2;
    QPushButton *mainButton_3;
    QPushButton *mainButton_4;
    QPushButton *mainButton_5;
    QPushButton *mainButton_6;
    QPushButton *yesButton;
    NumbericLable *cardCount1_lable;
    NumbericLable *cardCount2_lable;
    NumbericLable *cardCount_lable;
    NumbericLable *PriceCount_lable;
    QLabel *driverNameLablel;
    QLabel *SimChargeValueLablel;
    QLabel *IMEI_Lablel;
    QLabel *BusSerial_Lablel;
    quint16 cardCount1;
    quint16 cardCount2;
    quint32 PriceCount;
    quint8 DayCount;
    QTextEdit *testTextEdit;
    DataTransfer_Thread *_dataTransfer;
    QSound *ringSound;
    //    QSocketNotifier *notifier;//@temp
    //    QFile *buttonFile;//@temp
    //    int m_fd;//@temp

    void createHome();
    
signals:
    void newMessage(QString str);

private slots:
    void On_MainButtons_Clicked();
    void UpdateCounters(QList<Readers::CARD_INFO> info);
    void driverLogined(quint64 serial);
    void readerIdSent(quint8 id);
    void readerFormat_Ok(quint8 id);
    void readerConfig_Ok(quint8 id);
    void newCardSent(quint8 count);
    bool Report(ReportType reportType);
    void clearCounters();
    void updateValues();
    void incomingCall();
    
public slots:
    void test(QString str=QString(""));

};

#endif // MAINVIEW_H
