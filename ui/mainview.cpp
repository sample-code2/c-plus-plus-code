#include "mainview.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QFontDatabase>
#include <QTextCodec>
#include <QWSServer>
#include <QMessageBox>
#include <fcntl.h>
#include "antenawidget.h"
#include "led.h"
#include "settings.h"
#include "stdio.h"
#include "delay.h"
#include "mydatetime.h"
#include "wathchdog.h"
#include "eventlog.h"


MainView* MainView::_instance;

MainView::MainView(StartLoadingDialog *sld, QWidget *parent):QMainWindow(parent){
    /*مقداردهی های اولیه*/
    _instance = this;
    LcdResolation = QSize(800, 480);
    cardCount1=0;
    cardCount2=0;
    PriceCount=0;
    DayCount=0;

    qDebug() << QSqlDatabase::drivers();

    connect(this, SIGNAL(newMessage(QString)), sld, SLOT(showMessage(QString)));

    resize(LcdResolation);

    setWindowFlags(Qt::FramelessWindowHint);

    QFontDatabase::addApplicationFont(":/fonts/BHoma.ttf");
    QFontDatabase::addApplicationFont(":/fonts/BTitrBd.ttf");
    QFontDatabase::addApplicationFont(":/fonts/arial.ttf");

    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    setFont(QFont(":/fonts/BHoma.ttf"));

    QFile styleFile(":/style.css");
    styleFile.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(styleFile.readAll());
    setStyleSheet(styleSheet);
    styleFile.close();

    WathchDog *watchDog=new WathchDog(this);
    watchDog->startWhatching();

    powerController = new PowerController(this);

    messegeBox = new MessegeBox(this);
    messegeBox->resize(width()-100, height()-100);

    msgBox = new MessageBox(this);
    yesButton = msgBox->addButton("بله", QMessageBox::YesRole);
    msgBox->addButton("انصراف", QMessageBox::RejectRole);
    msgBox->hide();

    ringSound = new QSound("./sounds/ring.wav", this);

    emit newMessage("در حال باز کردن پایگاه داده");
    /*باز کردن بانک اطلاعاتی*/
    QSqlDatabase Sqlite_Eticket_DB = QSqlDatabase::addDatabase("QSQLITE", "Local_Eticket_Sqlite");
    Sqlite_Eticket_DB.setDatabaseName("./DB/E_Ticket.db");

    QSqlDatabase Sqlite_Avl_DB = QSqlDatabase::addDatabase("QSQLITE", "Local_Avl_Sqlite");
    Sqlite_Avl_DB.setDatabaseName("./DB/Avl.db");

    QSqlDatabase Sqlite_Send_DB = QSqlDatabase::addDatabase("QSQLITE", "Local_Send_Sqlite");
    Sqlite_Send_DB.setDatabaseName("./DB/Send.db");

    QSqlDatabase Sqlite_Bus_DB = QSqlDatabase::addDatabase("QSQLITE", "Local_Bus_Sqlite");
    Sqlite_Bus_DB.setDatabaseName("./DB/Bus.db");

    emit newMessage("آماده سازی کارت خوان ها");
#ifdef MY210
    readers = new Readers("/dev/ttySAC3", this);
#endif

#ifdef TINY210
    readers = new Readers("/dev/ttySAC2", this);
#endif

    emit newMessage("در حال پیکربندی");
    _gsm = new GSM("/dev/ttySAC1", this);
    _gsm->setAPN("mtnirancell");
    //_gsm->setAPN("tcco.ir");

    emit newMessage("GPS در حال پیکربندی");
    _gps = new GPS("/dev/ttySAC0",this);

    _dataTransfer = new DataTransfer_Thread(this);
    _dataTransfer->start();

    BackLable = new QLabel(this);
    BackLable->resize(LcdResolation);
    BackLable->move(0, 0);
    BackLable->setPixmap(QPixmap(":/Graphics/tt.png"));
    BackLable->setScaledContents(true);

    _keyboard = new VKeyboard(this, this);
    _keyboard->resize(500,250);
    _keyboard->move(width()/2-_keyboard->width()/2, height()-_keyboard->height());
    _keyboard->hide();

    loginDialog = new LoginDialog(this);
    loginDialog->move(width()/2-loginDialog->width()/2, height()/2-loginDialog->height()/2-100);
    loginDialog->hide();

    /*create home widgets*/
    createHome();

    /*Upgrade process need to createHome() for progerssBar*/
    updater=new Updater(this);

    settingsDialog = new SettingsDialog(this);
    settingsDialog->hide();

    reportDialog = new ReportDialog(this);
    reportDialog->hide();

    messagingDialog = new MessagingDialog(this);
    messagingDialog->hide();

    BackButton = new QPushButton(QIcon(":/Graphics/go-previous.png"), "", this);
    BackButton->resize(80, 80);
    BackButton->setIconSize(BackButton->size());
    BackButton->move(5, 5);
    BackButton->setFlat(true);
    BackButton->hide();

    loginDialog->raise();
    _keyboard->raise();
    msgBox->raise();
    messegeBox->raise();

    //    m_fd = open("/dev/buttons", O_RDONLY | O_NONBLOCK);//@temp
    //    if (m_fd < 0) {
    //        QMessageBox::information(this,"Error", "Fail to open /dev/buttons.");
    //    }else{
    //        notifier = new QSocketNotifier(m_fd, QSocketNotifier::Read, this);
    //        connect (notifier, SIGNAL(activated(int)), this, SLOT(On_MainButtons_Clicked()));
    //    }

    connect(BackButton, SIGNAL(clicked()), SLOT(On_MainButtons_Clicked()));

    /**************************************************************************/
    connect(readers, SIGNAL(newCardData_Recived(QList<Readers::CARD_INFO>)), SLOT(UpdateCounters(QList<Readers::CARD_INFO>)));
    connect(readers, SIGNAL(driverLogined(quint64)), SLOT(driverLogined(quint64)));
    connect(readers, SIGNAL(readerIdSent(quint8)), SLOT(readerIdSent(quint8)));
    connect(readers, SIGNAL(FormatOk(quint8)), SLOT(readerFormat_Ok(quint8)));
    connect(readers, SIGNAL(ConfigOk(quint8)), SLOT(readerConfig_Ok(quint8)));

    /**************************************************************************/
    connect(settingsDialog, SIGNAL(newReaderSettingAvailable()), readers, SLOT(newConfigAvailable()));
#ifdef MY210
    connect(settingsDialog, SIGNAL(readerIdAvailable()), readers, SLOT(applyReaderId()));
#elif defined TINY210
    connect(settingsDialog, SIGNAL(readerIdAvailable(quint8)), readers, SLOT(applyReaderId(quint8)));
#endif
    connect(settingsDialog, SIGNAL(newReaderCountAvailable(quint8)), readers, SLOT(newReaderCountAvailable(quint8)));
    connect(settingsDialog, SIGNAL(ReaderFormatAvailable(quint8)), readers, SLOT(formatReader(quint8)));
    connect(settingsDialog, SIGNAL(newReaderSettingAvailable()), this, SLOT(updateValues()));

    /**************************************************************************/
    connect(_gsm, SIGNAL(New_pppIp_Available()), _dataTransfer, SIGNAL(New_pppIp_Available()));//@temp
    connect(_gsm, SIGNAL(closeMySql_request()), _dataTransfer, SIGNAL(closeMySql_request()));//@temp
    connect(_gsm, SIGNAL(ringing()), SLOT(incomingCall()));//@temp

    /**************************************************************************/
    connect(_gps, SIGNAL(BusMoved()), SLOT(clearCounters()));
    connect(_gps, SIGNAL(dataNotRecived()), _gsm, SLOT(reconfig()));
    connect(_gps, SIGNAL(newDateTime(QDateTime)), settingsDialog, SLOT(setDateTimeFromGMT(QDateTime)));
}

void MainView::On_MainButtons_Clicked(){
    if(sender() == mainButton_1){
        BackButton->show();
        homeWidgets->hide();
        reportDialog->show();
        //Report(ThisDriver);
    }else if (sender() == mainButton_2) {
        Report(Today);
    }else if (sender() == mainButton_3) {
        Report(PrevoiusDay);
        qLog << "pre";
    }else if (sender() == mainButton_4) {
        if(loginDialog->user() == LoginDialog::ADMIN_1_USER || loginDialog->user() == LoginDialog::ROOT_USER){//@temp
            BackButton->show();
            homeWidgets->hide();
            settingsDialog->show();
            _keyboard->hide();
        }else{
            loginDialog->exec();
            if(loginDialog->user() == LoginDialog::ADMIN_1_USER || loginDialog->user() == LoginDialog::ROOT_USER){
                mainButton_4->click();
            }
        }
    }else if (sender() == mainButton_5) {
        clearCounters();
        //updater->startUpdate("QtcpChat_Win");//@temp
    }else if (sender() == mainButton_6) {
        Report(NextDay);
        //BackButton->show();
        //homeWidgets->hide();
        //messagingDialog->show();
    }else if (sender() == BackButton) {
        BackButton->hide();
        settingsDialog->hide();
        reportDialog->hide();
        messagingDialog->hide();
        homeWidgets->show();
        _keyboard->hide();
        loginDialog->logOut();
    }
    //    else if (sender() == notifier) {
    //        char buffer[8];
    //        memset(buffer, 0, sizeof buffer);
    //        read(m_fd, buffer, sizeof buffer);
    //        qLog << buffer;
    //    }
}

void MainView::UpdateCounters(QList<Readers::CARD_INFO> info){
    for(quint16 i=0; i<info.length(); i++)    {
        if(info.at(i).Reader_ID == 1) cardCount1++;
        else cardCount2++;
        PriceCount+=info.at(i).Price;
    }

    cardCount1_lable->setText(QString("%0").arg(cardCount1));
    cardCount2_lable->setText(QString("%0").arg(cardCount2));
    cardCount_lable->setText(QString("%0").arg(cardCount1+cardCount2));
    PriceCount_lable->setText(QString("%0").arg(PriceCount));

    _dataTransfer->clearCardSendCounter();
}

void MainView::driverLogined(quint64 serial){    
    if(serial == Settings::driverId()){
        MessageBox msgBox(this);
        msgBox.setText(QString("راننده گرامی %0\n شما از سیستم خارج میشوید").arg(Settings::driverName(serial)));
        msgBox.setInformativeText("آیا مطمئنید؟");
        QPushButton *yesButton = msgBox.addButton("بله", QMessageBox::YesRole);
        msgBox.addButton("انصراف", QMessageBox::RejectRole);
        msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
        msgBox.exec();
        if(msgBox.clickedButton() == yesButton){
            qLog << "Driver exit from system";
            EventLog::Log_DriverLogout();
            Settings::setDriverId(UnknownDriver);
            driverNameLablel->setStyleSheet("color:rgb(255,0,0); font: 25px");
            driverNameLablel->setText("راننده گرامی لطفا وارد شوید");
            picture->setPixmap(QPixmap());
            messegeBox->setText(QString("راننده گرامی %0\n خروج با موفقیت انجام شد").arg(Settings::driverName(serial)));
            messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
            messegeBox->Show(3);
            for(quint8 i=1; i<=3; i++){
                Settings::setReaderDriverIdAvalable(i, true);
            }
        }
    }else{
        if(Settings::driverId() != UnknownDriver) EventLog::Log_DriverLogout();
        Settings::setDriverId(serial);
        EventLog::Log_DriverLogin();
        driverNameLablel->setStyleSheet("color:rgb(0,255,0); font: 25px");
        driverNameLablel->setText(QString("راننده:%0").arg(Settings::driverName(serial)));
        picture->setPixmap(QPixmap(QString("pics/%0").arg(serial)));
        messegeBox->setText(QString("راننده گرامی %0\n ورود با موفقیت انجام شد").arg(Settings::driverName(serial)));
        messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
        messegeBox->Show(3);
        for(quint8 i=1; i<=3; i++){
            Settings::setReaderDriverIdAvalable(i, true);
        }
    }

    clearCounters();
    reportDialog->clearTable();
    BackButton->click();
}

void MainView::readerIdSent(quint8 id){
    messegeBox->setText(QString("آیدی کارتخوان شماره %0 تنظیم شد").arg(id));
    messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
    messegeBox->Show(3);
}

void MainView::readerFormat_Ok(quint8 id){
    messegeBox->setText(QString("اطلاعات کارتخوان شماره %0 فرمت شد").arg(id));
    messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
    messegeBox->Show(3);
}

void MainView::readerConfig_Ok(quint8 id){
    messegeBox->setText(QString("تنظیمات کارتخوان شماره %0 انجام شد").arg(id));
    messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
    messegeBox->Show(3);
}

void MainView::newCardSent(quint8 count){
    messegeBox->setText(QString("تعداد %0 عدد تراکنش کارت \nبه مرکز ارسال شد").arg(count));
    messegeBox->setIcon(QPixmap(":/Graphics/ok.png"));
    messegeBox->Show(3);
}

bool MainView::Report(ReportType reportType){

    switch(reportType){
    case PrevoiusDay:
        if(DayCount > 30) return false;
        DayCount++;
        break;
    case NextDay:
        if(DayCount <= 0){
            DayCount=0;
            return false;
        }
        DayCount--;
        break;
    case Today:
        DayCount = 0;
        break;
    case ThisDriver:
        DayCount = 0;
        break;
    }

    quint32 sumPrice1=0;
    quint32 sumPrice2=0;
    quint16 Count1=0;
    quint16 Count2=0;
    QDate date = QDate::currentDate();

    QString cmd;
    if(reportType == ThisDriver){
        cmd = QString("SELECT * FROM("
                      "(SELECT  COUNT(*) AS 'Count1', sum(price) AS 'SumPrice1' FROM (SELECT * FROM cardinfo where readerid=1 AND cardtype!=9 AND driverserial=%0 GROUP BY datetime, rfid, remainprice)),"
                      "(SELECT  COUNT(*) AS 'Count2', sum(price) AS 'SumPrice2' FROM (SELECT * FROM cardinfo where readerid!=1 AND cardtype!=9 AND driverserial=%0 GROUP BY datetime, rfid, remainprice))"
                      ");"
                      )
                .arg(Settings::driverId());
    }else{
        cmd = QString("SELECT * FROM("
                      "(SELECT  COUNT(*) AS 'Count1', sum(price) AS 'SumPrice1' FROM (SELECT * FROM cardinfo where readerid=1 AND cardtype!=9 AND datetime >= %0 AND datetime <=%1 AND driverserial=%2 GROUP BY datetime, rfid, remainprice)),"
                      "(SELECT  COUNT(*) AS 'Count2', sum(price) AS 'SumPrice2' FROM (SELECT * FROM cardinfo where readerid!=1 AND cardtype!=9 AND datetime >= %0 AND datetime <=%1 AND driverserial=%2 GROUP BY datetime, rfid, remainprice))"
                      ")"
                      )
                //.arg(date.toTime_t()-(date.time().hour()*3600 + date.time().minute()*60 + date.time().second())-(86400*DayCount))
                //.arg(date.toTime_t()-(date.time().hour()*3600 + date.time().minute()*60 + date.time().second())-(86400*(DayCount-1)))
                .arg(MyDateTime::createDateTime(QDateTime(date.addDays(-DayCount), QTime(0,0,0))))
                .arg(MyDateTime::createDateTime(QDateTime(date.addDays(-(DayCount-1)), QTime(0,0,0))))
                .arg(Settings::driverId());
    }

    QSqlQuery query(QSqlDatabase::database("Local_Eticket_Sqlite"));
    query.exec(cmd);
    if(query.lastError().isValid()) {
        qLog << query.lastError();
        return false;
    }
    if (query.first()){
        Count1=query.record().value("Count1").toUInt();
        Count2=query.record().value("Count2").toUInt();
        sumPrice1=query.record().value("SumPrice1").toUInt();
        sumPrice2=query.record().value("SumPrice2").toUInt();

        cardCount1_lable->setText(QString("%0").arg(Count1));
        cardCount2_lable->setText(QString("%0").arg(Count2));
        cardCount_lable->setText(QString("%0").arg(Count1+Count2));
        PriceCount_lable->setText(QString("%0").arg(sumPrice1+sumPrice2));
    }
    if(DayCount == 0){
        clock->start();
    }else{
        clock->setDate(QJalaliDate::FromGregorian(QDate::currentDate().addDays(-DayCount)));
    }
    return true;
}

void MainView::createHome(){
    homeWidgets = new QWidget(this);
    homeWidgets->setGeometry(BackLable->rect());

    clock = new Clock(homeWidgets);
    clock->resize(300, 100);
    clock->move(width()/2-clock->width()/2, 50);

    QLabel *tabrizBusLogo = new QLabel(homeWidgets);
    tabrizBusLogo->resize(230, 120);
    tabrizBusLogo->move(BackLable->width()-tabrizBusLogo->width()-10,60);
    tabrizBusLogo->setPixmap(QPixmap(":/Graphics/tabrizBusLogo.png"));
    tabrizBusLogo->setScaledContents(true);

    picture = new DriverPicture(homeWidgets);
    picture->setPixmap(QPixmap(QString("pics/%0").arg(Settings::driverId())));
    picture->resize(160, 180);
    picture->move(10, 45);

    mainButton_1 = new QPushButton("  گزارش کارکرد", homeWidgets);
    mainButton_1->resize(180, 70);
    mainButton_1->move(BackLable->width()-mainButton_1->width(), 230);
    mainButton_1->setIcon(QIcon(":/Graphics/money.png"));
    mainButton_1->setIconSize(QSize(55,55));
    mainButton_1->setLayoutDirection(Qt::RightToLeft);
    connect(mainButton_1, SIGNAL(clicked()), SLOT(On_MainButtons_Clicked()));
    mainButton_1->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 rgb(200, 200, 255), stop: 1 rgb(0, 0, 20, 80%));"
                                "border: 2px solid blue;"
                                "color: white;"
                                "font: 25px;"
                                "text-align:right;"
                                "border-top-left-radius: 50px;"
                                "border-bottom-left-radius: 5px;");

    mainButton_2 = new QPushButton("  کارکرد امروز", homeWidgets);
    mainButton_2->resize(180, 70);
    mainButton_2->move(mainButton_1->x(), mainButton_1->y()+mainButton_2->height()+10);
    mainButton_2->setIcon(QIcon(":/Graphics/money.png"));
    mainButton_2->setIconSize(QSize(55,55));
    mainButton_2->setLayoutDirection(Qt::RightToLeft);
    connect(mainButton_2, SIGNAL(clicked()), SLOT(On_MainButtons_Clicked()));
    mainButton_2->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 rgb(200, 200, 255), stop: 1 rgb(0, 0, 20, 80%));"
                                "border: 2px solid blue;"
                                "color: white;"
                                "font: 25px;"
                                "border-top-left-radius: 50px;"
                                "border-bottom-left-radius: 5px;");

    mainButton_3 = new QPushButton("    روز قبل", homeWidgets);
    mainButton_3->resize(180, 70);
    mainButton_3->move(mainButton_1->x(), mainButton_2->y()+mainButton_3->height()+10);
    mainButton_3->setIcon(QIcon(":/Graphics/go-next.png"));
    mainButton_3->setIconSize(QSize(55,55));
    mainButton_3->setLayoutDirection(Qt::RightToLeft);
    connect(mainButton_3, SIGNAL(clicked()), SLOT(On_MainButtons_Clicked()));
    mainButton_3->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 rgb(200, 200, 255), stop: 1 rgb(0, 0, 20, 80%));"
                                "border: 2px solid blue;"
                                "color: white;"
                                "font: 25px;"
                                "border-top-left-radius: 50px;"
                                "border-bottom-left-radius: 5px;");

    mainButton_4 = new QPushButton("تنظیمات", homeWidgets);
    mainButton_4->resize(180, 70);
    mainButton_4->move(0, mainButton_1->y());
    mainButton_4->setIcon(QIcon(":/Graphics/settings.png"));
    mainButton_4->setIconSize(QSize(55,55));
    mainButton_4->setLayoutDirection(Qt::LeftToRight);
    connect(mainButton_4, SIGNAL(clicked()), SLOT(On_MainButtons_Clicked()));
    mainButton_4->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 rgb(200, 200, 255), stop: 1 rgb(0, 0, 20, 80%));"
                                "border: 2px solid blue;"
                                "color: white;"
                                "font: 25px;"
                                "border-top-right-radius: 50px;"
                                "border-bottom-right-radius: 5px;");

    mainButton_5 = new QPushButton("پاک کردن", homeWidgets);
    mainButton_5->resize(180, 70);
    mainButton_5->move(0, mainButton_4->y()+mainButton_5->height()+10);
    mainButton_5->setIcon(QIcon(":/Graphics/trash-full.png"));
    mainButton_5->setIconSize(QSize(55,55));
    mainButton_5->setLayoutDirection(Qt::LeftToRight);
    connect(mainButton_5, SIGNAL(clicked()), SLOT(On_MainButtons_Clicked()));
    mainButton_5->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 rgb(200, 200, 255), stop: 1 rgb(0, 0, 20, 80%));"
                                "border: 2px solid blue;"
                                "color: white;"
                                "font: 25px;"
                                "border-top-right-radius: 50px;"
                                "border-bottom-right-radius: 5px;");

    mainButton_6 = new QPushButton("روز بعد", homeWidgets);
    mainButton_6->resize(180, 70);
    mainButton_6->move(0, mainButton_5->y()+mainButton_6->height()+10);
    mainButton_6->setIcon(QIcon(":/Graphics/go-previous.png"));
    mainButton_6->setIconSize(QSize(55,55));
    mainButton_6->setLayoutDirection(Qt::LeftToRight);
    connect(mainButton_6, SIGNAL(clicked()), SLOT(On_MainButtons_Clicked()));
    mainButton_6->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 rgb(200, 200, 255), stop: 1 rgb(0, 0, 20, 80%));"
                                "border: 2px solid blue;"
                                "color: white;"
                                "font: 25px;"
                                "border-top-right-radius: 50px;"
                                "border-bottom-right-radius: 5px;");

    cardCount1_lable = new NumbericLable(homeWidgets);
    cardCount1_lable->resize(150, 80);
    cardCount1_lable->move(BackLable->width()/2+2, 180);
    cardCount1_lable->setText("-");
    cardCount1_lable->setLable("درب جلو");
    connect(readers, SIGNAL(startDownloading1()), cardCount1_lable, SLOT(setInUse()));
    connect(readers, SIGNAL(endDownloading()), cardCount1_lable, SLOT(setDefault()));
    //    connect(Reader1, SIGNAL(errorRecived()), cardCount1_lable, SLOT(setDefault()));

    cardCount2_lable = new NumbericLable(homeWidgets);
    cardCount2_lable->resize(150, 80);
    cardCount2_lable->move(BackLable->width()/2-cardCount2_lable->width()-2, 180);
    cardCount2_lable->setText("-");
    cardCount2_lable->setLable("درب عقب");
    connect(readers, SIGNAL(startDownloading2()), cardCount2_lable, SLOT(setInUse()));
    connect(readers, SIGNAL(endDownloading()), cardCount2_lable, SLOT(setDefault()));
    //    connect(Reader2, SIGNAL(errorRecived()), cardCount2_lable, SLOT(setDefault()));

    cardCount_lable = new NumbericLable(homeWidgets);
    cardCount_lable->resize(300, 80);
    cardCount_lable->move((BackLable->width()-cardCount_lable->width())/2, 280);
    cardCount_lable->setText("-");
    cardCount_lable->setLable("مجموع دو درب");

    PriceCount_lable = new NumbericLable(homeWidgets);
    PriceCount_lable->resize(300, 80);
    PriceCount_lable->move((BackLable->width()-PriceCount_lable->width())/2, 380);
    PriceCount_lable->setText("-");
    PriceCount_lable->setLable("کارکرد به تومان");

    QLabel *Statusbar = new QLabel(homeWidgets);
    Statusbar->resize(width(), 42);
    //Statusbar->setStyleSheet("background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(200, 200, 255, 30%));");
    Statusbar->setPixmap(QPixmap(":/Graphics/StatusbarBack.png"));
    Statusbar->setScaledContents(true);

    LED *GpsLed = new LED("ارسال", Statusbar);
    GpsLed->move(5, 0);
    GpsLed->setColor(Qt::green);
    GpsLed->setOff();
    //connect(GsmGps, SIGNAL(SendState_Changed(bool)), GpsLed, SLOT(setOnOff(bool)));

    AntenaWidget *GpsAntena = new AntenaWidget("GPS", Statusbar);
    GpsAntena->move(width()-GpsAntena->width()-5, 0);
    GpsAntena->SetValue(0);
    GpsAntena->setFixShow(false);
    connect(_gps, SIGNAL(GpsAntena_Changed(quint8)), GpsAntena, SLOT(SetValue(quint8)));
    connect(_gps, SIGNAL(GpsState_Changed(bool)), GpsAntena, SLOT(setFixShow(bool)));

    AntenaWidget *GsmAntena = new AntenaWidget("GSM", Statusbar);
    GsmAntena->move(GpsAntena->x()-GsmAntena->width(), 0);
    GsmAntena->SetValue(0);
    GsmAntena->setFixShow(false);
    connect(_gsm, SIGNAL(GsmAntena_Changed(quint8)), GsmAntena, SLOT(SetValue(quint8)));
    connect(_gsm, SIGNAL(GsmState_Changed(bool)), GsmAntena, SLOT(setFixShow(bool)));

    driverNameLablel=new QLabel(Statusbar);
    driverNameLablel->resize(250, Statusbar->height());
    driverNameLablel->move(GsmAntena->x()-driverNameLablel->width()-5, 0);
    driverNameLablel->setAlignment(Qt::AlignRight | Qt::AlignCenter);
    driverNameLablel->setStyleSheet("color:rgb(255,0,0); font: 25px");
    if(Settings::driverId() == UnknownDriver) driverNameLablel->setText("راننده گرامی لطفا وارد شوید");
    else driverNameLablel->setText(QString("راننده:%0").arg(Settings::driverName(Settings::driverId())));

    SimChargeValueLablel=new QLabel(Statusbar);
    SimChargeValueLablel->resize(180, Statusbar->height()/2);
    SimChargeValueLablel->move(driverNameLablel->x()-SimChargeValueLablel->width(), Statusbar->y());
    SimChargeValueLablel->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    SimChargeValueLablel->setStyleSheet("color:rgb(0,255,0); font: 18px");
    SimChargeValueLablel->setText(QString("-:شارژ سیمکارت"));
    connect(_gsm, SIGNAL(SimCharge_Changed()), this, SLOT(updateValues()));

    IMEI_Lablel=new QLabel(Statusbar);
    IMEI_Lablel->resize(180, Statusbar->height()/2);
    IMEI_Lablel->move(SimChargeValueLablel->x()-SimChargeValueLablel->width(), Statusbar->y());
    IMEI_Lablel->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    IMEI_Lablel->setStyleSheet("color:rgb(0,255,0); font: 18px");
    IMEI_Lablel->setText(QString("IMEI:%0").arg(Settings::IMEI()));
    //connect(_gsm, SIGNAL(SimCharge_Changed(quint32)), this, SLOT(updateValues(quint32)));

    BusSerial_Lablel=new QLabel(Statusbar);
    BusSerial_Lablel->resize(180, Statusbar->height()/2);
    BusSerial_Lablel->move(SimChargeValueLablel->x(), SimChargeValueLablel->y()+BusSerial_Lablel->height());
    BusSerial_Lablel->setAlignment(Qt::AlignLeft | Qt::AlignCenter);
    BusSerial_Lablel->setStyleSheet("color:rgb(0,255,0); font: 18px");
    BusSerial_Lablel->setText(QString("خط/اتوبوس:%0/%1").arg(Settings::BusSerial()).arg(Settings::BusLine()));

    ProgressBar = new QProgressBar(this);
    ProgressBar->resize(width()-100, 40);
    ProgressBar->move(50, height()-ProgressBar->height());
    ProgressBar->hide();

    //    testTextEdit = new QTextEdit(this);//@temp
    //    testTextEdit->resize(400, 200);
    //    testTextEdit->move(10, 10);
    //    testTextEdit->setFont(QFont("B Homa", 40));
    //    testTextEdit->hide();

    //        QTimer *TestTimer = new QTimer(this);
    //        TestTimer->setInterval(1000);
    //        connect(TestTimer, SIGNAL(timeout()), SLOT(test()));
    //        TestTimer->start();
}

void MainView::updateValues(){
    if(sender() == settingsDialog){
        BusSerial_Lablel->setText(QString("خط/اتوبوس:%0/%1").arg(Settings::BusSerial()).arg(Settings::BusLine()));
    }else if(sender() == _gsm){
        SimChargeValueLablel->setText(QString("شارژسیمکارت:%0 ریال").arg(Settings::SimCharge()));
    }
}

void MainView::clearCounters(){
    cardCount1=0;
    cardCount2=0;
    PriceCount=0;
    cardCount1_lable->setText("-");
    cardCount2_lable->setText("-");
    cardCount_lable->setText("-");
    PriceCount_lable->setText("-");
    clock->start();
    DayCount=0;
}

void MainView::incomingCall(){
    ringSound->play();
    msgBox->setText("تماس ورودی");
    msgBox->setInformativeText("آیا جواب میدهید؟");
    msgBox->setIconPixmap(QPixmap(":/Graphics/warning.png"));
    if(msgBox->isHidden()) msgBox->show();
    if(msgBox->clickedButton() == yesButton){
        _gsm->answerIncomingCall();
    }
}

void MainView::test(QString str){
    //testTextEdit->append(str);
    //qLog << str;
    qLog << _gps->parameters().Longitude << _gps->parameters().Latitude << _gps->parameters().Altitude << _gps->parameters().Course << _gps->parameters().Date<< _gps->parameters().Speed<< _gps->parameters().Time<< _gps->parameters().NumberOfSatellite;
}
