#ifndef LED_H
#define LED_H

#include <QWidget>
#include <QLabel>

class LED : public QWidget
{
    Q_OBJECT
public:
    explicit LED(QString text=QString(), QWidget *parent = 0);

private:
    QLabel *pix;
    Qt::GlobalColor Color;
signals:
    
public slots:
    void setColor(Qt::GlobalColor color);
    Qt::GlobalColor color();
    void setOn();
    void setOff();
    void setOnOff(bool cmd);
    
};

#endif // LED_H
