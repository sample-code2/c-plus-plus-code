#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <QMessageBox>
#include <QLabel>
#include <QResizeEvent>
#include <QPropertyAnimation>

#define DEFAULT_BACKGROUND	"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #8C8F8C, stop: 1 rgb(0, 0, 150, 80%));"
#define DEFAULT_STYLE       "border: 5px solid #000000;border-radius:20px;"

class MessageBox : public QMessageBox
{
    Q_OBJECT

public:
    MessageBox(QWidget *parent=0);

private:
    QLabel *Back;
    QPropertyAnimation *FadeAnimation;

protected:
    virtual void resizeEvent(QResizeEvent *event);
};

#endif // MESSAGEBOX_H
