#ifndef NUMBERICLABLE_H
#define NUMBERICLABLE_H

#include <QWidget>
#include <QLabel>
#include <QResizeEvent>

class NumbericLable : public QWidget
{
    Q_OBJECT
public:
    explicit NumbericLable(QWidget *parent = 0);
    void setText(QString num);
    void setLable(QString text);
    
private:
    QLabel *Back;
    QLabel *Text;

protected:
    virtual void resizeEvent(QResizeEvent *event);


signals:
    
public slots:
    void setInUse();
    void setDefault();
};

#endif // NUMBERICLABLE_H
