#include "logindialog.h"
#include "mainview.h"
#include "Defines.h"

LoginDialog::LoginDialog(QWidget *parent) :QDialog(parent){
    setWindowFlags(Qt::FramelessWindowHint);
    resize(400, 200);

    keyboard = MainView::instance()->_keyboard;
    keyboard->raise();
    userType = NONE_USER;
    //userType = ROOT_USER;//@temp

    QSettings settings("tcco", "console");
    settings.setValue("users/rootPassword", DefaultRootPaaword);
    settings.setValue("users/admin1Password", DefaultAdmin1Paaword);
    settings.setValue("users/admin2Password", DefaultAdmin2Paaword);
    settings.setValue("users/admin3Password", DefaultAdmin3Paaword);

    /***********for back and status report*****************/
    back = new QLabel(this);
    back->setStyleSheet("font-family:'B Homa';font: 18px;color:rgb(0,255,0);border: 3px solid #000000;border-radius:15px;background-color:rgb(0,0,0,200); border-color:rgb(0,0,255);");
    back->setAlignment(Qt::AlignBottom | Qt::AlignCenter);

    Close_Button = new QPushButton(QIcon(":/Graphics/close.png"), "", this);
    Close_Button->resize(42, 42);
    Close_Button->setIconSize(Close_Button->size());
    Close_Button->move(10,10);
    Close_Button->setFlat(true);
    connect(Close_Button, SIGNAL(clicked()), SLOT(close()));

    userName_lineEdit = new LineEdit(this);
    userName_lineEdit->resize(160, 60);
    userName_lineEdit->move(5, 60);
    userName_lineEdit->setAlignment(Qt::AlignCenter);
    userName_lineEdit->setFont(QFont("B homa"));
    userName_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    userName_lineEdit->setPlaceholderText("نام کاربری");
    connect(userName_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    password_lineEdit = new LineEdit(this);
    password_lineEdit->resize(userName_lineEdit->size());
    password_lineEdit->move(userName_lineEdit->x()+userName_lineEdit->width()+5, userName_lineEdit->y());
    password_lineEdit->setAlignment(Qt::AlignCenter);
    password_lineEdit->setFont(QFont("B homa"));
    password_lineEdit->setStyleSheet("color: darkblue; font: 18px;");
    password_lineEdit->setPlaceholderText("رمز عبور");
    password_lineEdit->setEchoMode(QLineEdit::Password);
    connect(password_lineEdit, SIGNAL(focused()), SLOT(setKeyboard()));

    Login_Button = new QPushButton(QIcon(":/Graphics/go-next.png"), "", this);
    Login_Button->resize(userName_lineEdit->height(), userName_lineEdit->height());
    Login_Button->setIconSize(Login_Button->size());
    Login_Button->move(password_lineEdit->x()+password_lineEdit->width()+5, userName_lineEdit->y());
    Login_Button->setShortcutEnabled(Qt::Key_Enter);
    Login_Button->setFlat(true);
    connect(Login_Button, SIGNAL(clicked()), SLOT(On_MainButtons_Clicked()));

    back->setText(".لطفا نام کاربری و رمز عبور رو وارد کنید");
}

void LoginDialog::resizeEvent(QResizeEvent *event){
    back->resize(this->size());
    event->accept();
}

void LoginDialog::setKeyboard(){
    if(sender() == userName_lineEdit){
        keyboard->setTarget(userName_lineEdit);
        keyboard->show();
    }else if (sender() == password_lineEdit) {
        keyboard->setTarget(password_lineEdit);
        keyboard->show();

    }
}

LoginDialog::USER_TYPE LoginDialog::user(){
    return userType;
}

void LoginDialog::On_MainButtons_Clicked(){
    if(sender() == Login_Button){
        QSettings settings("tcco", "console");
        if(userName_lineEdit->text() == "ROOT" && password_lineEdit->text() == settings.value("users/rootPassword").toString()){
            userType = ROOT_USER;
        }else if(userName_lineEdit->text() == "ADMIN1" && password_lineEdit->text() == settings.value("users/admin1Password").toString()){
            userType = ADMIN_1_USER;
        }else if(userName_lineEdit->text() == "ADMIN2" && password_lineEdit->text() == settings.value("users/admin2Password").toString()){
            userType = ADMIN_2_USER;
        }else if(userName_lineEdit->text() == "ADMIN3" && password_lineEdit->text() == settings.value("users/admin3Password").toString()){
            userType = ADMIN_3_USER;
        }else{
            userType = NONE_USER;
        }

        if(user() == ADMIN_1_USER || user() == ADMIN_2_USER || user() == ADMIN_3_USER || user() == ROOT_USER){
            back->setText(".ورود با موفقیت انجام شد");
            QTimer::singleShot(600000, this, SLOT(logOut()));//10 min time for logOut
            password_lineEdit->clear();
            QTimer::singleShot(2000, this, SLOT(hide()));
            emit logedIn(user());
            close();
        }else{
            back->setText(".نام کاربری یا رمز عبور اشتباه است");
        }
    }
}

void LoginDialog::logOut(){
    userType = NONE_USER;
    back->setText(".لطفا نام کاربری و رمز عبور رو وارد کنید");
}
