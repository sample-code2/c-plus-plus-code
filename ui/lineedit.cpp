#include "lineedit.h"

LineEdit::LineEdit(QWidget *parent) :QLineEdit(parent){
}

void LineEdit::focusInEvent(QFocusEvent *e){
    emit focused();
    e->accept();
}

void LineEdit::resizeEvent(QResizeEvent *event){
    event->accept();
}
