#ifndef REPORTDIALOG_H
#define REPORTDIALOG_H

#include <QWidget>
#include <QPushButton>
#include <QSqlDatabase>
#include <QTimeEdit>
#include <QTableWidget>
#include "jalalidateedit.h"
#include "numbericlable.h"
#include "messegebox.h"
#include "mylibs/printer.h"


class ReportDialog : public QWidget
{
    Q_OBJECT
public:
    typedef struct{
        quint16 price;
        quint32 numberOfCards;
        quint32 sumPrice;
    }REPORT;

    typedef struct{
        QString driverName;
        quint32 numberOfCards;
        quint32 sumPrice;
    }DRIVER_REPORT;

    typedef enum{
        NONE_REPORT_TYPE,
        ALL_DRIVERS_REPORT_TYPE,
        THIS_DRIVER_REPORT_TYPE
    }REPORT_TYPE;

    explicit ReportDialog(QWidget *parent = 0);

private:
    QString driverName(quint32 serial);

    REPORT_TYPE reportType;
    Printer *printer;
    QPushButton *GetReportButton;
    QPushButton *PrintButton;
    QPushButton *GetReportAllDriverButton;
    QPushButton *Shutdown_Button;
    QTimeEdit *startTime;
    QTimeEdit *endTime;
    jalaliDateEdit *Startdate;
    jalaliDateEdit *Enddate;
    MessegeBox *messegbox;
    QTableWidget *reportTable;
    QTableWidget *reportTable_detail;

signals:
private slots:
    void On_Buttons_Clicked();
public slots:
    void clearTable();

};

#endif // REPORTDIALOG_H
