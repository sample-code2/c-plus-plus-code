#ifndef STARTLOADINGDIALOG_H
#define STARTLOADINGDIALOG_H

#include <QWidget>
#include <QLabel>

class StartLoadingDialog : public QWidget
{
    Q_OBJECT
public:
    explicit StartLoadingDialog(QWidget *parent = 0);
    
private:
    QLabel *messageLable;
signals:
    
public slots:
    void showMessage(QString str);
    
};

#endif // STARTLOADINGDIALOG_H
