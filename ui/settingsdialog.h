#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QWidget>
#include <QTabWidget>
#include <QComboBox>
#include <QPushButton>
#include <QDateTimeEdit>
#include <QLineEdit>
#include <QtSql>
#include "vkeyboard/vkeyboard.h"
#include "jalalidateedit.h"
#include "lineedit.h"
#include "Defines.h"


#define MIN_PRICE 1
#define MAX_PRICE 65000

class SettingsDialog : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsDialog(QWidget *parent = 0);

private:
    void Create_SystemPage();
    void Create_ReaderPage();
    void Create_ServerPage();
    void Create_DatabasePage();

    QTabWidget *Tabs;
    VKeyboard *keyboard;
    QWidget *SystemSettings;
    QWidget *ServerSettings;
    QWidget *ReaderSettings;
    QWidget *DatabaseSettings;
    LineEdit *busSerial_lineEdit;
    LineEdit *busLine_lineEdit;
    LineEdit *price0_lineEdit;
    LineEdit *price1_lineEdit;
    LineEdit *price2_lineEdit;
    LineEdit *price3_lineEdit;
    LineEdit *price4_lineEdit;
    LineEdit *price5_lineEdit;
    LineEdit *price6_lineEdit;
    LineEdit *price7_lineEdit;
    LineEdit *price8_lineEdit;
    LineEdit *price9_lineEdit;
    QComboBox *setReaderCountComboBox;
    QPushButton *setReaderIDButton1;
#ifdef TINY210
    QPushButton *setReaderIDButton2;
    QPushButton *setReaderIDButton3;
#endif
    QPushButton * clearMemoryButton1;
    QPushButton * clearMemoryButton2;
    QPushButton * clearMemoryButton3;
    QPushButton *Apply_Button;
    QPushButton *Reboot_Button;
    QPushButton *Shutdown_Button;
    //QPushButton * CheckUpdate_Button;
    QPushButton *Close_Button;
    QPushButton *ServerApply_Button;
    QPushButton *ServerReadValues_Button;
    QPushButton *ServerClear_Button;
    jalaliDateEdit *dateEdit;
    QTimeEdit *timeEdit;
    QPushButton *applyDateTimeButton_Custom;
    QPushButton *applyDateTimeButton_Auto;
    QPushButton *ReadValues_Button;
    QPushButton *clear_Button;
    QPushButton *VACUUM_Button;
    QPushButton *TruncetCardTable;
    QPushButton *TruncetAvlTable;
    LineEdit *ServerAddresLineedit;
    LineEdit *ServerPortLineedit;
    LineEdit *ServerUrlLineedit;
    LineEdit *CardSendRate_Lineedit;
    LineEdit *AvlSendRate_Lineedit;

private slots:
    void On_Buttons_Clicked();
    void setKeyboard();
    void ReaderCountChanged(int count);
protected:

signals:
    void newReaderSettingAvailable();
#ifdef MY210
    void readerIdAvailable();
#elif defined TINY210
    void readerIdAvailable(quint8 id);
#endif
    void ReaderFormatAvailable(quint8 id);
    void newReaderCountAvailable(quint8 count);


public slots:
    static void setDateTime(QDateTime dateTime);
    void setDateTimeFromGMT(QDateTime dateTime);

};

#endif // SETTINGSDIALOG_H
