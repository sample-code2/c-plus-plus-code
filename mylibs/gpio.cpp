#include "gpio.h"
#include <QFile>
#include <QDebug>

GPIO::GPIO(int pin, QObject *parent):QObject(parent){
    _pinNumber = pin;
    _pinNumber_str = QString("%1").arg(pin);
    _valueString = QString("%0/gpio%1%2").arg(GPIO_DEVICE_PATH).arg(pin).arg(GPIO_VALUE_DIR);
    _directionString = QString("%0/gpio%1%2").arg(GPIO_DEVICE_PATH).arg(pin).arg(GPIO_DIRECTION_DIR);
    _direction = In;
}

GPIO::~GPIO(){
    QFile file(GPIO_DEVICE_PATH + QString(GPIO_UNEXPORT_DIR));
    if (!file.open(QIODevice::WriteOnly)) return;
    file.seek(0);//Set pointer to begining of the file
    file.write(QByteArray(_pinNumber_str.toLatin1()));
    file.close();
}

bool GPIO::openPin(){
    QFile file(GPIO_DEVICE_PATH + QString(GPIO_EXPORT_DIR));
    if (!file.open(QIODevice::WriteOnly)) return false;
    file.seek(0);//Set pointer to begining of the file
    file.write(QByteArray(_pinNumber_str.toLatin1()));
    file.close();
    return true;
}

bool GPIO::closePin(){
    QFile file(GPIO_DEVICE_PATH + QString(GPIO_UNEXPORT_DIR));
    if (!file.open(QIODevice::WriteOnly)) return false;
    file.seek(0);//Set pointer to begining of the file
    file.write(QByteArray(_pinNumber_str.toLatin1()));
    file.close();
    return true;
}

bool GPIO::setDirection(Direction direction){
    QFile file(_directionString.toLatin1());
    if (!file.open(QIODevice::WriteOnly)) return false;
    file.seek(0);//Set pointer to begining of the file
    if(direction == In)  file.write("in", 2);
    else if(direction == Out) file.write("out", 3);
    file.close();
    _direction = direction;
    return true;
}

GPIO::Direction GPIO::getDirection(){
    return _direction;
}

bool GPIO::setValue(bool state){
    if(_direction == In)
        qWarning("can not write value on input pin");

    QFile file(_valueString.toLatin1());
    if (!file.open(QIODevice::WriteOnly)) return false;
    file.seek(0);//Set pointer to begining of the file
    if(state)  file.write("1", 1);
    else file.write("0", 1);
    file.close();
    return true;
}

bool GPIO::setValue(bool state, quint16 pinNumber){
    QFile file(QString("%0/gpio%1%2").arg(GPIO_DEVICE_PATH).arg(pinNumber).arg(GPIO_VALUE_DIR).toLatin1());
    if (!file.open(QIODevice::WriteOnly)) return false;
    file.seek(0);//Set pointer to begining of the file
    if(state)  file.write("1", 1);
    else file.write("0", 1);
    file.close();
    return true;
}

bool GPIO::setHigh(){   
    return setValue(true);
}

bool GPIO::setLow(){
    return setValue(false);
}

bool GPIO::value(){
    if(_direction == Out)
        qWarning("can not read Value from output pin");
    QFile file(_valueString.toLatin1());
    if (!file.open(QIODevice::ReadOnly)) return false;
    file.seek(0);//Set pointer to begining of the file
    QByteArray value = file.read(1);
    if(value[0]=='1')  return true;
    else if(value[0]=='0') return false;
    file.close();
    return false;
}

bool GPIO::value(quint16 pinNumber){
    QFile file(QString("%0/gpio%1%2").arg(GPIO_DEVICE_PATH).arg(pinNumber).arg(GPIO_VALUE_DIR).toLatin1());
    if (!file.open(QIODevice::ReadOnly)) return false;
    file.seek(0);//Set pointer to begining of the file
    QByteArray value = file.read(1);
    if(value[0]=='1')  return true;
    else if(value[0]=='0') return false;
    file.close();
    return false;
}
