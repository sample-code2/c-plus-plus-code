#include "mydatetime.h"

MyDateTime::MyDateTime(QObject *parent) :QObject(parent){

}

quint32 MyDateTime::createDateTime(QDateTime dateTime){

    quint32 temp=0, outPut=0;
    temp = dateTime.date().year()-2000;
    outPut  |= (temp << 26);
    temp = dateTime.date().month();
    outPut  |= (temp << 22);
    temp = dateTime.date().day();
    outPut  |= (temp << 17);
    temp = dateTime.time().hour();
    outPut  |= (temp << 12);
    temp = dateTime.time().minute();
    outPut  |= (temp << 6);
    temp = dateTime.time().second();
    outPut  |= temp;

    return outPut;
}

QDateTime MyDateTime::extractDateTime(quint32 dateTime){
    quint32 temp=0;

    temp = (dateTime & 0b00000000000000000000000000111111);//second:6
    quint8 sec = temp;
    temp = (dateTime & 0b00000000000000000000111111000000);//minute:6
    quint8 minute = (temp >> 6);
    temp = (dateTime & 0b00000000000000011111000000000000);//hour:5
    quint8 hour = (temp >> 12);
    temp = (dateTime & 0b00000000001111100000000000000000);//day:5
    quint8 day = (temp >> 17);
    temp = (dateTime & 0b00000011110000000000000000000000);//month:4
    quint8 month = (temp >> 22);
    temp = (dateTime & 0b11111100000000000000000000000000);//year:6
    quint8 year = (temp >> 26);

    return QDateTime(QDate(year+2000, month, day), QTime(hour, minute, sec));
}

bool MyDateTime::isValid(quint32 dateTime){
    return extractDateTime(dateTime).isValid();
}
