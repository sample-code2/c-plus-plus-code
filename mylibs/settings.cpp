#include "settings.h"
#include <QSettings>
#include <QSqlQuery>
#include <QSqlRecord>
#include "Defines.h"
#include "readers.h"

Settings::Settings(QObject *parent) :QObject(parent){

}

void Settings::setIMEI(quint64 imei){
    QSettings settings("tcco", "console");
    settings.setValue("core/IMEI", imei);
}

void Settings::setServerIp(QString cmd){
    QSettings settings("tcco", "console");
    settings.setValue("server/IpAddress", cmd);
}

void Settings::setServerPort(QString cmd){
    QSettings settings("tcco", "console");
    settings.setValue("server/Port", cmd);
}

void Settings::setServerUrl(QString cmd){
    QSettings settings("tcco", "console");
    settings.setValue("server/ServerUrl", cmd);
}

void Settings::setpppIp(QString cmd){
    QSettings settings("tcco", "console");
    settings.setValue("server/pppIp", cmd);
}

void Settings::setCardSendRate(quint32 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("send/CardSendRate", cmd);
}

void Settings::setCardUnsentInterval(quint32 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("send/CardUnsentInterval", cmd);
}

void Settings::setAvlSendRate(quint32 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("send/AvlSendRate", cmd);
}

void Settings::setAvlUnsentInterval(quint32 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("send/AvlUnsentInterval", cmd);
}

void Settings::setAvlMaxNumberDataInPack(quint16 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("avl/AvlMaxNumberDataInPack", cmd);
}

void Settings::setFtpUserName(QString cmd){
    QSettings settings("tcco", "console");
    settings.setValue("server/FtpUserName", cmd);
}

void Settings::setFtpPassword(QString cmd){
    QSettings settings("tcco", "console");
    settings.setValue("server/FtpPassword", cmd);
}

void Settings::setFtpPort(quint16 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("server/FtpPort", cmd);
}

void Settings::setBusSerial(quint32 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("bus/BusSerial", cmd);
}

void Settings::setBusLine(quint32 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("bus/BusLine", cmd);
}

void Settings::setWorkingStartDateTime(QDateTime cmd){
    QSettings settings("tcco", "console");
    settings.setValue("core/WorkingStartDateTime", cmd);
}

void Settings::setWorkingEndDateTime(QDateTime cmd){
    QSettings settings("tcco", "console");
    settings.setValue("core/WorkingEndDateTime", cmd);
}

void Settings::setProgramPath(QString cmd){
    QSettings settings("tcco", "console");
    settings.setValue("core/ProgramPath", cmd);
}

void Settings::setReaderDriverIdAvalable(quint8 readerNumber, bool cmd){
    QSettings settings("tcco", "console");

    if(readerNumber == 1) settings.setValue("readers/Reader1DriverIdAvalable", cmd);
    else if(readerNumber == 2) settings.setValue("readers/Reader2DriverIdAvalable", cmd);
    else if(readerNumber == 3) settings.setValue("readers/Reader3DriverIdAvalable", cmd);
}

void Settings::setReaderConfigAvalable(quint8 readerNumber, bool cmd){
    QSettings settings("tcco", "console");

    if(readerNumber == 1) settings.setValue("readers/Reader1ConfigAvalable", cmd);
    else if(readerNumber == 2) settings.setValue("readers/Reader2ConfigAvalable", cmd);
    else if(readerNumber == 3) settings.setValue("readers/Reader3ConfigAvalable", cmd);
}

void Settings::setDriverId(quint64 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("driver/rfid", cmd);
}

void Settings::setSimCharge(quint32 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("core/simCharge", cmd);
}


QString Settings::driverName(quint64 serial){
    QSqlQuery query(QSqlDatabase::database("Local_Bus_Sqlite"));
    query.exec(QString("SELECT name FROM drivers WHERE RFID = %0 ;").arg(serial));
    query.first();
    if(query.record().value("name").isNull()){
        return QString("%0").arg(serial);
    }else{
        return query.record().value("name").toString();
    }
}

void Settings::setReaderCount(quint8 cmd){
    QSettings settings("tcco", "console");
    settings.setValue("readers/ReaderCount", cmd);
}

/**********************************************************************************/

quint64 Settings::IMEI(){
    QSettings settings("tcco", "console");
    return settings.value("core/IMEI", 0).toULongLong();
}

QByteArray Settings::Version(){
    char ver[3] = {VERSION};
    return  QByteArray(ver , 3);
}

QString Settings::ServerIp(){
    QSettings settings("tcco", "console");
    return  settings.value("server/IpAddress", DefaultServerIp).toString();
}

QString Settings::ServerPort(){
    QSettings settings("tcco", "console");
    return settings.value("server/Port", DefaultServerPort).toString();
}

QString Settings::ServerUrl(){
    QSettings settings("tcco", "console");
    return settings.value("server/ServerUrl", DefaultSendUrl).toString();
}

QString Settings::pppIp(){
    QSettings settings("tcco", "console");
    return settings.value("server/pppIp", "0.0.0.0").toString();
}

quint32 Settings::CardSendRate(){
    QSettings settings("tcco", "console");
    return settings.value("send/CardSendRate", DefaultCardSendRate).toUInt();
}

quint32 Settings::CardUnsentInterval(){
    QSettings settings("tcco", "console");
    return settings.value("send/CardUnsentInterval", DefaultCardUnsentInterval).toUInt();
}

quint32 Settings::AvlSendRate(){
    QSettings settings("tcco", "console");
    return settings.value("send/AvlSendRate", DefaultAvlSendRate).toUInt();
}

quint32 Settings::AvlUnsentInterval(){
    QSettings settings("tcco", "console");
    return settings.value("send/AvlUnsentInterval", DefaultAvlUnsentInterval).toUInt();
}

quint16 Settings::AvlMaxNumberDataInPack(){
    QSettings settings("tcco", "console");
    return settings.value("avl/AvlMaxNumberDataInPack", DefaultMaxNumAvlDataInPack).toUInt();
}

QString Settings::FtpUserName(){
    QSettings settings("tcco", "console");
    return settings.value("server/FtpUserName", DefaultFtpUserName).toString();
}

QString Settings::FtpPassword(){
    QSettings settings("tcco", "console");
    return settings.value("server/FtpPassword", DefaultFtpPassword).toString();
}

quint16 Settings::FtpPort(){
    QSettings settings("tcco", "console");
    return settings.value("send/FtpPort", DefaultFtpPort).toUInt();
}

quint32 Settings::BusSerial(){
    QSettings settings("tcco", "console");
    return settings.value("bus/BusSerial", 0).toUInt();
}

quint32 Settings::BusLine(){
    QSettings settings("tcco", "console");
    return settings.value("bus/BusLine", 0).toUInt();
}

QDateTime Settings::WorkingStartDateTime(){
    QSettings settings("tcco", "console");
    return settings.value("core/WorkingStartDateTime", QDateTime::fromString(DefaultStartDateTime, "yyyy/MM/dd HH:mm:ss")).toDateTime();
}

QDateTime Settings::WorkingEndDateTime(){
    QSettings settings("tcco", "console");
    return settings.value("core/WorkingEndDateTime", QDateTime::fromString(DefaultEndDateTime, "yyyy/MM/dd HH:mm:ss")).toDateTime();
}

QString Settings::ProgramPath(){
    QSettings settings("tcco", "console");
    return settings.value("core/ProgramPath", "/console/").toString();
}

bool Settings::ReaderDriverIdAvalable(quint8 readerNumber){
    QSettings settings("tcco", "console");

    if(readerNumber == 1)return settings.value("readers/Reader1DriverIdAvalable", false).toBool();
    if(readerNumber == 2)return settings.value("readers/Reader2DriverIdAvalable", false).toBool();
    if(readerNumber == 3)return settings.value("readers/Reader3DriverIdAvalable", false).toBool();

    return false;
}

bool Settings::ReaderConfigAvalable(quint8 readerNumber){
    QSettings settings("tcco", "console");

    if(readerNumber == 1)return settings.value("readers/Reader1ConfigAvalable", false).toBool();
    if(readerNumber == 2)return settings.value("readers/Reader2ConfigAvalable", false).toBool();
    if(readerNumber == 3)return settings.value("readers/Reader3ConfigAvalable", false).toBool();

    return false;
}

quint8 Settings::ReaderCount(){
    QSettings settings("tcco", "console");
    return settings.value("readers/ReaderCount", 2).toUInt();
}

quint64 Settings::driverId(){
    QSettings settings("tcco", "console");
    return settings.value("driver/rfid", UnknownDriver).toULongLong();
}

quint32 Settings::SimCharge(){
    QSettings settings("tcco", "console");
    return settings.value("core/simCharge", 0).toULongLong();
}

