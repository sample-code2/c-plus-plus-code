#include "gps.h"
#include <QDebug>
#include <QSqlQuery>
#include "settings.h"
#include "mydatetime.h"
#include "mainview.h"
#include "Defines.h"

GPS::GPS(QString portName, QObject *parent) : QObject(parent){

    GPRMC = "GPRMC";
    GPGGA = "GPGGA";
    GPGSV = "GPGSV";
    GPRMC_Pos = 0;
    GPGGA_Pos = 0;
    GPGSV_Pos = 0;
    CommaCount = 0;
    GpsInfoMode = NONEMode;
    GpsFailRecived_Counter = 0;
    Gps_Is_Valid = false;
    GpsReadStatusTrue_Counter = 0;
    Avl_Buffering_Counter=0;
    lastNumberOfSatellite=0;
    lastGpsState=false;
    MovingTimeCounter=0;

    PortSettings settings = {BAUD4800, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};
    _gpsPort = new QextSerialPort(portName, settings, QextSerialPort::EventDriven, this);
    if(OpenUart()) qLog << ("Opening GPS serial port "+portName+"\tOK.");
    else qLog << ("Opening GPS serial port "+portName+"\tFAIL.");
    connect(_gpsPort, SIGNAL(readyRead()), SLOT(ReadUart()));

    ActionTimer = new QTimer(this);
    ActionTimer->setInterval(1000);
    connect(ActionTimer, SIGNAL(timeout()), SLOT(ActionProcess()));
    ActionTimer->start();
}

void GPS::ActionProcess(){
    Avl_Buffering_Counter++;
    GpsFailRecived_Counter++;

    if(GpsFailRecived_Counter >= 120 && MainView::instance()->_gsm->isOn()){
        GpsFailRecived_Counter=0;
        qDebug("gps data not receving ...");
        emit dataNotRecived();
    }

    AVL_INFO AvlInfo = INFO;
    if( dataIsValid() &&
            QDateTime(AvlInfo.Date, AvlInfo.Time) != QDateTime(PreviousAvlInfo.Date, PreviousAvlInfo.Time) &&//جلوگیری از ‌‌‌‌‌ذخیره داده تکراری
            QDateTime(AvlInfo.Date, AvlInfo.Time) >= Settings::WorkingStartDateTime() &&
            QDateTime(AvlInfo.Date, AvlInfo.Time) <= Settings::WorkingEndDateTime()){
        if((qAbs(AvlInfo.Course - PreviousAvlInfo.Course) >= 25 && AvlInfo.Speed > 5) ||
                (Avl_Buffering_Counter >= Settings::AvlSendRate()/Settings::AvlMaxNumberDataInPack() && (AvlInfo.Speed > 5 || PreviousAvlInfo.Speed > 5))
                ){

            qLog<< "------------------------------avl data buffered-------------------------------";
            Avl_Buffering_Counter=0;
            QSqlQuery insertQuery(QSqlDatabase::database("Local_Avl_Sqlite"));
            insertQuery.prepare("INSERT INTO avlinfo "
                                "(busserial, datetime, longitude, latitude , altitude, dir, speed, course, gps_ant, gsm_ant, sim_charge_value, bat_charge_value, isSent) "
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0);");
            insertQuery.addBindValue(Settings::BusSerial());
            insertQuery.addBindValue(MyDateTime::createDateTime(QDateTime(AvlInfo.Date, AvlInfo.Time)));
            insertQuery.addBindValue(AvlInfo.Longitude);
            insertQuery.addBindValue(AvlInfo.Latitude);
            insertQuery.addBindValue(AvlInfo.Altitude);
            insertQuery.addBindValue(AvlInfo.Dir);
            insertQuery.addBindValue(AvlInfo.Speed);
            insertQuery.addBindValue(AvlInfo.Course);
            insertQuery.addBindValue(AvlInfo.NumberOfSatellite);
            insertQuery.addBindValue(MainView::instance()->_gsm->gsmInfo().Gsm_Ant);
            insertQuery.addBindValue(MainView::instance()->_gsm->gsmInfo().Sim_Charge_Value);
            insertQuery.addBindValue(MainView::instance()->_gsm->gsmInfo().Bat_Charge_Value);
            insertQuery.exec();
            PreviousAvlInfo = AvlInfo;
        }
    }

    /*emit gps antena signal*/
    quint8 gpsAntValue = parameters().NumberOfSatellite*6;
    if(lastNumberOfSatellite != gpsAntValue){
        if(gpsAntValue < 100) emit GpsAntena_Changed(gpsAntValue);
        else emit GpsAntena_Changed(100);
    }
    lastNumberOfSatellite = gpsAntValue;

    /*emit gps state signal*/
    bool gpsState = dataIsValid();
    if(gpsState != lastGpsState){
        if (dataIsValid()){
            emit GpsState_Changed(true);
            emit newDateTime(QDateTime(AvlInfo.Date, AvlInfo.Time));
        }else
            emit GpsState_Changed(false);
    }
    lastGpsState = gpsState;

    if(dataIsValid() && parameters().Speed>=5 && MovingTimeCounter<=3) MovingTimeCounter++;
    if(parameters().Speed<5) MovingTimeCounter=0;
    if(MovingTimeCounter == 3)emit BusMoved();
}

bool GPS::OpenUart(){
    return _gpsPort->open(QIODevice::ReadWrite);
}

QByteArray GPS::ReadUart(){
    QByteArray RecivedArray = _gpsPort->readAll();

    for(int i=0; i<RecivedArray.length(); i++){

        if (RecivedArray.at(i) == '$') {
            GpsInfoMode = NONEMode;
            CommaCount = 0;
        } else {
            //****SEARCH GPRMC *****************************************************
            if(Str_Interupt(GPRMC, &GPRMC_Pos, RecivedArray.at(i), true)){
                GpsInfoMode = GPRMCMode;
                GpsFailRecived_Counter = 0;
            }
            //****SEARCH GPGGA *****************************************************
            if(Str_Interupt(GPGGA, &GPGGA_Pos, RecivedArray.at(i), true)){
                GpsInfoMode = GPGGAMode;
            }
            //****SEARCH GPGSV *****************************************************
            if(Str_Interupt(GPGSV, &GPGSV_Pos, RecivedArray.at(i), true)){
                GpsInfoMode = GPGSVMode;
            }
            //**********************************************************************
            if (RecivedArray.at(i) == ',') {
                CommaCount++;
                if (GpsInfoMode == GPRMCMode && GPSTempBuffer.size()>=0) {
                    switch (CommaCount) {
                    case 2:
                        INFO.Time = QTime(GPSTempBuffer.mid(0,2).toUShort(),GPSTempBuffer.mid(2,2).toUShort(),GPSTempBuffer.mid(4,2).toUShort());
                        break; // rikhtane time dar gps_info
                    case 3:
                        if (GPSTempBuffer.at(0) != 'A') {
                            Gps_Is_Valid = false;
                            GpsReadStatusTrue_Counter = 0;
                        } else {
                            if (GpsReadStatusTrue_Counter == 5){
                                GpsReadStatusTrue_Counter++;
                                Gps_Is_Valid = true;
                            }else
                                GpsReadStatusTrue_Counter++;
                        }
                        break;
                    case 4:
                        INFO.Latitude = GPSTempBuffer.toFloat();
                        break;
                    case 5:
                        if (GPSTempBuffer.at(0) == 'N') _NS = 1;
                        else _NS = 0;
                        break;
                    case 6:
                        INFO.Longitude = GPSTempBuffer.toFloat();
                        break;
                    case 7:
                        if (GPSTempBuffer.at(0) == 'E'){
                            if (_NS == 1) INFO.Dir = NE;
                            else INFO.Dir = SE;
                        } else{
                            if (_NS == 1) INFO.Dir = NW;
                            else INFO.Dir = SW;
                        }
                        break;
                    case 8:
                        INFO.Speed = (int)GPSTempBuffer.toFloat();
                        break; // rikhtane speed dar gps_info
                    case 9:
                        INFO.Course = (int)GPSTempBuffer.toFloat();
                        break; // rikhtane course dar gps_info
                    case 10:
                        INFO.Date = QDate(2000+GPSTempBuffer.mid(4,2).toUShort(),GPSTempBuffer.mid(2,2).toUShort(),GPSTempBuffer.mid(0,2).toUShort());
                        break; // rikhtane date dar gps_info
                    }
                } else if (GpsInfoMode == GPGGAMode  && GPSTempBuffer.size()>=0 && CommaCount == 10){
                    INFO.Altitude = (int)GPSTempBuffer.toFloat();
                } else if (GpsInfoMode == GPGSVMode  && GPSTempBuffer.size()>=0 && CommaCount == 4) {
                    INFO.NumberOfSatellite = GPSTempBuffer.toInt();
                }
                GPSTempBuffer.clear();
            } else {
                GPSTempBuffer.append(RecivedArray.at(i));
            }
        }
    }
    return RecivedArray;
}

bool GPS::Str_Interupt(QString str, quint8 *pos, QChar input, bool CaseSensitive) {
    if (input == str[(*pos)] || (!CaseSensitive && input.toUpper() == str[(*pos)].toUpper())) (*pos)++;
    else if (input == str[0] || (!CaseSensitive && input.toUpper() == str[0].toUpper())) (*pos) = 1;
    else (*pos) = 0;

    if ((*pos) == str.length()) {
        (*pos) = 0;
        return true;
    }
    return false;
}

bool GPS::dataIsValid(){
    return Gps_Is_Valid;
}

GPS::AVL_INFO GPS::parameters(){
    return INFO;
}
