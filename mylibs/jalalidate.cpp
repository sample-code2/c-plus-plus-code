#include "jalalidate.h"

QJalaliDate::QJalaliDate(int  j_y, int  j_m, int  j_d){
    year=j_y;
    month=j_m;
    day=j_d;
}

QJalaliDate::QJalaliDate(QDate Gdate){
    year=FromGregorian(Gdate).Year();
    month=FromGregorian(Gdate).Month();
    day=FromGregorian(Gdate).Day();
}

QJalaliDate QJalaliDate::FromGregorian(QDate Gdate){
    int g_days_in_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int j_days_in_month[12] = {31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29};

    int gy, gm, gd;
    int jy, jm, jd;
    long g_day_no, j_day_no;
    int j_np;

    int i;

    gy = Gdate.year()-1600;
    gm = Gdate.month()-1;
    gd = Gdate.day()-1;

    g_day_no = 365*gy+(gy+3)/4-(gy+99)/100+(gy+399)/400;
    for (i=0;i<gm;++i)
        g_day_no += g_days_in_month[i];
    if (gm>1 && ((gy%4==0 && gy%100!=0) || (gy%400==0)))
        /* leap and after Feb */
        ++g_day_no;
    g_day_no += gd;

    j_day_no = g_day_no-79;

    j_np = j_day_no / 12053;
    j_day_no %= 12053;

    jy = 979+33*j_np+4*(j_day_no/1461);
    j_day_no %= 1461;

    if (j_day_no >= 366) {
        jy += (j_day_no-1)/365;
        j_day_no = (j_day_no-1)%365;
    }

    for (i = 0; (i < 11) && (j_day_no >= j_days_in_month[i]); ++i) {
        j_day_no -= j_days_in_month[i];
    }
    jm = i+1;
    jd = j_day_no+1;

    return QJalaliDate(jy, jm, jd);
}

QJalaliDate QJalaliDate::currentDate(){
    return FromGregorian(QDate::currentDate());
}

QDate QJalaliDate::ToGregorian(){
    int g_days_in_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int j_days_in_month[12] = {31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29};

    int gy, gm, gd;
    int jy, jm, jd;
    long g_day_no, j_day_no;
    int leap;

    int i;

    jy = year-979;
    jm = month-1;
    jd = day-1;

    j_day_no = 365*jy + (jy/33)*8 + (jy%33+3)/4;
    for (i=0; i < jm; ++i)
        j_day_no += j_days_in_month[i];

    j_day_no += jd;

    g_day_no = j_day_no+79;

    gy = 1600 + 400*(g_day_no/146097); /* 146097 = 365*400 + 400/4 - 400/100 + 400/400 */
    g_day_no = g_day_no % 146097;

    leap = 1;
    if (g_day_no >= 36525) /* 36525 = 365*100 + 100/4 */
    {
        g_day_no--;
        gy += 100*(g_day_no/36524); /* 36524 = 365*100 + 100/4 - 100/100 */
        g_day_no = g_day_no % 36524;

        if (g_day_no >= 365)
            g_day_no++;
        else
            leap = 0;
    }

    gy += 4*(g_day_no/1461); /* 1461 = 365*4 + 4/4 */
    g_day_no %= 1461;

    if (g_day_no >= 366) {
        leap = 0;

        g_day_no--;
        gy += g_day_no/365;
        g_day_no = g_day_no % 365;
    }

    for (i = 0; g_day_no >= g_days_in_month[i] + (i == 1 && leap); i++)
        g_day_no -= g_days_in_month[i] + (i == 1 && leap);
    gm = i+1;
    gd = g_day_no+1;

    return(QDate(gy,gm,gd));
}

void QJalaliDate::setDate(int  j_y, int  j_m, int  j_d){
    year=j_y;
    month=j_m;
    day=j_d;
}

void QJalaliDate::setDate(QJalaliDate Jdate){
    year=Jdate.Year();
    month=Jdate.Month();
    day=Jdate.Day();
}

QJalaliDate QJalaliDate::Date(){
    return *this;
}

QString QJalaliDate::toString(){
    return QString("%1/%2/%3").arg(year).arg(month).arg(day);
}

int QJalaliDate::Year(){
    return year;
}

int QJalaliDate::Month(){
    return month;
}

int QJalaliDate::Day(){
    return day;
}

int QJalaliDate::dayOfWeek(){
    QDate Date = ToGregorian();

    switch(Date.dayOfWeek()){
    case 1: //mon
        return 3;//دوشنبه
    case 2:
        return 4;
    case 3:
        return 5;
    case 4:
        return 6;
    case 5:
        return 7;
    case 6://sat
        return 1;//شنبه
    case 7:
        return 2;
    }
    return 0;
}

QString QJalaliDate::dayOfWeek_str(){
    switch(dayOfWeek()){
    case 1:
        return "شنبه";
    case 2:
        return "یکشنبه";
    case 3:
        return "دوشنبه";
    case 4:
        return "سه شنبه";
    case 5:
        return "چهارشنبه";
    case 6:
        return "پنجشنبه";
    case 7:
        return "جمعه";
    }
    return 0;
}
