#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QDateEdit>

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = 0);
    static void setIMEI(quint64 cmd);
    static void setVersion(QByteArray cmd);
    static void setServerIp(QString cmd);
    static void setServerPort(QString cmd);
    static void setServerUrl(QString cmd);
    static void setpppIp(QString cmd);
    static void setCardSendRate(quint32 cmd);
    static void setCardUnsentInterval(quint32 cmd);
    static void setAvlSendRate(quint32 cmd);
    static void setAvlUnsentInterval(quint32 cmd);
    static void setFtpUserName(QString cmd);
    static void setFtpPassword(QString cmd);
    static void setFtpPort(quint16 cmd);
    static void setBusSerial(quint32 cmd);
    static void setBusLine(quint32 cmd);
    static void setWorkingStartDateTime(QDateTime cmd);
    static void setWorkingEndDateTime(QDateTime cmd);
    static void setAvlMaxNumberDataInPack(quint16 cmd);
    static void setProgramPath(QString cmd);
    static void setReaderDriverIdAvalable(quint8 readerNumber, bool cmd);
    static void setReaderConfigAvalable(quint8 readerNumber, bool cmd);
    static void setDriverId(quint64 cmd);
    static void setSimCharge(quint32 cmd);
    static void setReaderCount(quint8 cmd);

    static quint64 IMEI();
    static QByteArray Version();
    static QString ServerIp();
    static QString ServerPort();
    static QString ServerUrl();
    static QString pppIp();
    static quint32 CardSendRate();
    static quint32 CardUnsentInterval();
    static quint32 AvlSendRate();
    static quint32 AvlUnsentInterval();
    static QString FtpUserName();
    static QString FtpPassword();
    static quint16 FtpPort();
    static quint32 BusSerial();
    static quint32 BusLine();
    static QDateTime WorkingStartDateTime();
    static QDateTime WorkingEndDateTime();
    static quint16 AvlMaxNumberDataInPack();
    static QString ProgramPath();
    static bool ReaderDriverIdAvalable(quint8 readerNumber);
    static bool ReaderConfigAvalable(quint8 readerNumber);
    static quint64 driverId();
    static quint32 SimCharge();
    static QString driverName(quint64 serial);
    static quint8 ReaderCount();
    
signals:
    
public slots:
    
};

#endif // SETTINGS_H
