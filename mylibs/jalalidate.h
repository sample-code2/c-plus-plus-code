#ifndef JALALIDATE_H
#define JALALIDATE_H

#include <QtCore/QObject>
#include <QtCore/QDate>

class QJalaliDate
{
public:
    explicit QJalaliDate(int  j_y, int  j_m, int  j_d);
    explicit QJalaliDate(QDate Gdate);
    static QJalaliDate FromGregorian(QDate Gdate);
    static QJalaliDate currentDate();
    QDate ToGregorian();
    void setDate(int  j_y, int  j_m, int  j_d);
    void setDate(QJalaliDate Jdate);
    QJalaliDate Date();
    QString toString();
    int Year();
    int Month();
    int Day();
    int dayOfWeek();
    QString dayOfWeek_str();

private:
    int year;
    int month;
    int day;

signals:

public slots:


};

#endif // JALALIDATE_H
