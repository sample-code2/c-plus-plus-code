#ifndef DELAY_H
#define DELAY_H

#include <QObject>

class Delay : public QObject
{
    Q_OBJECT
public:
    explicit Delay(QObject *parent = 0);
    static void delay_ms(quint32 ms);
    
signals:
    
public slots:
    
};

#endif // DELAY_H
