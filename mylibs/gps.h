#ifndef GPS_H
#define GPS_H

#include <QObject>
#include <QDateTime>
#include <QTimer>
#include "qextserialport.h"

class GPS : public QObject
{
    Q_OBJECT
public:
    typedef struct { //len=26
        QDate Date;
        QTime Time;
        float Longitude;
        float Latitude;
        quint16 Altitude;
        quint8 Dir;
        quint16 Speed;
        quint16 Course;
        quint8 NumberOfSatellite;
    } AVL_INFO;

    explicit GPS(QString portName, QObject *parent = 0);
    bool OpenUart();
    bool dataIsValid();
    AVL_INFO parameters();

private:
    typedef enum {
        NONEMode,
        GPRMCMode,
        GPGGAMode,
        GPGSVMode
    } GPS_INFO_MODE;

    typedef enum{
        NE=1,
        NW=2,
        SE=3,
        SW=4
    }EARTH_DIRECTION;

    QextSerialPort *_gpsPort;
    bool Str_Interupt(QString str, quint8 *pos, QChar input, bool CaseSensitive);    

    QString GPRMC;
    QString GPGGA;
    QString GPGSV;
    quint8 GPRMC_Pos;
    quint8 GPGGA_Pos;
    quint8 GPGSV_Pos;
    quint8 CommaCount;
    GPS_INFO_MODE GpsInfoMode;
    quint16 GpsFailRecived_Counter;
    QString GPSTempBuffer;
    bool Gps_Is_Valid;
    quint16 GpsReadStatusTrue_Counter;
    bool _NS;
    AVL_INFO INFO;
    AVL_INFO PreviousAvlInfo;
    QTimer *ActionTimer;
    quint16 Avl_Buffering_Counter;
    quint8 lastNumberOfSatellite;
    quint8 lastGpsState;
    quint8 MovingTimeCounter;

signals:
    void GpsAntena_Changed(quint8);
    void GpsState_Changed(bool);
    void newDateTime(QDateTime dateTime);
    void BusMoved();
    void dataNotRecived();
    
public slots:

private slots:
    QByteArray ReadUart();
    void ActionProcess();
    
};

#endif // GPS_H
