#ifndef GSM_H
#define GSM_H

#include <QObject>
#include <QtNetwork>
#include "gpio.h"
#include "qextserialport.h"

class GSM : public QObject{
    Q_OBJECT

public:
    typedef struct {
        //len=26
        quint8 Gsm_Ant;
        quint16 Sim_Charge_Value;
        quint16 Bat_Charge_Value;
    } GSM_INFO;

    typedef enum{
        NotSend = 0,
        Sent = 1,
        WaitForAck = 2
    } SEND_STATE;

    typedef enum{
        AVL_DATA,
        E_Tiket_DATA
    }DATA_TYPE;

    typedef enum{
        MODEM_MODE,
        AT_MODE,
        NONE_MODE
    }CONNECTION_MODE;

    typedef enum{
        IDLE_STATE,
        CONNECTING_STATE,
        DISCONNECTING_STATE,
        AT_STATE
    }GSM_STATE;

    explicit GSM(QString portName, QObject *parent=0);

    void setAPN(QString apn);

    bool isOn();
    bool isConnect(int timeOut_ms=15000, QUrl url=QUrl("http://iran.ir"));
    bool isConnectInterface();
    void AT_Command(QString Command, quint16 delay_ms=20);
    bool configGSM();
    bool configGPS();
    bool isPortInUse();
    quint64 getIMEI();
    quint64 IMEI();
    quint32 getSimCharge();
    quint16 GetBattryValue();
    quint8 GetGsmAntValue();
    GSM_INFO gsmInfo();
    CONNECTION_MODE connectionMode();

private:
    bool connectToAPN();
    bool connectToAPN(QString apn);
    bool disconnectFromAPN();

    bool OpenUart();
    void CloseUart();
    qint64 WriteUart(QByteArray array);
    bool Str_Interupt(QString str, quint8 *pos, QChar input, bool CaseSensitive);
    bool isEchoMode();
    QHostAddress pppIp();

    QextSerialPort *_GsmPort;
    QByteArray ReciveBuffer;
    GPIO *_powerPin;
    GPIO *_statusPin;
    GPIO *_ringDetectPin;
    QTimer *ActionTimer;           
    bool _errorRecived;
    bool _sendOkRecived;
    bool _connectOk;
    bool _Rials_Available;
    CONNECTION_MODE _ConnectionMode;
    GSM_STATE mutexOfGsmState;

    QString CMTI;
    quint8 CMTI_Pos;
    QString SendAck;
    quint8 SendAck_Pos;
    QString SendOk;
    quint8 SendOk_Pos;
    QString CONNECT;
    quint8 CONNECT_Pos;
    QString ERROR;
    quint8 ERROR_Pos;
    QString Rial;
    quint8 Rial_Pos;
    QString Ring;
    quint8 Ring_Pos;
    QString CPIN;
    quint8 CPIN_Pos;
    QString UPDATE;
    quint8 UPDATE_Pos;

    quint16 Card_FullSend_Counter;
    quint16 Card_PartialSend_Counter;
    quint16 Avl_Partialsend_counter;
    quint16 Avl_FullSend_Counter;
    quint16 IsConnectToAPN_Counter;
    quint16 ringTime_Counter;
    quint16 Get_Charge_Counter;
    quint16 Get_GsmParameters_Counter;
    quint16 PowerCheck_Counter;

    GSM_INFO _GsmInfo;

public slots:
    bool setConnectionMode(CONNECTION_MODE mode);
    bool powerOn();
    bool powerOff();
    bool powerReset();
    void reconfig();
    void answerIncomingCall();

private slots:
    QByteArray ReadUart();
    void ActionProcess();    

signals:
    void error_Recived();
    void connectOk();
    void rial_Recived();
    void SendOk_Recived();
    void SimCharge_Changed();
    void GsmAntena_Changed(quint8 value);
    void BattryValue_Changed(quint16 value);
    void GsmState_Changed(bool);
    void New_pppIp_Available();
    void closeMySql_request();
    void ringing();

};

#endif // GSM_H
