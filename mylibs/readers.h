#ifndef READERS_H
#define READERS_H

#include <QObject>
#include <QSqlDatabase>
#include <QTimeLine>
#include "qextserialport.h"
#include "MyTypes.h"
#include "Defines.h"
#include "gpio.h"


#define UnknownDriver    1
#define GlobalReaderId   7

class ReaderThread;

class Readers : public QObject
{
    Q_OBJECT
public:
    typedef union{
        char Byte;
        struct{
            quint8 Address:3;
            quint8 Command:5;
        }Parameters;
    }DC_COMMAND;
    typedef struct {
        quint64 Pasenger_ID;
        quint64 Driver_ID;
        quint32 Datetime;
        quint32 Bus_ID;
        quint32 Line_Number;
        quint32 RemainingPrice;
        quint16 Price;
        quint8  CardType;
        quint8  Reader_ID;
        quint8 CheckSum;
    } CARD_INFO;
    typedef struct {
        quint32 Datetime;
        quint32 Bus_ID;
        quint32 Line_Number;
        quint16 Price[10];
    } READER_CONFIGS;

    typedef enum {
        Sos_Card=4,
        Maintenance_CARD=5,
        SHOOTINGGALLERY_CARD=6,
        CarWash_CARD=7,
        PassengerController_CARD=8,
        DriverCintroller_CARD=9,
        DriverManager_CARD=10,
        LineChanger_CARD=11,
        CardCharger_CARD=12,
        Bus_Owner=13,
        CardDistrobuter=14,
        NONE_CARD=255
    } MANAGERS_CARD_TYPE;

    typedef enum{
        Request_Command =0b00001,
        StartData_Command =0b00010,
        EndData_Command =0b00011,
        DriverLogin_Command = 0b00100,
        Settings_Command = 0b00101,
        Ack_Command = 0b00110,
        SetReaderId_Command = 0b00111,
        ERROR_Command = 0b01000,
        FoematReaderMemory_Command = 0b01001,
        ManageCardsLogin_Command = 0b01010,
        Finish_Command = 0b11111
    }COMMAND;
    typedef enum{
        NONE_STATE,
        READY_STATE,
        WaitForCardData_STATE,
        WaitForDriverId_STATE,
        SendDriverIdForReader1_STATE,
        SendDriverIdForReader2_STATE,
        SendDriverIdForReader3_STATE,
        SendConfigForReader1_STATE,
        SendConfigForReader2_STATE,
        SendConfigForReader3_STATE,
        SendReaderId_STATE,
        SendFormatMemory1_STATE,
        SendFormatMemory2_STATE,
        SendFormatMemory3_STATE,
        WaitForManageCards_STATE
    }STATE;

    explicit Readers(QString portName, QObject *parent = 0);
    bool OpenUart(QString portName);

private:
    CARD_INFO extractCardInfo(QByteArray data);
    bool sendCommand(quint8 address, quint8 command);
    void writeData(QByteArray array);
    quint8 CalculateChecksum(QByteArray data);
    bool insertToDataBase(QList<CARD_INFO> infoList);
    bool commandRecived(DC_COMMAND cmd);
    void applyConfig(quint8 _activeReader);

    quint8 activeReader;
    STATE State;
    QextSerialPort *ReaderPort;
    QByteArray ReciveBuffer;
    QList<CARD_INFO> Recived_CardList;
    bool Direction;
    bool CommandRecivedFlag;
    QTimer *recuestTimer;
    QTimer *directionTimer;
    QTimer *commandTimoutTimer;
    quint8 ReaderId;
    quint64 Driver_ID;
    quint64 Manager_ID;
    QByteArray Manager_DATA;
    quint16 Manager_Password;
    MANAGERS_CARD_TYPE Manager_Type;
    bool dataIsvalid;
    quint8 readerCount;
    quint8 readerIdAvailable;
    quint8 readerFormatAvailable;
    GPIO *_DirectionPin;
    GPIO *_Reader1PowerControlerPin;
    GPIO *_Reader2PowerControlerPin;
    GPIO *_Reader3PowerControlerPin;

public slots:
    qint64 WriteUart(QByteArray array);
    void ReadUart();
    void newConfigAvailable();
#ifdef MY210
    void applyReaderId();
#elif defined TINY210
    void applyReaderId(quint8);
#endif
    void newReaderCountAvailable(quint8 count);
    void formatReader(quint8 readerId);

private slots:
    bool sendRequest();
    void setDirection(bool direction);
    void setDirectionToRecive();
    void commandTimeOut();

signals:
    void readyRead(QByteArray);
    void newCardData_Recived(QList<Readers::CARD_INFO>);
    void startDownloading1();
    void startDownloading2();
    void endDownloading();
    void driverLogined(quint64);
    void readerIdSent(quint8);
    void FormatOk(quint8);
    void ConfigOk(quint8);
    void ManagerLogined(MANAGERS_CARD_TYPE type, quint16 Pass);
};

#endif // READERS_H
