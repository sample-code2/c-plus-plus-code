#ifndef PRINTER_H
#define PRINTER_H

#include <QObject>
#include "qextserialport.h"
#include <QDate>
#include "MyTypes.h"

class Printer : public QObject
{
    Q_OBJECT
public:

    typedef enum{E17_MODEL, SP_MP_D6_MODE, NONE_MODEL} PRINTER_MODEL;
    typedef enum{EN, FA, AR} LANGUAGE;
    typedef enum{BACKWARD, BOTHSIDE, ELSE} CHARSTATE;

    explicit Printer(QString portName="", PRINTER_MODEL model=NONE_MODEL, QObject *parent = 0);
    QByteArray reverseArray(QByteArray array);
    void print_FA(QString str);
    void printNumber_FA(QString str);
    void print_EN(QString str);
    void printDateTime(QDateTime dateTime);
    bool OpenUart(QString portName);
    void changeLanguage(LANGUAGE lang);
    void initPrinter();
    void ReversePrint(bool action);
    void sendEnter(quint8 count);
    void newLine(quint8 count);
    void printSpace(quint8 count);
private:

    QextSerialPort *PrinterPort;
    CHARSTATE charState(QChar ch);
    PRINTER_MODEL _model;

signals:
    
public slots:
    qint64 WriteUart(QByteArray array);
};

#endif // PRINTER_H
