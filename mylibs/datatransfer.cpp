#include "datatransfer.h"
#include "settings.h"
#include "mainview.h"
#include "mydatetime.h"
#include "Defines.h"
#include "QEventLoop"
#include "delay.h"
#include <QCryptographicHash>

DataTransfer::DataTransfer(QObject *parent):QObject(parent){
    SendIpv4 = false;

    QSqlDatabase Sqlite_Eticket_DB = QSqlDatabase::addDatabase("QSQLITE", "Local_Eticket_Sqlite_DtThtread");
    Sqlite_Eticket_DB.setDatabaseName("./DB/E_Ticket.db");

    QSqlDatabase Sqlite_Avl_DB = QSqlDatabase::addDatabase("QSQLITE", "Local_Avl_Sqlite_DtThtread");
    Sqlite_Avl_DB.setDatabaseName("./DB/Avl.db");

    QSqlDatabase Sqlite_Send_DB = QSqlDatabase::addDatabase("QSQLITE", "Local_Send_Sqlite_DtThtread");
    Sqlite_Send_DB.setDatabaseName("./DB/Send.db");

    QSqlDatabase Sqlite_Bus_DB = QSqlDatabase::addDatabase("QSQLITE", "Local_Bus_Sqlite_DtThtread");
    Sqlite_Bus_DB.setDatabaseName("./DB/Bus.db");

    MySql_DB = QSqlDatabase::addDatabase("QMYSQL", "Server_MySql_DtThtread");
    MySql_DB.setHostName(Settings::ServerIp());
    MySql_DB.setDatabaseName("bus");
    MySql_DB.setUserName("root");
    MySql_DB.setConnectOptions("MYSQL_OPT_RECONNECT=1;CLIENT_COMPRESS=1");
    MySql_DB.setPassword("MyPaSsTaB1392");

    sendAvlTimer = new QTimer(this);
    sendAvlTimer->setInterval(Settings::AvlSendRate()*1000);
    connect(sendAvlTimer, SIGNAL(timeout()), SLOT(InsertAvlData_To_ServerDB()));
    sendAvlTimer->start();

    sendCardTimer = new QTimer(this);
    sendCardTimer->setInterval(Settings::CardSendRate()*1000);
    connect(sendCardTimer, SIGNAL(timeout()), SLOT(InsertCardData_To_ServerDB()));
    sendCardTimer->start();

    sendPppIpTimer = new QTimer(this);
    sendPppIpTimer->setInterval(1000);
    connect(sendPppIpTimer, SIGNAL(timeout()), SLOT(Insert_pppIp_To_ServerDB()));
    sendPppIpTimer->start();
}

DataTransfer::~DataTransfer(){
    qLog << "end of DataTransfer object";
    QSqlDatabase::removeDatabase("Local_Eticket_Sqlite_DtThtread");
    QSqlDatabase::removeDatabase("Local_Avl_Sqlite_DtThtread");
    QSqlDatabase::removeDatabase("Local_Send_Sqlite_DtThtread");
    QSqlDatabase::removeDatabase("Local_Bus_Sqlite_DtThtread");
    QSqlDatabase::removeDatabase("Server_MySql_DtThtread");

    delete sendAvlTimer;
    delete sendCardTimer;
    delete sendPppIpTimer;
}

void DataTransfer::closeMySqlDB(){
    qLog << "*********************close mysql***************************";
    MySql_DB.close();
}

bool DataTransfer::isConnectToInternet(){
    return QDir().exists("/var/run/ppp0-ok");
}

bool DataTransfer::InsertCardData_To_ServerDB(){
    if (!isConnectToInternet()){
        qLog << "*******************card sending start fail. modem not connected***********************";
        return false;
    }

    qLog << "*******************start send card***********************";
    QSqlQuery Select_Query(QSqlDatabase::database("Local_Eticket_Sqlite_DtThtread"));
    QSqlQuery Update_Query(QSqlDatabase::database("Local_Eticket_Sqlite_DtThtread"));
    QSqlRecord PreviousRecord, CurrentRecord;
    QList<quint32> IndexOfSendCardSToServer;
    QDateTime RecordDateTime;
    QString queryText;
    quint8 Counter=0;
    QByteArray data;
    quint64 tmp64=0;
    quint32 tmp32=0;
    quint16 tmp16=0;
    quint8 tmp8=0;

    /**********************read from local data base**************************/
    Select_Query.exec(QString("SELECT * FROM cardinfo WHERE issent = 0 ORDER BY datetime DESC LIMIT %0;").arg(60));
    if(!Select_Query.first()) {
        emit eTicketDB_Empty();
        qLog << "eTicket database empty.";
        return false;
    }
    qLog << Select_Query.executedQuery();

    data.clear();
    IndexOfSendCardSToServer.clear();
    Select_Query.first();
    while(Select_Query.isValid()){
        CurrentRecord = Select_Query.record();
        RecordDateTime = MyDateTime::extractDateTime(CurrentRecord.value("datetime").toUInt());

        if(data.length() == 0){
            //append IMEI
            tmp64 = Settings::IMEI();
            data.append((const char *)&tmp64, 8);
            //append bus serial
            tmp32 = Settings::BusSerial();
            data.append((const char *)&tmp32, 4);
            //append bus Version
            data.append(Settings::Version());
        }

        if(data.length() == 15 ||
                MyDateTime::extractDateTime(PreviousRecord.value("datetime").toUInt()).date() !=  RecordDateTime.date()||
                PreviousRecord.value("driverserial").toUInt() != CurrentRecord.value("driverserial").toUInt()||
                PreviousRecord.value("linenumber").toUInt() != CurrentRecord.value("linenumber").toUInt() ||
                PreviousRecord.value("busserial").toUInt() != CurrentRecord.value("busserial").toUInt()
                ){
            tmp8 =1;
            data.append((const char *)&tmp8, 1);
            //Date
            tmp8 = RecordDateTime.date().year()%2000;
            data.append((const char *)&tmp8, 1);
            tmp8 = RecordDateTime.date().month();
            data.append((const char *)&tmp8, 1);
            tmp8 = RecordDateTime.date().day();
            data.append((const char *)&tmp8, 1);
            //Driver_Card_Serial
            tmp32 = CurrentRecord.value("driverserial").toUInt();
            data.append((const char *)&tmp32, 4);
            //Line_Number
            tmp32 = CurrentRecord.value("linenumber").toUInt();
            data.append((const char *)&tmp32, 4);
            //Bus_Serial
            tmp32 = CurrentRecord.value("busserial").toUInt();
            data.append((const char *)&tmp32, 4);
        }
        tmp8 =2;
        data.append((const char *)&tmp8, 1);
        //Record_Number
        tmp32 = CurrentRecord.value("index").toUInt();
        data.append((char *)&tmp32, 4);
        //Passenger_Card_Serial
        tmp32 = CurrentRecord.value("rfid").toUInt();
        data.append((char *)&tmp32, 4);
        //Card_Type
        tmp8 = CurrentRecord.value("cardtype").toUInt();
        data.append((const char *)&tmp8, 1);
        //Time
        tmp8 = RecordDateTime.time().hour();
        data.append((const char *)&tmp8, 1);
        tmp8 = RecordDateTime.time().minute();
        data.append((const char *)&tmp8, 1);
        tmp8 = RecordDateTime.time().second();
        data.append((const char *)&tmp8, 1);
        //Ticket_Price
        tmp16 = CurrentRecord.value("price").toUInt();
        data.append((const char *)&tmp16, 2);
        //Remain_PriceAvl_Partialsend_counter
        tmp16 = CurrentRecord.value("remainprice").toUInt();
        data.append((const char *)&tmp16, 2);
        //Reader_ID
        tmp8 = CurrentRecord.value("readerid").toUInt();
        data.append((const char *)&tmp8, 1);

        Counter++;
        IndexOfSendCardSToServer.append(CurrentRecord.value("index").toUInt());

        if (data.length() >= MaxPcketLen) break;

        PreviousRecord = CurrentRecord;
        Select_Query.next();
    }
    qLog;
    if(!Counter) return false;
    //if(!Counter || (mode == FullPacket && data.length() < MaxPcketLen)) return false;

    /*********************insert data to remote database*********************/
    queryText = QString( "INSERT INTO  cardinfo_bin ( recordNumber, data ) VALUES ( %0, x'%1' );")
            .arg(QString("%0%1").arg(IndexOfSendCardSToServer.at(IndexOfSendCardSToServer.size()-1)).arg(Settings::BusSerial(),6,10,QChar('0')))
            .arg(QString(data.toHex()));

    qLog << queryText;
    QSqlQuery insertQuery(QSqlDatabase::database("Server_MySql_DtThtread"));
    insertQuery.prepare(queryText);
    if(!insertQuery.exec()){
        qLog << insertQuery.lastError().number()<<insertQuery.lastError().text()<<insertQuery.lastError().type();
        if(insertQuery.lastError().type() == QSqlError::StatementError){
            if(insertQuery.lastError().number()==1062){//Duplicate entry for key PRIMARY
                qWarning() << "delete this package sines exsist on server";
                qLog;
            }else if(insertQuery.lastError().number()==2006){//MySQL server has gone away
                QSqlDatabase::database("Server_MySql_DtThtread").close();
            }
        }else if(insertQuery.lastError().type() == QSqlError::ConnectionError){
            QSqlDatabase::database("Server_MySql_DtThtread").close();
        }else if(insertQuery.lastError().type() == QSqlError::TransactionError){
            qLog;
        }else if(insertQuery.lastError().type() == QSqlError::UnknownError){
            qLog;
        }else{
            qLog;
        }
        return false;
    }
    qLog << insertQuery.executedQuery();

    /*Set flag for sent packages*/
    queryText = QString("UPDATE 'cardinfo' SET issent=1 WHERE 'cardinfo'.'index' IN(");
    quint8 size= IndexOfSendCardSToServer.size();
    for(quint8 i=0; i<(size-1); i++){
        queryText += QString("%0, ").arg(IndexOfSendCardSToServer.at(i));
    }
    if (size>0) queryText += QString("%0 );").arg(IndexOfSendCardSToServer.at(size-1));

    qLog << queryText;
    if(!Update_Query.exec(queryText)) return false;

    qLog << "*******************end send card***********************";
    return true;
}

bool DataTransfer::InsertAvlData_To_ServerDB(){
    if (!isConnectToInternet()){
        qLog << "*******************avl sending start fail. modem not connected***********************";
        return false;
    }
    qLog << "*******************start send avl***********************";
    QSqlQuery Select_Query(QSqlDatabase::database("Local_Avl_Sqlite_DtThtread"));
    QSqlQuery Update_Query(QSqlDatabase::database("Local_Avl_Sqlite_DtThtread"));
    QSqlRecord PreviousRecord, CurrentRecord;
    QList<quint32> IndexOfSendAvlsToServer;
    QDateTime RecordDateTime;
    QString queryText;
    quint8 Counter=0;
    QByteArray data;
    quint64 tmp64=0;
    quint32 tmp32=0;
    quint16 tmp16=0;
    quint8 tmp8=0;
    float tmpFloat=0;


    Select_Query.exec(QString("SELECT * FROM avlinfo WHERE issent = 0 ORDER BY datetime DESC LIMIT %1;").arg(60));
    if(!Select_Query.first()) return false;
    qLog << Select_Query.executedQuery();

    data.clear();
    IndexOfSendAvlsToServer.clear();
    Select_Query.first();
    while(Select_Query.isValid()){
        CurrentRecord = Select_Query.record();
        RecordDateTime = MyDateTime::extractDateTime(CurrentRecord.value("datetime").toUInt());

        if(data.length() == 0){
            //append IMEI
            tmp64 = Settings::IMEI();
            data.append((const char *)&tmp64, 8);
            //append bus serial
            tmp32 = Settings::BusSerial();
            data.append((const char *)&tmp32, 4);
            //append bus Version
            data.append(Settings::Version());
        }
        if(data.length() == 15 ||
                MyDateTime::extractDateTime(PreviousRecord.value("datetime").toUInt()).date() !=  RecordDateTime.date() ||
                PreviousRecord.value("sim_charge_value").toUInt() != CurrentRecord.value("sim_charge_value").toUInt() ||
                PreviousRecord.value("busserial").toUInt() != CurrentRecord.value("busserial").toUInt()
                ){
            tmp8 =100;
            data.append((const char *)&tmp8, 1);
            //Date
            tmp8 = RecordDateTime.date().year()%2000;
            data.append((const char *)&tmp8, 1);
            tmp8 = RecordDateTime.date().month();
            data.append((const char *)&tmp8, 1);
            tmp8 = RecordDateTime.date().day();
            data.append((const char *)&tmp8, 1);
            //Sim_Charge_Value
            tmp16 = CurrentRecord.value("sim_charge_value").toUInt();
            data.append((const char *)&tmp16, 2);
            //Bat_Charge_Value
            tmp16 = CurrentRecord.value("bat_charge_value").toUInt();
            data.append((const char *)&tmp16, 2);
            //Gps_Ant
            tmp8 = CurrentRecord.value("gps_ant").toUInt();
            data.append((const char *)&tmp8, 1);
            //Gsm_Ant
            tmp8 = CurrentRecord.value("gsm_ant").toUInt();
            data.append((const char *)&tmp8, 1);
            //Bus_Serial
            tmp32 = CurrentRecord.value("busserial").toUInt();
            data.append((const char *)&tmp32, 4);
        }
        tmp8 =101;
        data.append((const char *)&tmp8, 1);

        //Record_Number
        tmp32 = CurrentRecord.value("index").toUInt();
        data.append((char *)&tmp32, 4);

        //Time
        tmp8 = RecordDateTime.time().hour();
        data.append((const char *)&tmp8, 1);
        tmp8 = RecordDateTime.time().minute();
        data.append((const char *)&tmp8, 1);
        tmp8 = RecordDateTime.time().second();
        data.append((const char *)&tmp8, 1);

        //Lon
        tmpFloat = CurrentRecord.value("longitude").toFloat();
        data.append((char *)&tmpFloat, 4);

        //Lat
        tmpFloat = CurrentRecord.value("latitude").toFloat();
        data.append((char *)&tmpFloat, 4);

        //Alt
        tmp16 = CurrentRecord.value("altitude").toUInt();
        data.append((const char *)&tmp16, 2);

        //Speed
        tmp16 = CurrentRecord.value("speed").toUInt();
        data.append((const char *)&tmp16, 2);

        //Course
        tmp8= CurrentRecord.value("course").toUInt()/2;
        data.append((const char *)&tmp8, 1);

        //Direction
        tmp8= CurrentRecord.value("dir").toUInt();
        data.append((const char *)&tmp8, 1);

        Counter++;
        IndexOfSendAvlsToServer.append(CurrentRecord.value("index").toUInt());

        if (data.length() >= MaxPcketLen) break;

        PreviousRecord = CurrentRecord;
        Select_Query.next();
    }
    //if(!Counter || (mode == FullPacket && data.length() < MaxPcketLen)) return false;
    if(!Counter) return false;


    /*********************insert data to remote database*********************/
    queryText = QString("INSERT INTO  avlinfo_bin ( recordNumber, data ) VALUES ( %0, x'%1' );")
            .arg(QString("%0%1").arg(IndexOfSendAvlsToServer.at(IndexOfSendAvlsToServer.size()-1)).arg(Settings::BusSerial(),6,10,QChar('0')))
            .arg(QString(data.toHex()));

    qLog << queryText;
    QSqlQuery insertQuery(QSqlDatabase::database("Server_MySql_DtThtread"));
    insertQuery.prepare(queryText);
    if(!insertQuery.exec()){
        qLog << insertQuery.lastError().number()<<insertQuery.lastError().text()<<insertQuery.lastError().type();
        if(insertQuery.lastError().type() == QSqlError::StatementError){
            if(insertQuery.lastError().number()==1062){//Duplicate entry for key PRIMARY
                qLog << "delete this package sines exsist on server";
            }else if(insertQuery.lastError().number()==2006){//MySQL server has gone away
                QSqlDatabase::database("Server_MySql_DtThtread").close();
            }
        }else if(insertQuery.lastError().type() == QSqlError::ConnectionError){
            QSqlDatabase::database("Server_MySql_DtThtread").close();
        }else if(insertQuery.lastError().type() == QSqlError::TransactionError){
            qLog;
        }else if(insertQuery.lastError().type() == QSqlError::UnknownError){
            qLog;
        }else{
            qLog;
        }
        return false;
    }
    qLog << insertQuery.executedQuery();

    /*Set flag for sent packages*/
    queryText = QString("UPDATE 'avlinfo' SET issent=1 WHERE 'avlinfo'.'index' IN(");
    quint8 size= IndexOfSendAvlsToServer.size();

    for(quint8 i=0; i<(size-1); i++){
        queryText += QString("%0, ").arg(IndexOfSendAvlsToServer.at(i));
    }
    if (size>0) queryText += QString("%0 );").arg(IndexOfSendAvlsToServer.at(size-1));

    qLog << queryText;
    if(!Update_Query.exec(queryText)) return false;
    qLog << "*******************end send avl***********************";

    return true;
}

bool DataTransfer::executePackage_Query(QUERY_TYPE type){
    qLog;

    QSqlQuery Delete_Query(QSqlDatabase::database("Local_Send_Sqlite_DtThtread"));
    QSqlQuery Select_Query(QSqlDatabase::database("Local_Send_Sqlite_DtThtread"));

    /*****************select a query from querylog table********************/
    Select_Query.exec(QString("SELECT * FROM 'datalog' WHERE dataType=%0 ORDER BY 'datalog'.'index' DESC LIMIT %1;").arg(type).arg(1));
    if(!Select_Query.first()) return false;
    qLog << Select_Query.executedQuery();
    qLog << Select_Query.record().value("data").toByteArray().toHex();

    //QString cmd = QString( "INSERT INTO  %0 ( recordNumber, data ) VALUES ( :recordNumber, :data );").arg(type == InsertAvls ? "avlinfo_bin":"cardinfo_bin");

    QString cmd = QString( "INSERT INTO  %0 ( recordNumber, data ) VALUES ( %1, x'%2' );")
            .arg(type == InsertAvls ? "avlinfo_bin":"cardinfo_bin")
            .arg(QString("%0%1").arg(Select_Query.record().value("index").toUInt()).arg(Settings::BusSerial(),6,10,QChar('0')))
            .arg(QString(Select_Query.record().value("data").toByteArray().toHex()));

    qLog << cmd;
    QSqlQuery insertQuery(QSqlDatabase::database("Server_MySql_DtThtread"));
    insertQuery.prepare(cmd);
    if(!insertQuery.exec()){

        qLog << insertQuery.lastError().number()<<insertQuery.lastError().text()<<insertQuery.lastError().type();

        if(insertQuery.lastError().type() == QSqlError::StatementError){
            if(insertQuery.lastError().number()==1062){//Duplicate entry for key PRIMARY

                qWarning() << "delete this package sines exsist on server";
                Delete_Query.exec(QString("DELETE FROM 'datalog' WHERE 'datalog'.'index'=%0;").arg(Select_Query.record().value("index").toULongLong()));

            }else if(insertQuery.lastError().number()==2006){//MySQL server has gone away

                QSqlDatabase::database("Server_MySql_DtThtread").close();

            }
        }else if(insertQuery.lastError().type() == QSqlError::ConnectionError){

            QSqlDatabase::database("Server_MySql_DtThtread").close();

        }else if(insertQuery.lastError().type() == QSqlError::TransactionError){
            qLog;
        }else if(insertQuery.lastError().type() == QSqlError::UnknownError){
            qLog;
        }else{
            qLog;
        }
        return false;
    }
    qLog << insertQuery.executedQuery();

    Delete_Query.exec(QString("DELETE FROM 'datalog' WHERE 'datalog'.'index'=%0;").arg(Select_Query.record().value("index").toULongLong()));
    if(Delete_Query.lastError().isValid()){
        return false;
    }
    qLog << "insert OK.";

    return true;
}

void DataTransfer::Insert_pppIp_To_ServerDB_recuest(){
    SendIpv4 = true;
}

bool DataTransfer::Insert_pppIp_To_ServerDB(){

    if(!SendIpv4){
        //    qLog << "*******************new ip not exist.***********************";
        return false;
    }
    if (!isConnectToInternet()){
        qLog << "*******************sending start fail. modem not connected***********************";
        return false;
    }
    qLog << "*****************start send pppIp***********************";
    qLog <<  QSqlDatabase::database("Server_MySql_DtThtread").open();
    QSqlQuery insertQuery(QSqlDatabase::database("Server_MySql_DtThtread"));
    QSqlQuery selectQuery(QSqlDatabase::database("Server_MySql_DtThtread"));
    if(!MySql_DB.isOpen()) return false;

    QString query_str = QString("select busSerial from console_ipv4 where busSerial = %0 ;").arg(Settings::BusSerial());
    qLog;
    if(selectQuery.exec(query_str)){
        qLog;
        if(selectQuery.first()){
            qLog;
            query_str = QString("update console_ipv4 set ip='%0' where busSerial=%1;").arg(Settings::pppIp()).arg(Settings::BusSerial());
            if(!insertQuery.exec(query_str)){
                qLog << "update pppIp Fail..." << insertQuery.lastError() << insertQuery.lastError().number();
                return false;
            }
            qLog << "update pppIp Ok...";
        }else{
            qLog;
            query_str = QString("INSERT INTO console_ipv4 (busSerial, ip) VALUES(%0, '%1');").arg(Settings::BusSerial()).arg(Settings::pppIp());
            if(!insertQuery.exec(query_str)){
                qLog << "insert pppIp Fail..." << insertQuery.lastError() << insertQuery.lastError().number();
                return false;
            }
            qLog << "insert pppIp Ok...";
        }
    }else{
        qLog << selectQuery.lastError() << selectQuery.lastError().number();
        return false;
    }
    qLog << "*******************end send pppIp***********************";
    SendIpv4= false;
    return true;
}

void DataTransfer::clearCardSendCounter(){
    qLog << "clearCardSendCounter";
    sendCardTimer->start();
}

bool DataTransfer::forceUnloade_Eticket(){
    //تمامی تایمرهای عملیاتهای غیر از ارسال کارتها متوقف شود
    //    sendAvlTimer->stop();
    //    sendPppIpTimer->stop();
    //    bool result=0;
    //    while ((result = InsertCardData_To_ServerDB())){
    //        if(!){
    //            emit forceUnload_End(false);
    //            return false;
    //        }
    //    }
    //    qLog << "force Unload end";
}

/*************************************************************************/

DataTransfer_Thread::DataTransfer_Thread(QObject *parent):QThread(parent){
    setTerminationEnabled(true);

    isRunningTimer = new QTimer(this);
    isRunningTimer->setInterval(100*1000);
    isRunningTimer->start();
    connect(isRunningTimer, SIGNAL(timeout()), SLOT(reStartThread()));
}

DataTransfer_Thread::~DataTransfer_Thread(){

}

void DataTransfer_Thread::reStartThread(){
    qLog << isRunning()<<isFinished() << "DataTransfer_Thread hang up ...";

    disconnect(this, SIGNAL(New_pppIp_Available()), dataTransfer, SLOT(Insert_pppIp_To_ServerDB()));
    disconnect(this, SIGNAL(closeMySql_request()), dataTransfer, SLOT(closeMySqlDB()));
    disconnect(this, SIGNAL(clearCardSendCounter_request()), dataTransfer, SLOT(clearCardSendCounter()));
    qLog;
    terminate();
    qLog;
    setTerminationEnabled(false);
    //sleep(1000);
    qLog;
    delete dataTransfer;
    qLog << isRunning()<<isFinished();
    if(!isRunning()){
        qLog << "reStart DataTransfer_Thread";
        start();
    }
}

void DataTransfer_Thread::run(){
    /*هر شیئ که در این تابع تعریف میشود باید در تابع reStartThread delete.  شود*/

    setTerminationEnabled(true);

    dataTransfer = new DataTransfer();

    connect(this, SIGNAL(New_pppIp_Available()), dataTransfer, SLOT(Insert_pppIp_To_ServerDB_recuest()));
    connect(this, SIGNAL(closeMySql_request()), dataTransfer, SLOT(closeMySqlDB()));
    connect(this, SIGNAL(clearCardSendCounter_request()), dataTransfer, SLOT(clearCardSendCounter()));

    QTimer runningTimer;
    runningTimer.setInterval(1000);
    connect(&runningTimer, SIGNAL(timeout()), isRunningTimer, SLOT(start()));
    runningTimer.start();

    qLog << "data transfer thread exec/////////////////////////////////////////////////////////////////////////";
    exec();
}

void DataTransfer_Thread::clearCardSendCounter(){
    emit clearCardSendCounter_request();
}
