#include "printer.h"
#include "jalalidate.h"
#include "Defines.h"
#include <QDebug>
#include <QChar>

Printer::Printer(QString portName, PRINTER_MODEL model, QObject *parent) : QObject(parent){
    _model = model;

    if(OpenUart(portName)) qLog << ("Opening printer serial port "+portName+"\tOK.");
    else qLog << ("Opening printer serial port "+portName+"\tFAIL.");
}

Printer::CHARSTATE Printer::charState(QChar ch){
    QString BOTHSIDE_Group = QString("سشصضطظعغفقکگلمنبپتثجچحخهی");
    QString BACKWARD_Group = QString("ادذرزژو");

    for(int i=0; i<BOTHSIDE_Group.length(); i++){
        if(ch.unicode() == BOTHSIDE_Group.at(i).unicode()) return BOTHSIDE;
    }
    for(int i=0; i<BACKWARD_Group.length(); i++){
        if(ch.unicode() == BACKWARD_Group.at(i).unicode()) return BACKWARD;
    }
    return ELSE;
    //quint16 BOTHSIDE_Group[]={QChar('ئ').unicode(), QChar('گ').unicode(), QChar('ج').unicode(), QChar('ث').unicode(), QChar('ت').unicode(), QChar('ه').unicode(), QChar('ب').unicode(), QChar('خ').unicode(), QChar('ح').unicode(), QChar('ص').unicode(), QChar('ش').unicode(), QChar('س').unicode(), QChar('ع').unicode(), QChar('غ').unicode(), QChar('ط').unicode(), QChar('ظ').unicode(), QChar('ض').unicode(), QChar('ک').unicode(), QChar('ق').unicode(),  QChar('ف').unicode(), QChar('ن').unicode(), QChar('م').unicode(), QChar('ل').unicode(), QChar('چ').unicode(), QChar('پ').unicode(), QChar('ی').unicode()};
    //quint16 backward_Group[]={QChar('ژ').unicode(),QChar('ذ').unicode(),QChar('د').unicode(),QChar('ز').unicode(),QChar('ر').unicode(),QChar('و').unicode(),QChar('ا').unicode()};
}

void Printer::print_FA(QString str){
    QByteArray str_fa;
    QList<QByteArray> str_list;
    bool isFirstChar=false;
    bool isEndChar=false;

    for(qint16 i=0; i<str.length(); i++){
        if(i==0){isFirstChar=true; isEndChar=false;}
        else if(i<str.length()-1){isFirstChar=false; isEndChar=false;}
        else{isFirstChar=false; isEndChar=true;}


        if(str[i].isDigit()){
            if(str[i].unicode()==QString("0").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("21"));
            }else if(str[i].unicode()==QString("1").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("22"));
            }else if(str[i].unicode()==QString("2").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("23"));
            }else if(str[i].unicode()==QString("3").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("24"));
            }else if(str[i].unicode()==QString("4").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("25"));
            }else if(str[i].unicode()==QString("5").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("26"));
            }else if(str[i].unicode()==QString("6").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("27"));
            }else if(str[i].unicode()==QString("7").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("28"));
            }else if(str[i].unicode()==QString("8").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("29"));
            }else if(str[i].unicode()==QString("9").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("2A"));
            }

            if(isEndChar){
                str_list << str_fa;
                str_fa.clear();
            }else{

                if(!str[i+1].isDigit()){
                    str_list << str_fa;
                    str_fa.clear();
                }
            }

        }else{
            if(str[i].unicode()==QString("آ").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("38"));
            }else if(str[i].unicode()==QString("ا").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE)
                    str_fa.append(QByteArray::fromHex("3F"));
                else
                    str_fa.append(QByteArray::fromHex("3E"));
            }else if(str[i].unicode()==QString("ب").at(0).unicode()){
                //آیا به عقب میچسبد؟
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("42"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("41"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("43"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("40"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("پ").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("A8"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("A7"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("A9"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("A6"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ت").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("48"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("47"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("49"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("46"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ث").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("4C"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("4B"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("4D"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("4A"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ج").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("50"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("4F"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("51"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("4E"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("چ").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("AC"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("AB"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("AD"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("A9"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ح").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("54"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("53"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("55"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("52"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("خ").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("58"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("57"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("59"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("56"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("د").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE)
                    str_fa.append(QByteArray::fromHex("5B"));//چسبان
                else
                    str_fa.append(QByteArray::fromHex("5A"));//تک
            }else if(str[i].unicode()==QString("ذ").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE)
                    str_fa.append(QByteArray::fromHex("5D"));
                else
                    str_fa.append(QByteArray::fromHex("5C"));
            }else if(str[i].unicode()==QString("ر").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE)
                    str_fa.append(QByteArray::fromHex("5F"));
                else
                    str_fa.append(QByteArray::fromHex("5E"));
            }else if(str[i].unicode()==QString("ز").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE)
                    str_fa.append(QByteArray::fromHex("61"));
                else
                    str_fa.append(QByteArray::fromHex("60"));
            }else if(str[i].unicode()==QString("س").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("64"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("63"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("65"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("62"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ش").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("68"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("67"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("69"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("66"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ص").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("6C"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("6B"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("6D"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("6A"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ض").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("70"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("6F"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("71"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("6E"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ط").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("74"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("73"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("75"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("72"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ظ").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("78"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("77"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("79"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("76"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ع").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("7C"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("7B"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("7D"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("7A"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("غ").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("80"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("7F"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("81"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("7E"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ف").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("84"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("83"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("85"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("82"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ق").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("89"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("87"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("88"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("86"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ک").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("8C"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("8B"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("8D"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("8A"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("گ").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("32"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("31"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("33"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("30"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ل").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("90"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("8F"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("91"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("8E"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("م").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("94"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("93"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("95"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("92"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ن").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("98"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("97"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("99"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("96"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("و").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE)
                    str_fa.append(QByteArray::fromHex("9F"));
                else
                    str_fa.append(QByteArray::fromHex("9E"));
            }else if(str[i].unicode()==QString("ه").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("9C"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("9B"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("9D"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("9A"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString("ی").at(0).unicode()){
                if(!isFirstChar && charState(str[i-1])==BOTHSIDE){
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("A2"));//حرف وسط
                    }else{
                        str_fa.append(QByteArray::fromHex("A1"));//حرف آخر
                    }
                }else{
                    if(!isEndChar && (charState(str[i+1])==BOTHSIDE || charState(str[i+1])==BACKWARD)){
                        str_fa.append(QByteArray::fromHex("A3"));//حرف اول
                    }else{
                        str_fa.append(QByteArray::fromHex("A0"));//حرف تک
                    }
                }
            }else if(str[i].unicode()==QString(" ").at(0).unicode()){
                str_fa.append(QByteArray::fromHex("20"));
            }

            if(isEndChar){
                str_list << reverseArray(str_fa);
                str_fa.clear();
            }else{
                if(str[i+1].isDigit()){
                    str_list << reverseArray(str_fa);
                    str_fa.clear();
                }
            }
        }
    }
    changeLanguage(FA);
    for(quint8 i=0; i<str_list.size(); ++i){
        WriteUart(str_list.at(str_list.size()-1-i));
    }
    changeLanguage(EN);
}

void Printer::printNumber_FA(QString str){
    QByteArray str_fa;
    for(qint16 i=0; i<str.length(); i++){
        if(str[i].unicode()==QString("۰").at(0).unicode() || str[i].unicode()==QString("0").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("21"));
        }else if(str[i].unicode()==QString("۱").at(0).unicode() || str[i].unicode()==QString("1").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("22"));
        }else if(str[i].unicode()==QString("۲").at(0).unicode() || str[i].unicode()==QString("2").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("23"));
        }else if(str[i].unicode()==QString("۳").at(0).unicode() || str[i].unicode()==QString("3").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("24"));
        }else if(str[i].unicode()==QString("۴").at(0).unicode() || str[i].unicode()==QString("4").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("25"));
        }else if(str[i].unicode()==QString("۵").at(0).unicode() || str[i].unicode()==QString("5").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("26"));
        }else if(str[i].unicode()==QString("۶").at(0).unicode() || str[i].unicode()==QString("6").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("27"));
        }else if(str[i].unicode()==QString("۷").at(0).unicode() || str[i].unicode()==QString("7").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("28"));
        }else if(str[i].unicode()==QString("۸").at(0).unicode() || str[i].unicode()==QString("8").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("29"));
        }else if(str[i].unicode()==QString("۹").at(0).unicode() || str[i].unicode()==QString("9").at(0).unicode()){
            str_fa.append(QByteArray::fromHex("2A"));
        }
    }
    changeLanguage(FA);
    WriteUart(str_fa);
    changeLanguage(EN);
}

void Printer::print_EN(QString str){
    changeLanguage(EN);
    WriteUart(str.toAscii());
}

void Printer::sendEnter(quint8 count){
    for(int i=0; i<count; i++)
        WriteUart(QByteArray::fromHex("0D"));
}

void Printer::newLine(quint8 count){
    for(int i=0; i<count; i++)
        WriteUart(QByteArray::fromHex("0A"));
}

void Printer::printSpace(quint8 count){
    for(int i=0; i<count; i++)
        WriteUart(QByteArray::fromHex("20"));
}

QByteArray Printer::reverseArray(QByteArray array){
    QByteArray reversedArray;
    for(int i=0; i<array.length(); i++){
        reversedArray.append(array.at(array.length()-i-1));
    }
    return reversedArray;
}

bool Printer::OpenUart(QString portName){
    PortSettings setting = {BAUD9600, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};
    PrinterPort = new QextSerialPort(portName, setting, QextSerialPort::EventDriven, this);
    PrinterPort->open(QIODevice::ReadWrite);
    //connect(ReaderPort, SIGNAL(readyRead()), SLOT(ReadUart()));
    if(PrinterPort->isOpen()) return true;
    else return false;
}

qint64 Printer::WriteUart(QByteArray array){
    return PrinterPort->write(array);
}

void Printer::printDateTime(QDateTime dateTime){
    QJalaliDate jdate = QJalaliDate::FromGregorian(dateTime.date());

    if(dateTime.time().hour()<10)printNumber_FA("0");
    printNumber_FA(QString("%0").arg(dateTime.time().hour()));
    print_EN(":");
    if(dateTime.time().minute()<10)printNumber_FA("0");
    printNumber_FA(QString("%0").arg(dateTime.time().minute()));
    print_EN("  ");
    printNumber_FA(QString("%0").arg(jdate.Year()));
    print_EN("/");
    if(jdate.Month()<10)printNumber_FA("0");
    printNumber_FA(QString("%0").arg(jdate.Month()));
    print_EN("/");
    if(jdate.Day()<10)printNumber_FA("0");
    printNumber_FA(QString("%0").arg(jdate.Day()));
}

void Printer::changeLanguage(LANGUAGE lang){
    switch(lang){
    case EN:
        WriteUart(QByteArray::fromHex("1b 36 00"));
        break;
    case FA:
        WriteUart(QByteArray::fromHex("1b 36 02"));
        break;
    case AR:
        WriteUart(QByteArray::fromHex("1b 36 01"));
        break;
    }
}

void Printer::initPrinter(){
    WriteUart(QByteArray::fromHex("1B 40 0A"));// init printer
}

void Printer::ReversePrint(bool action){
    if(action)
        WriteUart(QByteArray::fromHex("1B 63 00"));// reversre page
    else
        WriteUart(QByteArray::fromHex("1B 63 01"));
}

