#ifndef DATATRANSFER_H
#define DATATRANSFER_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include <QtSql>
#include <QFuture>
#include "qextserialport.h"

class DataTransfer_Thread;

class DataTransfer : public QObject
{
    Q_OBJECT
public:
    typedef enum{
        InsertCarts=0,
        InsertAvls=1
    }QUERY_TYPE;

    explicit DataTransfer(QObject *parent = 0);
    ~DataTransfer();
    bool isConnectToInternet();

    QSqlDatabase MySql_DB ;
    bool SendIpv4;
private:
    bool executePackage_Query(QUERY_TYPE type);

    QTimer *sendAvlTimer;
    QTimer *sendCardTimer;
    QTimer *sendPppIpTimer;

signals:
    void eTicketDB_Empty();
    void forceUnload_End(bool status);
    
public slots:
    void Insert_pppIp_To_ServerDB_recuest();
    bool Insert_pppIp_To_ServerDB();
    void closeMySqlDB();
    void clearCardSendCounter();
    bool forceUnloade_Eticket();

private slots:
    bool InsertCardData_To_ServerDB();
    bool InsertAvlData_To_ServerDB();

};

class DataTransfer_Thread : public QThread
{
    Q_OBJECT
public:
    explicit DataTransfer_Thread(QObject *parent = 0);
    ~DataTransfer_Thread();

    void clearCardSendCounter();

private:
    virtual void run();

    QTimer *isRunningTimer;    
    DataTransfer *dataTransfer;

signals:
    void New_pppIp_Available();
    void closeMySql_request();
    void clearCardSendCounter_request();

public slots:    

private slots:
    void reStartThread();

};


#endif // DATATRANSFER_H
