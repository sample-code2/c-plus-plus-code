#include "updater.h"
#include "settings.h"
#include <QDebug>
#include <QMessageBox>
#include <MyTypes.h>
#include "ui/mainview.h"
#include "Defines.h"

Updater::Updater(QObject *parent):QObject(parent){
    ProgressBar = MainView::instance()->ProgressBar;

    ftp = new QFtp(parent);
    connect(ftp, SIGNAL(commandFinished(int,bool)), SLOT(ftpCommandFinished(int,bool)));
    connect(ftp, SIGNAL(dataTransferProgress(qint64,qint64)), SLOT(updateDataTransferProgress(qint64,qint64)));
    connect(ftp, SIGNAL(listInfo(QUrlInfo)), this, SLOT(getListInfo(QUrlInfo)));
}

Updater::~Updater(){
    file->close();
    delete file;
}

void Updater::startUpdate(QString fileName){
    remoteFileName = fileName;

    /*اگر ورژن درخواست شده روی کنسول موجود بود نیازی به دانلود نیست*/
    if (QFile::exists(Settings::ProgramPath() + remoteFileName)) {
        qDebug("this version already  exists.");
        QFile::setPermissions(Settings::ProgramPath() + remoteFileName, QFile::ExeOwner | QFile::ReadOwner | QFile::WriteOwner);

        updateScriptFile();
        return;
    }

    if(ftp->state() == QFtp::LoggedIn){
        downloadFile();
        return;
    }
    /*connect to ftp server*/
    ftp->connectToHost(Settings::ServerIp(), Settings::FtpPort());

    /*login to ftp server*/
    ftp->login(Settings::FtpUserName(), Settings::FtpPassword());
}

void Updater::ftpCommandFinished(int, bool error){
    if (ftp->currentCommand() == QFtp::ConnectToHost) {
        if (error) {
            qLog << "Unable to connect to the FTP server.";
            return;
        }
        qLog << "connect to ftp server ok.";
        return;
    }
    if (ftp->currentCommand() == QFtp::Login){
        if(error) qLog << "ftp login false.";
        else{
            qLog << "ftp login ok.";
            ftp->cd("Console/Programs/");
            ftp->list();
            ProgressBar->setFormat("%p% در حال به روز رسانی نرم افزار");
            ProgressBar->setVisible(true);
            ProgressBar->setValue(0);
            downloadFile();
        }
    }
    if (ftp->currentCommand() == QFtp::Get) {
        file->close();
        ProgressBar->hide();
        if (error) {
            qLog << "download error";
            file->remove();
        } else {
            qLog << "download finish";

            /*set executeable Permission downloaded file*/
            file->setPermissions(QFile::ExeOwner | QFile::ReadOwner | QFile::WriteOwner);

            /*فایل برنامه به مسیر اصلی انتقال داده شود*/
            file->copy(Settings::ProgramPath() + remoteFileName);
            file->remove();

            updateScriptFile();

            /*exit from console.*/
            exit(0);
        }
        delete file;
    } else if (ftp->currentCommand() == QFtp::List) {
        //        if (isDirectory.isEmpty()) {
        //            fileList->addTopLevelItem(new QTreeWidgetItem(QStringList() << tr("<empty>")));
        //        }
    }
}

void Updater::updateDataTransferProgress(qint64 readBytes, qint64 totalBytes){    
    ProgressBar->setMaximum(totalBytes);
    ProgressBar->setValue(readBytes);
}

void Updater::downloadFile(){

    if (QFile::exists(Settings::ProgramPath()+"temp/"+remoteFileName)) {
        QFile::remove(Settings::ProgramPath()+"temp/"+remoteFileName);
        qLog << QString("There already exists a file called %1 in the temp directory.\ndelete old file and creat new for dowload.").arg(remoteFileName);
    }

    /*create temp directory if not exist befor*/
    if(!QDir(Settings::ProgramPath()+"temp/").exists()){
        qLog << "temp dir not exist";
        QDir().mkpath(Settings::ProgramPath()+"temp/");
    }

    file = new QFile(Settings::ProgramPath()+"temp/"+remoteFileName);
    if (!file->open(QIODevice::WriteOnly)) {
        qLog << tr("Unable to save the file %1: %2.").arg(remoteFileName).arg(file->errorString());
        delete file;
        return;
    }

    ftp->get(remoteFileName, file);
}

void Updater::getListInfo(const QUrlInfo &urlInfo)
{
    qLog << urlInfo.name();
    //    QTreeWidgetItem *item = new QTreeWidgetItem;
    //    item->setText(0, urlInfo.name());
    //    item->setText(1, QString::number(urlInfo.size()));
    //    item->setText(2, urlInfo.owner());
    //    item->setText(3, urlInfo.group());
    //    item->setText(4, urlInfo.lastModified().toString("MMM dd yyyy"));

    //    QPixmap pixmap(urlInfo.isDir() ? ":/images/dir.png" : ":/images/file.png");
    //    item->setIcon(0, pixmap);

    //    isDirectory[urlInfo.name()] = urlInfo.isDir();
    //    fileList->addTopLevelItem(item);
    //    if (!fileList->currentItem()) {
    //        fileList->setCurrentItem(fileList->topLevelItem(0));
    //        fileList->setEnabled(true);
    //    }
}

void Updater::updateScriptFile(){
    /*در صورت عدم وجود فایل اسکریپت یک نمونه جایگزین شود*/
    if (!QFile::exists(Settings::ProgramPath() + "start.sh")) {
        QFile::copy(":/start.sh", Settings::ProgramPath() + "start.sh");
    }

    /*open and read start.sh file*/
    QFile oldScriptFile(Settings::ProgramPath() + "start.sh");
    oldScriptFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QByteArray startScriptFile_Str = oldScriptFile.readAll();
    oldScriptFile.close();

    /*edit file text*/
    int startIndex = startScriptFile_Str.indexOf("./console");
    int endIndex = startScriptFile_Str.indexOf(" -qws", startIndex);
    if(startIndex<0 || endIndex<0){
        /*در صورت خرابی فایل یک فایل نمونه از ریسورس جایگزین شود*/
        oldScriptFile.remove();
        QFile::copy(":/start.sh", Settings::ProgramPath() + "start.sh");

        /*open and read start.sh file*/
        oldScriptFile.open(QIODevice::ReadOnly | QIODevice::Text);
        startScriptFile_Str = oldScriptFile.readAll();
        oldScriptFile.close();

        /*edit file text*/
        startIndex = startScriptFile_Str.indexOf("./console");
        endIndex = startScriptFile_Str.indexOf(" -qws", startIndex);
    }
    startScriptFile_Str.replace(startIndex,  endIndex-startIndex, "./"+remoteFileName.toAscii());

    /*اگر فایل همنام در مسیر بود پاک شود*/
    if (QFile::exists(Settings::ProgramPath() + "temp_start")) {
        QFile::setPermissions(Settings::ProgramPath() + "temp_start", QFile::ReadOwner | QFile::WriteOwner);
        QFile::remove(Settings::ProgramPath() + "temp_start");
    }

    /*create temp_start file and write new text into*/
    QFile newScriptFile(Settings::ProgramPath() + "temp_start");
    newScriptFile.open(QIODevice::WriteOnly | QIODevice::Text);
    newScriptFile.write(startScriptFile_Str);
    newScriptFile.close();

    /*set Permissions for new file*/
    newScriptFile.setPermissions(QFile::ExeOwner | QFile::ReadOwner | QFile::WriteOwner);

    /*remove old script and rename new file to 'start.h'*/
    oldScriptFile.remove();
    newScriptFile.rename(Settings::ProgramPath() + "start.sh");
}
