#ifndef GPIO_H
#define GPIO_H

#include <QString>
#include <QObject>

/**************************************************************
                Header pin number definitions
pin is the pin nuber on con1 of the FriendlyARM 6410 core board
***************************************************************/

/*
 *for 6410 stamp module
#define GPIO_PIN_3   33
#define GPIO_PIN_4   34
#define GPIO_PIN_5   35
#define GPIO_PIN_6   36
#define GPIO_PIN_7   137
#define GPIO_PIN_8   138
#define GPIO_PIN_9   139
#define GPIO_PIN_10  140
#define GPIO_PIN_11  141
#define GPIO_PIN_12  142
#define GPIO_PIN_13  195
#define GPIO_PIN_14  196
#define GPIO_PIN_15  197
#define GPIO_PIN_16  198
#define GPIO_PIN_17  199
#define GPIO_PIN_18  200*/

/*
 *for 210v2 stamp  module
 P3 add
#define GPIO_PIN_3  128
#define GPIO_PIN_4  129
#define GPIO_PIN_5  130
#define GPIO_PIN_6  131
#define GPIO_PIN_7  132
#define GPIO_PIN_8  133
#define GPIO_PIN_9  134
#define GPIO_PIN_10  136*/

/*
 *for 210v2 stamp  module
 P4  */
#define GPIO_PIN_1  172
#define GPIO_PIN_3  173
#define GPIO_PIN_5  174
#define GPIO_PIN_7  175
#define GPIO_PIN_9  176
#define GPIO_PIN_11  177
#define GPIO_PIN_13  178
#define GPIO_PIN_15  179

#define GPIO_PIN_2  150
#define GPIO_PIN_4  151
#define GPIO_PIN_6  152
#define GPIO_PIN_8  153
#define GPIO_PIN_10  159
#define GPIO_PIN_12  160
#define GPIO_PIN_14  161
#define GPIO_PIN_16  162


//Header path number definitions
#define GPIO_DEVICE_PATH      "/sys/class/gpio"
#define GPIO_EXPORT_DIR       "/export"
#define GPIO_UNEXPORT_DIR     "/unexport"
#define GPIO_DIRECTION_DIR    "/direction"
#define GPIO_VALUE_DIR        "/value"

class GPIO : public QObject
{
    Q_OBJECT
public:
    GPIO(int pin, QObject *parent=0);
    ~GPIO();
    enum Direction {In,Out,Err};

    bool openPin();
    bool closePin();
    bool setDirection(Direction direction); //set direction in/out.
    Direction getDirection(); //returns direction

public slots:
    bool setValue(bool state);
    static bool setValue(bool state, quint16 pinNumber);
    bool value();
    static bool value(quint16 pinNumber);
    bool setHigh();
    bool setLow();

private:
    Direction  _direction;
    int _state;
    int _pinNumber;
    QString _directionString;
    QString _valueString;
    QString _pinNumber_str;

};

#endif // GPIO_H
