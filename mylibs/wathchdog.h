#ifndef WATHCHDOG_H
#define WATHCHDOG_H

#define WatchDog_PATH "/dev/watchdog"

#include <QObject>
#include <QTimer>
#include "Defines.h"

class WathchDog : public QObject
{
    Q_OBJECT
public:
    explicit WathchDog(QObject *parent = 0);    
    
private:
    QTimer *kickTimer;

signals:
    
public slots:
    static bool Kick();
    void startWhatching();
    void stopWhatching();
};

#endif // WATHCHDOG_H
