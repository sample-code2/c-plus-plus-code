#include "delay.h"
#include <QEventLoop>
#include <QTimer>

Delay::Delay(QObject *parent) :QObject(parent){

}

void Delay::delay_ms(quint32 ms){
     QEventLoop loop;
     QTimer::singleShot(ms, &loop, SLOT(quit()));
     loop.exec();
}
