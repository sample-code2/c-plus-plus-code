#ifndef EVENTLOG_H
#define EVENTLOG_H

#include <QObject>

class EventLog : public QObject
{
    Q_OBJECT
public:
    typedef enum {
        ShutDown_EventType=1,
        DriverLogin_EventType=2,
        DriverLogout_EventType=3
    }EventType;

    explicit EventLog(QObject *parent = 0);
    static bool Log_shutDown();
    static bool Log_DriverLogin();
    static bool Log_DriverLogout();
    
signals:
    
public slots:
    
};

#endif // EVENTLOG_H
