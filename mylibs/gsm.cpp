#include "gsm.h"
#include <QProcess>
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QTimer>
#include <QSqlQuery>
#include <QSqlError>
#include <QDateTime>
#include <QSqlRecord>
#include "settings.h"
#include "Defines.h"
#include "Defines.h"
#include "mydatetime.h"
#include "delay.h"
#include "mainview.h"

GSM::GSM(QString portName, QObject *parent): QObject(parent){
    CMTI = QString("CMTI: \"SM\",");
    CMTI_Pos = 0;
    SendOk = QString("SEND OK");
    SendOk_Pos = 0;
    _sendOkRecived = false;
    SendAck = QString("successfully Saved");
    SendAck_Pos = 0;
    CONNECT = QString("CONNECT OK");
    CONNECT_Pos = 0;
    _connectOk = false;
    ERROR = QString("ERROR");
    ERROR_Pos = 0;
    _errorRecived = false;
    Rial = QString("Rial");
    Rial_Pos = 0;
    _Rials_Available = false;
    Ring = QString("RING");
    Ring_Pos = 0;
    CPIN = QString("+CPIN: NOT READY");
    CPIN_Pos = 0;
    UPDATE = QString("update Available:");
    UPDATE_Pos = 0;
    //Update_Available=false;

    Card_FullSend_Counter=0;
    Card_PartialSend_Counter=0;
    Avl_Partialsend_counter=0;
    Avl_FullSend_Counter=0;
    IsConnectToAPN_Counter = 0;
    Get_GsmParameters_Counter=60000;
    PowerCheck_Counter=0;
    ringTime_Counter=0;

    mutexOfGsmState = IDLE_STATE;
    _ConnectionMode  = NONE_MODE;

    _powerPin = new GPIO(GPIO_PIN_3, this);
    _powerPin->openPin();
    _powerPin->setDirection(GPIO::Out);

    _statusPin = new GPIO(GPIO_PIN_1, this);
    _statusPin->openPin();
    _statusPin->setDirection(GPIO::In);

    _ringDetectPin = new GPIO(GPIO_PIN_12, this);
    _ringDetectPin->openPin();
    _ringDetectPin->setDirection(GPIO::In);

    PortSettings settings = {BAUD115200, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};
    _GsmPort = new QextSerialPort(portName, settings, QextSerialPort::EventDriven, this);
    connect(_GsmPort, SIGNAL(readyRead()), SLOT(ReadUart()));

    ActionTimer = new QTimer(this);
    ActionTimer->setInterval(1000);
    connect(ActionTimer, SIGNAL(timeout()), SLOT(ActionProcess()));

    if(!isOn()){
        qLog << "SIM908 is off";
        powerOn();
        Delay::delay_ms(10*1000);
    }

    setConnectionMode(AT_MODE);

    getIMEI();
    configGSM();//must be first
    configGPS();

    ActionTimer->start();
}

bool GSM::OpenUart(){
    if(_GsmPort->open(QIODevice::ReadWrite)){
        qLog << ("Opening GSM serial port "+_GsmPort->portName()+"\tOK.");
        return true;
    }

    qLog << ("Opening GSM serial port "+_GsmPort->portName()+"\tFAIL.");
    return false;
}

void GSM::CloseUart(){
    _GsmPort->close();
}

qint64 GSM::WriteUart(QByteArray array){
    return _GsmPort->write(array);
}

QByteArray GSM::ReadUart(){
    QByteArray RecivedArray = _GsmPort->readAll();
    qLog<<RecivedArray<<__LINE__;
    ReciveBuffer.append(RecivedArray);

    for(int i=0; i<RecivedArray.length(); i++){
        //***************************************************************
        if (Str_Interupt(CMTI, &CMTI_Pos, RecivedArray.at(i), true)) {
            //Msg_Index_Counter = 0;
            //Msg_Index_Available = true;
        }
        //***************************************************************مشاهده
        if (Str_Interupt(SendAck, &SendAck_Pos, RecivedArray.at(i), true)) {
            //SendAck_Recived = true;
        }
        //***************************************************************
        if (Str_Interupt(SendOk, &SendOk_Pos, RecivedArray.at(i), true)) {
            _sendOkRecived = true;
            emit SendOk_Recived();
        }
        //*********************************************************
        if (Str_Interupt(CONNECT, &CONNECT_Pos, RecivedArray.at(i), true)) {
            _connectOk = true;
            emit connectOk();
        }
        //*********************************************************
        if (Str_Interupt(ERROR, &ERROR_Pos, RecivedArray.at(i), true)) {
            _errorRecived = true;
            emit error_Recived();
        }
        //********************************************************
        if (Str_Interupt(Rial, &Rial_Pos, RecivedArray.at(i), false)) {
            _Rials_Available = true;
            emit rial_Recived();
        }
        //********************************************************
        if (Str_Interupt(Ring, &Ring_Pos, RecivedArray.at(i), true)) {
            //emit ringing();
        }
        //********************************************************
        if (Str_Interupt(CPIN, &CPIN_Pos, RecivedArray.at(i), true)) {
            //SimRemoved = true;
        }
        //********************************************************
        if (Str_Interupt(UPDATE, &UPDATE_Pos, RecivedArray.at(i), true)) {
            //Update_Available = true;
        }
    }

    if (ReciveBuffer.length() >= 256) {
        ReciveBuffer.clear();
    }
    return RecivedArray;
}

void GSM::ActionProcess(){
    Get_GsmParameters_Counter++;
    Card_FullSend_Counter++;
    Card_PartialSend_Counter++;
    Avl_Partialsend_counter++;
    PowerCheck_Counter++;
    Avl_FullSend_Counter++;
    IsConnectToAPN_Counter++;

    if (mutexOfGsmState == IDLE_STATE && IsConnectToAPN_Counter >= 20) {//@temp
        IsConnectToAPN_Counter=0;
        setConnectionMode(MODEM_MODE);
    }else if (PowerCheck_Counter >= 60) {//@temp
        PowerCheck_Counter=0;
        powerOn();
    }else if (Get_GsmParameters_Counter >= 2400) {
        Get_GsmParameters_Counter=0;
        _GsmInfo.Gsm_Ant = GetGsmAntValue();
        _GsmInfo.Bat_Charge_Value = GetBattryValue();
        _GsmInfo.Sim_Charge_Value = getSimCharge();
    }else{
    }

#ifdef MY210
    if(!_ringDetectPin->value() && isOn()){
        ringTime_Counter++;
        if(ringTime_Counter>3){
            ringTime_Counter=0;
            emit ringing();
            qLog << "RRRRRRRRRRRRIIIIIIIIIIIIIINNNNNNNNNNNNNNNNGGGGGGGGGGGGGGGGGGGGGG";
        }
    }
#endif
}

GSM::CONNECTION_MODE GSM::connectionMode(){
    return _ConnectionMode;
}

bool  GSM::setConnectionMode(GSM::CONNECTION_MODE mode){
    if(!isOn()) return false;

    //todo
    if(connectionMode() == mode){
        if (mode == AT_MODE && !isConnectInterface()) return true;
        if (mode == MODEM_MODE && isConnectInterface())  return true;
    }

    bool result = false;
    if(mode == AT_MODE){
        emit closeMySql_request();
        Delay::delay_ms(100);//wait for colse
        result = disconnectFromAPN();
        OpenUart();
        AT_Command(EchoMode_On_Command);
        qLog;
        /*آیا پورت در اختار ماست؟*/
        if(!isEchoMode()){
            ActionTimer->stop();
            powerReset();
            Delay::delay_ms(10000);
            OpenUart();
            AT_Command(EchoMode_On_Command);
            configGSM();//must be first
            configGPS();
            ActionTimer->start();
        }
        if(result){
            emit GsmState_Changed(false);
        }
    } else if(mode == MODEM_MODE){
        CloseUart();
        Delay::delay_ms(2000);
        result = connectToAPN();
        if(result){
            emit GsmState_Changed(true);
            QHostAddress ppp_ip = pppIp();
            if(ppp_ip != QHostAddress("0.0.0.0") && ppp_ip.toString() != Settings::pppIp()){
                qLog << "new ip availible **************************************";
                Settings::setpppIp(ppp_ip.toString());
                emit New_pppIp_Available();
            }
        }
    }

    if (result){
        _ConnectionMode = mode;
    }

    return result;
}

QHostAddress GSM::pppIp(){
    QList<QNetworkInterface> interfaceAddressList = QNetworkInterface::allInterfaces();
    for (int i=0;i<interfaceAddressList.size();++i){
        if(interfaceAddressList.at(i).name().indexOf("ppp") >= 0){
            QList<QNetworkAddressEntry> intAddLst = interfaceAddressList.at(i).addressEntries();
            if(intAddLst.size() > 0){
                qLog << intAddLst.at(0).ip();
                return intAddLst.at(0).ip();
            }
        }
    }
    return QHostAddress("0.0.0.0");
}

bool GSM::Str_Interupt(QString str, quint8 *pos, QChar input, bool CaseSensitive) {
    if (input == str[(*pos)] || (!CaseSensitive && input.toUpper() == str[(*pos)].toUpper())) (*pos)++;
    else if (input == str[0] || (!CaseSensitive && input.toUpper() == str[0].toUpper())) (*pos) = 1;
    else (*pos) = 0;

    if ((*pos) == str.length()) {
        (*pos) = 0;
        return true;
    }
    return false;
}

void GSM::AT_Command(QString Command, quint16 delay_ms) {
    QEventLoop loop;

    WriteUart(Command.toAscii());
    WriteUart("\r\n");

    QTimer::singleShot(delay_ms, &loop, SLOT(quit()));
    loop.exec();
}

void GSM::setAPN(QString apn){
    QString connectScript_Path="/etc/ppp/gprs_connect";

    /*open and read sample gprs_connect file from resource file*/
    QFile sampleScriptFile(":/gprs_connect");
    sampleScriptFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QByteArray sampleScriptFile_Str = sampleScriptFile.readAll();
    sampleScriptFile.close();

    /*edit file text
    "AT+CGDCONT=1,\"IP\",\"mtnirancell\"" */
    int startIndex = sampleScriptFile_Str.indexOf("AT+CGDCONT")+22;
    int endIndex = sampleScriptFile_Str.indexOf("\\\"\"", startIndex);
    sampleScriptFile_Str.replace(startIndex,  endIndex-startIndex, apn.toAscii());

    /*create new file*/
    QFile newScriptFile(connectScript_Path);
    newScriptFile.open(QIODevice::WriteOnly | QIODevice::Text);
    newScriptFile.write(sampleScriptFile_Str);
    newScriptFile.close();
}

bool GSM::connectToAPN(){
    qLog << __LINE__;


    QProcess process;
    bool result;
    result = isConnectInterface();
    if (result)
        return true;

    for (quint8 i=0;i<10 && mutexOfGsmState == DISCONNECTING_STATE;++i){
        Delay::delay_ms(1000);
    }

    GSM_STATE pastMutexOfGsmState = mutexOfGsmState;
    mutexOfGsmState = CONNECTING_STATE;

    process.startDetached("gprs-ppp");

    qDebug("------------------------------------salam ");
    for (quint8 i=0; i<60 && ! (result = isConnectInterface()) ; ++i){
        //qLog << __LINE__;
        Delay::delay_ms(1000);
    }

    qDebug("------------------------------------khodahafez");

    mutexOfGsmState = pastMutexOfGsmState;
    return result;
}

bool GSM::connectToAPN(QString apn){
    setAPN(apn);
    return connectToAPN();
}

bool GSM::disconnectFromAPN(){
    QProcess process;
    bool result;

    for (quint8 i=0;i<60 && mutexOfGsmState == CONNECTING_STATE;++i){
        Delay::delay_ms(1000);
    }
    GSM_STATE pastMutexOfGsmState = mutexOfGsmState;
    mutexOfGsmState = DISCONNECTING_STATE;
    process.startDetached("killall -15 pppd");
    qLog << "*********start disconnecting*********************" << __LINE__;
    for (quint8 i=0; i<60 && (result = isConnectInterface()) ; ++i){
        qLog << __LINE__;
        Delay::delay_ms(1000);
    }
    Delay::delay_ms(1000*10);//@temp
    qLog << "*********disconneced*********************" << __LINE__;

    mutexOfGsmState = pastMutexOfGsmState;
    return !result;
}

bool GSM::isPortInUse(){
    return mutexOfGsmState;
}

bool GSM::isOn(){
    return _statusPin->value();
}

bool GSM::powerOn(){
    if(isOn()) return true;

    _powerPin->setHigh();
    QTimer::singleShot(1000, _powerPin, SLOT(setLow()));
    return true;
}

bool GSM::powerOff(){
    if(!isOn()) return true;

    _powerPin->setHigh();
    QTimer::singleShot(1000, _powerPin, SLOT(setLow()));
    return true;
}

bool GSM::powerReset(){
    powerOff();
    Delay::delay_ms(3000);
    powerOn();
    return true;
}

bool GSM::isConnect(int timeOut_ms,QUrl url){
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QEventLoop loop1;
    qLog << __LINE__;


    QTimer::singleShot(timeOut_ms, &loop1, SLOT(quit()));
    QNetworkReply *reply = manager->get(QNetworkRequest(url));
    connect(reply, SIGNAL(finished()), &loop1, SLOT(quit()));
    loop1.exec();
    qLog << __LINE__;


    if (reply->error() == QNetworkReply::NoError)
        return true;
    qLog << __LINE__;


    return false;
}

bool GSM::isConnectInterface(){
    return QDir().exists("/var/run/ppp0-ok");
}

bool GSM::configGSM(){
    AT_Command(EchoMode_On_Command, 100);
    AT_Command(Set_Rate_Command);
    AT_Command(Sms_Text_Mode_Command);
    AT_Command(Sim_Detect_Enable_Command);
    AT_Command(CID_Enable_Command);
    AT_Command(Store_Gsm_Parameters_Command);

    return true;
}

bool GSM::configGPS(){
    AT_Command(Gps_Power_On_Command);
    AT_Command(Gps_Reset_Mode_Command);
    AT_Command(Gps_Out_Mode_Command);
    AT_Command(Gps_SetRare_Command, 200);

    return true;
}

quint64 GSM::IMEI(){
    quint64 imei = Settings::IMEI();

    if(imei == 0){
        Settings::setIMEI(getIMEI());
    }
    return Settings::IMEI();
}

quint64 GSM::getIMEI(){
    quint64 imei=0;

    ReciveBuffer.clear();
    AT_Command(GET_IMEI_Command);

    qint16 pointer = ReciveBuffer.indexOf("ATD*#06#");
    if(pointer >= 0){
        pointer+=12;
        imei = ReciveBuffer.mid(pointer, ReciveBuffer.indexOf("\r\n", pointer)-pointer).toULongLong();
    }
    Settings::setIMEI(imei);
    return imei;
}

quint32 GSM::getSimCharge(){
    QEventLoop loop;
    quint8 len,end, i, j;
    quint32 Sim_Charge_Value=0;

    mutexOfGsmState = AT_STATE;

    CONNECTION_MODE mode = connectionMode();
    if(mode == MODEM_MODE){
        setConnectionMode(AT_MODE);
    }
    if (connectionMode() != AT_MODE){
        mutexOfGsmState = IDLE_STATE;
        return 0;
    }

    ReciveBuffer.clear();
    _Rials_Available = false;
    _errorRecived = false;

    AT_Command(Get_SimCharge_Command);

    QTimer::singleShot(25000, &loop, SLOT(quit()));
    connect(this, SIGNAL(rial_Recived()), &loop, SLOT(quit()));
    connect(this, SIGNAL(error_Recived()), &loop, SLOT(quit()));
    loop.exec();

    mutexOfGsmState = IDLE_STATE;

    if (!_Rials_Available) return false;
    int rialPos = QString(ReciveBuffer).indexOf("rial", 0, Qt::CaseInsensitive);
    for(i=1, j=0; !QChar(ReciveBuffer.at(rialPos-i)).isNumber() && j<10; i++, j++);
    end=rialPos-i;
    for(i=1, j=0; QChar(ReciveBuffer.at(end-i)).isNumber() && j<10; i++, j++);
    len=i;
    Sim_Charge_Value = ReciveBuffer.mid(end-len+1, len).toUInt();
    Settings::setSimCharge(Sim_Charge_Value);
    emit SimCharge_Changed();
    return Sim_Charge_Value;
}

quint16 GSM::GetBattryValue() {

    mutexOfGsmState = AT_STATE;

    CONNECTION_MODE mode = connectionMode();
    if(mode == MODEM_MODE){
        setConnectionMode(AT_MODE);
    }
    if (connectionMode() != AT_MODE)
    {
        mutexOfGsmState = IDLE_STATE;
        return 0;
    }
    quint16 value=0;

    ReciveBuffer.clear();
    _errorRecived = false;
    AT_Command(Get_Bat_Volt_Command, 30);

    mutexOfGsmState = IDLE_STATE;

    if(_errorRecived) return false;

    qint16 pointer = ReciveBuffer.indexOf("+CBC:");
    pointer = ReciveBuffer.indexOf(',', pointer+5);
    pointer = ReciveBuffer.indexOf(',', pointer+1)+1;

    value = ReciveBuffer.mid(pointer).left(ReciveBuffer.mid(pointer).indexOf("\r\n")).toUInt();
    emit BattryValue_Changed(value);

    return value;
}

quint8 GSM::GetGsmAntValue() {
    mutexOfGsmState = AT_STATE;

    CONNECTION_MODE mode = connectionMode();
    if(mode == MODEM_MODE){
        setConnectionMode(AT_MODE);
    }
    if (connectionMode() != AT_MODE){
        mutexOfGsmState = IDLE_STATE;
        return 0;
    }

    ReciveBuffer.clear();
    _errorRecived = false;
    AT_Command(Get_Gsm_Ant_Command, 30);

    if(_errorRecived){
        mutexOfGsmState = IDLE_STATE;
        return false;
    }

    qint16 pointer = ReciveBuffer.indexOf("+CSQ: ");
    pointer += 6;
    qint16 value = ReciveBuffer.mid(pointer, ReciveBuffer.indexOf(',', pointer)-pointer).toUInt();
    value = (quint8)(value*3.22);

    mutexOfGsmState = IDLE_STATE;

    if(value<= 100){
        emit GsmAntena_Changed(value);
        return value;
    }

    return false;
}

bool GSM::isEchoMode(){
    ReciveBuffer.clear();
    AT_Command("AT");
    qint8 pointer = ReciveBuffer.indexOf("OK");
    if(pointer >= 0){
        return true;
    }

    return false;
}

GSM::GSM_INFO GSM::gsmInfo(){
    return _GsmInfo;
}

void GSM::reconfig(){
    ActionTimer->stop();
    setConnectionMode(GSM::AT_MODE);

    configGSM();//must be first
    configGPS();
    ActionTimer->start();
}

void GSM::answerIncomingCall(){
    setConnectionMode(AT_MODE);
    AT_Command(Answer_Incoming_Call_Command);
}
