#ifndef MYDATETIME_H
#define MYDATETIME_H

#include <QObject>
#include <QDateTime>

class MyDateTime : public QObject
{
    Q_OBJECT
public:
    explicit MyDateTime(QObject *parent = 0);
    static QDateTime extractDateTime(quint32 dateTime);
    static quint32 createDateTime(QDateTime dateTime);
    static bool isValid(quint32 dateTime);
    
signals:
    
public slots:
    
};

#endif // MYDATETIME_H
