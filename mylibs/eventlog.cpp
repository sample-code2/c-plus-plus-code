#include "eventlog.h"
#include "QSqlDatabase"
#include "QSqlQuery"
#include "QSqlError"
#include "QDebug"
#include "settings.h"
#include "Defines.h"
#include "mydatetime.h"

EventLog::EventLog(QObject *parent):QObject(parent){

}

bool EventLog::Log_shutDown(){
    quint32 tmp32=0;
    QByteArray data;

    tmp32 = Settings::BusSerial();
    data.append((const char *)&tmp32, 4);

    tmp32 = Settings::driverId();
    data.append((const char *)&tmp32, 4);

    QString cmd=QString("INSERT INTO EventLog (EventType, EventDateTime , Data, IsSent) VALUES(%0, %1, X'%2', 0);")
            .arg(ShutDown_EventType)
            .arg(MyDateTime::createDateTime(QDateTime::currentDateTime()))
            .arg(QString(data.toHex()));
    QSqlQuery insertQuery(QSqlDatabase::database("Local_Bus_Sqlite"));
    insertQuery.exec(cmd);
    qLog << insertQuery.executedQuery();
    if(insertQuery.lastError().isValid()) return false;
    return true;
}

bool EventLog::Log_DriverLogin(){
    quint32 tmp32=0;
    QByteArray data;

    tmp32 = Settings::BusSerial();
    data.append((const char *)&tmp32, 4);

    tmp32 = Settings::driverId();
    data.append((const char *)&tmp32, 4);

    QString cmd=QString("INSERT INTO EventLog (EventType, EventDateTime , Data, IsSent) VALUES(%0, %1, X'%2', 0);")
            .arg(DriverLogin_EventType)
            .arg(MyDateTime::createDateTime(QDateTime::currentDateTime()))
            .arg(QString(data.toHex()));
    QSqlQuery insertQuery(QSqlDatabase::database("Local_Bus_Sqlite"));
    insertQuery.exec(cmd);
    qLog << insertQuery.executedQuery();
    if(insertQuery.lastError().isValid()) return false;
    return true;
}

bool EventLog::Log_DriverLogout(){
    quint32 tmp32=0;
    QByteArray data;

    tmp32 = Settings::BusSerial();
    data.append((const char *)&tmp32, 4);

    tmp32 = Settings::driverId();
    data.append((const char *)&tmp32, 4);

    QString cmd=QString("INSERT INTO EventLog (EventType, EventDateTime , Data, IsSent) VALUES(%0, %1, X'%2', 0);")
            .arg(DriverLogout_EventType)
            .arg(MyDateTime::createDateTime(QDateTime::currentDateTime()))
            .arg(QString(data.toHex()));
    QSqlQuery insertQuery(QSqlDatabase::database("Local_Bus_Sqlite"));
    insertQuery.exec(cmd);
    qLog << insertQuery.executedQuery();
    if(insertQuery.lastError().isValid()) return false;
    return true;
}
