#include "wathchdog.h"
#include "QFile"
#include "QDebug"

WathchDog::WathchDog(QObject *parent):QObject(parent){
    kickTimer = new QTimer(this);
    kickTimer->setInterval(1000);
    connect(kickTimer, SIGNAL(timeout()), SLOT(Kick()));
}

bool WathchDog::Kick(){
    QFile file(WatchDog_PATH);
    if (!file.open(QIODevice::WriteOnly)) return false;
    file.write("1", 1);
    file.close();

    //qLog << "WatchDog kicked.";
    return true;
}

void WathchDog::startWhatching(){
    kickTimer->start();
}

void WathchDog::stopWhatching(){
    kickTimer->stop();
}
