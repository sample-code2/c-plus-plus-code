#include "powercontroller.h"
#include "Defines.h"
#include "messagebox.h"
#include "mainview.h"
#include "eventlog.h"
#include "delay.h"

PowerController::PowerController(QObject *parent) : QObject(parent){
    powerController_Pin = new GPIO(GPIO_PIN_10, this);
    powerController_Pin->openPin();
    powerController_Pin->setDirection(GPIO::Out);
    powerController_Pin->setHigh();
}

void PowerController::ShutDown(){
    MessageBox msgBox(MainView::instance());
    msgBox.setText("سیستم خاموش می شود");
    msgBox.setInformativeText("آیا مطمئنید؟");
    QPushButton *yesButton = msgBox.addButton("بله", QMessageBox::YesRole);
    msgBox.addButton("انصراف", QMessageBox::RejectRole);
    msgBox.setIconPixmap(QPixmap(":/Graphics/warning.png"));
    msgBox.exec();
    if(msgBox.clickedButton() != yesButton) return;

    /************************Go to shut down***************************/
    EventLog::Log_shutDown();

    Delay::delay_ms(1000);
    powerController_Pin->setLow();
}
