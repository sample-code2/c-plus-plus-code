#ifndef POWERCONTROLLER_H
#define POWERCONTROLLER_H

#include <QObject>
#include <gpio.h>

class PowerController : public QObject
{
    Q_OBJECT
public:
    explicit PowerController(QObject *parent = 0);    

private:
    GPIO *powerController_Pin;
    
signals:
    
public slots:
    void ShutDown();
    
};

#endif // POWERCONTROLLER_H
