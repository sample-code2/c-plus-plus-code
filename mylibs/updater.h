#ifndef UPDATER_H
#define UPDATER_H

#include <QObject>
#include <QFtp>
#include <QFile>
#include <QProgressBar>

class Updater : public QObject
{
    Q_OBJECT
public:
    explicit Updater(QObject *parent = 0);
    ~Updater();
    
private:
    void updateScriptFile();

    QFtp *ftp;
    QFile *file;
    QProgressBar *ProgressBar;
    QString remoteFileName;

signals:
    
public slots:
    void startUpdate(QString fileName);

private slots:
    void ftpCommandFinished(int commandId, bool error);
    void updateDataTransferProgress(qint64 readBytes, qint64 totalBytes);
    void getListInfo(const QUrlInfo &urlInfo);
    void downloadFile();
    
};

#endif // UPDATER_H
