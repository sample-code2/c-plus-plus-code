#include "readers.h"
#include <QtSql>
#include <mylibs/settings.h>
#include <mylibs/jalalidate.h>
#include "mydatetime.h"
#include "delay.h"

Readers::Readers(QString portName, QObject *parent):QObject(parent){
    CommandRecivedFlag=false;
    Direction=false;
    dataIsvalid=false;
    ReaderId=0;
    readerIdAvailable=0;
    readerFormatAvailable=0;
    Driver_ID=0;
    Manager_ID=0;
    Manager_Password=0;
    Manager_Type = NONE_CARD;
    Manager_DATA.clear();
    State = READY_STATE;
    readerCount=Settings::ReaderCount();

    _DirectionPin = new GPIO(GPIO_PIN_5, this);
    _DirectionPin->openPin();
    _DirectionPin->setDirection(GPIO::Out);
    _DirectionPin->setLow();

    _Reader1PowerControlerPin = new GPIO(GPIO_PIN_2, this);
    _Reader1PowerControlerPin->openPin();
    _Reader1PowerControlerPin->setDirection(GPIO::Out);
    _Reader1PowerControlerPin->setLow();

    _Reader2PowerControlerPin = new GPIO(GPIO_PIN_4, this);
    _Reader2PowerControlerPin->openPin();
    _Reader2PowerControlerPin->setDirection(GPIO::Out);
    _Reader2PowerControlerPin->setLow();

    _Reader3PowerControlerPin = new GPIO(GPIO_PIN_6, this);
    _Reader3PowerControlerPin->openPin();
    _Reader3PowerControlerPin->setDirection(GPIO::Out);
    _Reader3PowerControlerPin->setLow();

    _DirectionPin = new GPIO(GPIO_PIN_5, this);
    _DirectionPin->openPin();
    _DirectionPin->setDirection(GPIO::Out);
    _DirectionPin->setLow();

    _DirectionPin = new GPIO(GPIO_PIN_5, this);
    _DirectionPin->openPin();
    _DirectionPin->setDirection(GPIO::Out);
    _DirectionPin->setLow();


    if(OpenUart(portName)) qLog << ("Opening Readers serial port "+portName+"\tOK.");
    else qLog << ("Opening serial port "+portName+"\tFAIL.");

    recuestTimer = new QTimer(this);
    recuestTimer->setInterval(500);//@temp
    connect(recuestTimer, SIGNAL(timeout()), SLOT(sendRequest()));
    recuestTimer->start();

    commandTimoutTimer = new QTimer(this);
    commandTimoutTimer->setInterval(400);//@temp
    commandTimoutTimer->setSingleShot(true);
    connect(commandTimoutTimer, SIGNAL(timeout()), SLOT(commandTimeOut()));

    directionTimer = new QTimer(this);
    directionTimer->setInterval(5);
    directionTimer->setSingleShot(true);
    connect(directionTimer, SIGNAL(timeout()), SLOT(setDirectionToRecive()));
}

bool Readers::sendCommand(quint8 address, quint8 command){
    QByteArray data;
    quint8 tmp8=0;
    DC_COMMAND cmd;

    /*set direction to send mode*/
    setDirection(true);

    cmd.Parameters.Address = address;
    cmd.Parameters.Command = command;

    tmp8 = 255;
    data.append((const char *)&tmp8, 1);
    data.append((const char *)&cmd.Byte, 1);
    WriteUart(data);
    directionTimer->start();

    return true;
}

void Readers::writeData(QByteArray array){
    QByteArray writeArray;

    for(quint16 i=0; i<array.length(); i++){
        writeArray.append(array.at(i));
        if((quint8)array.at(i) == 255)
            writeArray.append(array.at(i));
    }
    /*set direction to send mode*/
    setDirection(true);
    WriteUart(writeArray);
    directionTimer->start();
}

qint64 Readers::WriteUart(QByteArray array){
    return ReaderPort->write(array);
}

bool Readers::OpenUart(QString portName){
    PortSettings setting = {BAUD115200, DATA_8, PAR_NONE, STOP_1, FLOW_OFF, 10};
    ReaderPort = new QextSerialPort(portName, setting, QextSerialPort::EventDriven, this);
    ReaderPort->open(QIODevice::ReadWrite);
    connect(ReaderPort, SIGNAL(readyRead()), SLOT(ReadUart()));
    if(ReaderPort->isOpen()) return true;
    else return false;
}

void Readers::ReadUart(){
    QByteArray RecivedArray = ReaderPort->readAll();
    //emit readyRead(RecivedArray);
    //qLog << RecivedArray.toHex();

    for(int i=0; i<RecivedArray.length(); i++)
    {
        if((quint8)RecivedArray.at(i) == 255){
            if(CommandRecivedFlag){
                /*append*/
                ReciveBuffer.append(RecivedArray.at(i));
                CommandRecivedFlag = false;
            }else{
                CommandRecivedFlag = true;
            }
        }else{
            if(CommandRecivedFlag){
                /*command recived*/
                CommandRecivedFlag =false;
                DC_COMMAND command;
                command.Byte = RecivedArray.at(i);
                commandRecived(command);
            }else{
                /*append*/
                ReciveBuffer.append(RecivedArray.at(i));
            }
        }
    }
    if (ReciveBuffer.length() > 1000) {
        ReciveBuffer.clear();
    }
}

Readers::CARD_INFO Readers::extractCardInfo(QByteArray data){
    CARD_INFO info;
    quint8 index=0;
    quint8 tmp8=0;

    info.Pasenger_ID=0;
    info.Driver_ID=0;
    info.Bus_ID=0;
    info.Line_Number=0;
    info.RemainingPrice=0;
    info.Price=0;
    info.CardType=0;
    info.Reader_ID=0;
    info.CheckSum=0;

    memcpy(&info.Pasenger_ID, data.data(), 7);
    index += 7;
    info.Pasenger_ID = info.Pasenger_ID>>8;

    memcpy(&info.Driver_ID, data.data()+index, 7);
    index += 7;
    info.Driver_ID = info.Driver_ID>>8;

    memcpy(&info.Datetime, data.data()+index, 4);
    index += 4;

    memcpy(&info.Bus_ID, data.data()+index, 3);
    index += 3;
    info.Bus_ID = info.Bus_ID>>8;

    memcpy(&info.Line_Number, data.data()+index, 3);
    index += 3;
    info.Line_Number = info.Line_Number>>8;

    memcpy(&info.RemainingPrice, data.data()+index, 3);
    index += 3;
    info.RemainingPrice = info.RemainingPrice>>8;

    memcpy(&info.Price, data.data()+index, 2);
    index += 2;

    memcpy(&tmp8, data.data()+index, 1);
    info.CardType  = (tmp8 & 0b00001111);
    info.Reader_ID = (tmp8 & 0b11110000)>>4;
    index += 1;

    /*reserve byte*/
    index += 1;

    memcpy(&info.CheckSum, data.data()+index, 1);

    return info;
}

void Readers::setDirection(bool direction){
    Direction = direction;
    _DirectionPin->setValue(direction);
}

void Readers::setDirectionToRecive(){
    _DirectionPin->setLow();
    Direction = false;
}

void Readers::commandTimeOut(){
    emit endDownloading();
    State = READY_STATE;
}

bool Readers::commandRecived(Readers::DC_COMMAND cmd){
    //qLog << "a command recived" << cmd.Parameters.Address;
    if((quint8)cmd.Parameters.Address != activeReader || State==NONE_STATE){
        State=NONE_STATE;
        commandTimoutTimer->start();
        return false;
    }
    //qLog << "command accept"<<cmd.Parameters.Address;
    quint8 _command = (quint8)cmd.Parameters.Command;

    if(_command == StartData_Command){
        qLog << "StartData_Command Recived state:" << State << WaitForCardData_STATE;
        if(State==WaitForCardData_STATE){
            qLog << "start data"<<cmd.Parameters.Address;
            commandTimoutTimer->start();
            ReciveBuffer.clear();
            if(activeReader == 1)emit startDownloading1();
            else emit startDownloading2();
        }else{
            State=NONE_STATE;
            commandTimoutTimer->start();
        }
    }else if(_command == EndData_Command){
        qLog << "endData_Command Recived state:" << cmd.Parameters.Address << State << WaitForCardData_STATE;
        if(State==WaitForCardData_STATE){
            CARD_INFO info = extractCardInfo(ReciveBuffer);
            quint8 checksum=CalculateChecksum(QByteArray(ReciveBuffer.data(), ReciveBuffer.length()-1));
            if(ReciveBuffer.length()==32 && info.CheckSum == checksum){
                qLog << "busId:"<<info.Bus_ID<<"busLine:"<<info.Line_Number<<"CardId:"<<info.CardType<<"RePrice:"<<info.RemainingPrice;
                Recived_CardList << info;
            }else{
                dataIsvalid=false;
                if(ReciveBuffer.length()!=32){qLog << "Length is not valid." << "Length=" << ReciveBuffer.length();}
                if(info.CheckSum != checksum) qDebug("CheckSum is not valid.");
            }
            State=WaitForCardData_STATE;
            commandTimoutTimer->start();
        }else{
            State=NONE_STATE;
            commandTimoutTimer->start();
        }
    }else if(_command == Finish_Command){
        commandTimoutTimer->start();
        //qLog << "finish avilable"<< cmd.Parameters.Address;
        if(State==WaitForCardData_STATE){
            if(Recived_CardList.length()>0){
                if(dataIsvalid && insertToDataBase(Recived_CardList)){
                    /*insert to dataBase and send ack for reader*/
                    qLog << "ack sent for reader.";
                    emit newCardData_Recived(Recived_CardList);
                    sendCommand(cmd.Parameters.Address, Ack_Command);
                }else{
                    dataIsvalid=false;
                    sendCommand(cmd.Parameters.Address, ERROR_Command);
                }
            }
        }else if(State == WaitForDriverId_STATE){
            State = READY_STATE;
            if(ReciveBuffer.length() == 8 && (quint8)ReciveBuffer[7]==CalculateChecksum(QByteArray(ReciveBuffer.data(), 7))){
                qLog << ReciveBuffer.toHex();

                memcpy(&Driver_ID, ReciveBuffer.data(), 7);
                Driver_ID = Driver_ID>>8;//@temp

                Delay::delay_ms(1);
                sendCommand(cmd.Parameters.Address, Ack_Command);
                qLog << "ack sent for reader";

                emit driverLogined(Driver_ID);
                qLog << "Driver Id=" << Driver_ID;
            }else{
                sendCommand(cmd.Parameters.Address, ERROR_Command);
                State=NONE_STATE;
                commandTimoutTimer->start();
            }
        }else if(State == WaitForManageCards_STATE){
            if(ReciveBuffer.length() == 15 && (quint8)ReciveBuffer[14]==CalculateChecksum(QByteArray(ReciveBuffer.data(), 14))){
                qLog << ReciveBuffer.toHex();
                memcpy(&Manager_ID, ReciveBuffer.data(), 7);
                Manager_ID = Manager_ID >> 8;//@temp
                Manager_Type = (MANAGERS_CARD_TYPE)ReciveBuffer.at(7);//9th byte
                memcpy(&Manager_Password, ReciveBuffer.data()+8, 2);
                Manager_DATA = ReciveBuffer.mid(10, 4);

                qLog << "managerId:"<< Manager_ID<<"managerType:"<<Manager_Type<<"ManagerPassword:"<<Manager_Password<<"Manager_DATA:"<<Manager_DATA.toHex();

                Delay::delay_ms(1);
                sendCommand(cmd.Parameters.Address, Ack_Command);
                qLog << "ack sent for reader";
                emit ManagerLogined(Manager_Type, Manager_Password);
            }else{
                qLog << "lenth or checksum error ...";
                Delay::delay_ms(1);
                sendCommand(cmd.Parameters.Address, ERROR_Command);
            }
            State = READY_STATE;

        }
        ReciveBuffer.clear();
        Recived_CardList.clear();
    }else if(_command == DriverLogin_Command){
        ReciveBuffer.clear();
        State=WaitForDriverId_STATE;
        commandTimoutTimer->start();
        qLog << "Driver logined." << cmd.Parameters.Address;
    }else if(_command == Ack_Command){
        qLog<<"Ack Recived"<<cmd.Parameters.Address;
        if(State == SendDriverIdForReader1_STATE){
            Settings::setReaderDriverIdAvalable(1, false);
        }else if(State == SendDriverIdForReader2_STATE){
            Settings::setReaderDriverIdAvalable(2, false);
        }else if(State == SendDriverIdForReader3_STATE){
            Settings::setReaderDriverIdAvalable(3, false);
        }else if(State == SendConfigForReader1_STATE){
            Settings::setReaderConfigAvalable(1, false);
            emit ConfigOk(1);
        }else if(State == SendConfigForReader2_STATE){
            Settings::setReaderConfigAvalable(2, false);
            emit ConfigOk(2);
        }else if(State == SendConfigForReader3_STATE){
            Settings::setReaderConfigAvalable(3, false);
            emit ConfigOk(3);
        }else if(State == SendFormatMemory1_STATE){
            emit FormatOk(1);
            readerFormatAvailable=0;
        }else if(State == SendFormatMemory2_STATE){
            emit FormatOk(2);
            readerFormatAvailable=0;
        }else if(State == SendFormatMemory3_STATE){
            emit FormatOk(3);
            readerFormatAvailable=0;
        }else if(State == SendReaderId_STATE){
            emit readerIdSent(readerIdAvailable);
            readerIdAvailable=0;
        }
    }else if(_command == ManageCardsLogin_Command){
        ReciveBuffer.clear();
        State=WaitForManageCards_STATE;
        commandTimoutTimer->start();

        qLog<<"Manage card Logined."<<cmd.Parameters.Address;
    }
    return true;
}

quint8 Readers::CalculateChecksum(QByteArray data){
    quint8 chechSum=0;
    for(quint16 i=0; i<data.length(); i++) chechSum ^= (quint8)data.at(i);
    return chechSum;
}

bool Readers::insertToDataBase(QList<CARD_INFO> infoList){

    quint32 tmpDatetime;
    QSqlQuery insertQuery(QSqlDatabase::database("Local_Eticket_Sqlite"));

    for(quint8 i=0; i<infoList.length(); i++){
        if(MyDateTime::extractDateTime(infoList[i].Datetime).isValid()) tmpDatetime=infoList[i].Datetime;
        else tmpDatetime = MyDateTime::createDateTime(QDateTime::currentDateTime());

        insertQuery.prepare("INSERT INTO cardinfo "
                            "(rfid,datetime,busserial,readerid,driverserial,cardtype,linenumber,price,remainprice,issent) "
                            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, 0);");
        insertQuery.addBindValue(infoList[i].Pasenger_ID);
        insertQuery.addBindValue(tmpDatetime);
        insertQuery.addBindValue(infoList[i].Bus_ID);
        insertQuery.addBindValue(infoList[i].Reader_ID);
        insertQuery.addBindValue(infoList[i].Driver_ID);
        insertQuery.addBindValue(infoList[i].CardType);
        insertQuery.addBindValue(infoList[i].Line_Number);
        insertQuery.addBindValue(infoList[i].Price);
        insertQuery.addBindValue(infoList[i].RemainingPrice);
        insertQuery.exec();

        if(insertQuery.lastError().isValid()){
            qLog<<"query error";
            qWarning() << insertQuery.lastError().text();
            return false;
        }
    }
    return true;
}

void Readers::newConfigAvailable(){
    Settings::setReaderConfigAvalable(1, true);
    Settings::setReaderConfigAvalable(2, true);
    Settings::setReaderConfigAvalable(3, true);
}

void Readers::formatReader(quint8 readerId){
    readerFormatAvailable = readerId;
}

void Readers::newReaderCountAvailable(quint8 count){
    Settings::setReaderCount(count);
    readerCount=Settings::ReaderCount();
}

void Readers::applyConfig(quint8 _activeReader){
    if(_activeReader == 1)State = SendConfigForReader1_STATE;
    else if(_activeReader == 2)State = SendConfigForReader2_STATE;
    else if(_activeReader == 3)State = SendConfigForReader3_STATE;
    READER_CONFIGS config;
    QSettings settings("tcco", "console");
    config.Datetime = MyDateTime::createDateTime(QDateTime::currentDateTime());
    config.Bus_ID=Settings::BusSerial();
    config.Line_Number=settings.value("bus/BusLine", 0).toUInt();
    for(quint8 i=0; i<10; i++){
        config.Price[i] = settings.value(QString("bus/Price%1").arg(i), 0).toUInt();
    }
    QByteArray data;
    quint32 tmp32=0;
    quint32 tmp8=0;

    data.clear();
    data.append((char *)&config.Datetime, 4);
    tmp32 = config.Bus_ID;
    tmp32 = tmp32 << 8;
    data.append((char *)&tmp32, 3);
    tmp32 = config.Line_Number;
    tmp32 = tmp32 << 8;
    data.append((char *)&tmp32, 3);
    for(quint8 i=0; i<10; i++){
        data.append((char *)&config.Price[i], 2);
    }
    /*append reserve byte*/
    data.append('\0');
    /*append checksum*/
    tmp8 = CalculateChecksum(data);
    data.append((char *)&tmp8, 1);

    sendCommand(_activeReader, Settings_Command);
    writeData(data);
    sendCommand(_activeReader, Finish_Command);
}
#ifdef MY210
void Readers::applyReaderId(){
    recuestTimer->stop();

    _Reader1PowerControlerPin->setHigh();//power off
    _Reader2PowerControlerPin->setHigh();//power off
    _Reader3PowerControlerPin->setHigh();//power off
    Delay::delay_ms(1000);

    for(quint8 j=0; j<Settings::ReaderCount(); j++){
        if(j==0) _Reader1PowerControlerPin->setLow();//power on
        if(j==1) _Reader2PowerControlerPin->setLow();//power on
        if(j==2) _Reader3PowerControlerPin->setLow();//power on
        Delay::delay_ms(2000);

        readerIdAvailable = j+1;
        activeReader = readerIdAvailable;

        CommandRecivedFlag = false;//ممکن است در خاموش وروشن کردن کاراکتر 255 رسیده باشد
        for(quint8 i=1; i<=3; i++){
            sendCommand(i, SetReaderId_Command);
            writeData(QByteArray((char *)&readerIdAvailable, 1));
            sendCommand(i, Finish_Command);
            commandTimoutTimer->start();
            State = SendReaderId_STATE;
        }
        Delay::delay_ms(1000);
        if(j==0) _Reader1PowerControlerPin->setHigh();//power off
        if(j==1) _Reader2PowerControlerPin->setHigh();//power off
        if(j==2) _Reader3PowerControlerPin->setHigh();//power off
    }
    _Reader1PowerControlerPin->setLow();//power on
    _Reader2PowerControlerPin->setLow();//power on
    _Reader3PowerControlerPin->setLow();//power on

    recuestTimer->start();
}
#elif defined TINY210
void Readers::applyReaderId(quint8 id){
    qLog;
    readerIdAvailable = id;
}
#endif

bool Readers::sendRequest(){
    if(State != READY_STATE) return false;

    activeReader = (++ReaderId%readerCount)+1;

    if(readerFormatAvailable && readerFormatAvailable==activeReader){
        if(activeReader == 1)State = SendFormatMemory1_STATE;
        else if(activeReader == 2)State = SendFormatMemory2_STATE;
        else if(activeReader == 3)State = SendFormatMemory3_STATE;

        sendCommand(activeReader, FoematReaderMemory_Command);
        sendCommand(activeReader, FoematReaderMemory_Command);
        sendCommand(activeReader, FoematReaderMemory_Command);
        commandTimoutTimer->start();
    }
#ifdef TINY210
    else if(readerIdAvailable){
        qLog;
        for(quint8 i=1; i<=3; i++){
            sendCommand(i, SetReaderId_Command);
            writeData(QByteArray((char *)&readerIdAvailable, 1));
            sendCommand(i, Finish_Command);
            commandTimoutTimer->start();
        }
        Delay::delay_ms(5);
        State = SendReaderId_STATE;
    }
#endif
    else if(Settings::ReaderDriverIdAvalable(activeReader)){
        if(activeReader == 1)State = SendDriverIdForReader1_STATE;
        else if(activeReader == 2)State = SendDriverIdForReader2_STATE;
        else if(activeReader == 3)State = SendDriverIdForReader3_STATE;
        sendCommand(activeReader, DriverLogin_Command);
        quint64 tmpDriverId=Settings::driverId();
        writeData(QByteArray((char *)&tmpDriverId, 8));
        sendCommand(activeReader, Finish_Command);

    }else if(Settings::ReaderConfigAvalable(activeReader)){
        applyConfig(activeReader);
    }else{
        Recived_CardList.clear();
        ReciveBuffer.clear();
        State = WaitForCardData_STATE;
        dataIsvalid=true;
        sendCommand(activeReader, Request_Command);
    }
    commandTimoutTimer->start();

    return true;
}
